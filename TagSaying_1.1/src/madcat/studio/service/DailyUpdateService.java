package madcat.studio.service;

import java.util.ArrayList;
import java.util.Random;

import madcat.studio.constants.Constants;
import madcat.studio.data.FamousData;
import madcat.studio.tagsaying.Setting;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

public class DailyUpdateService extends Activity {
	
	private final String TAG										=	"DailyUpdateService";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		Utils.logPrint(getClass(), "알람 매니저 작동 시작");
		
		super.onCreate(savedInstanceState);
		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		int randIndex = Utils.getMainRandomIndex(mContext, Setting.PREF_ALL_UPDATE);

		// 만일, 즐겨찾기된 명언 값을 위젯에 표시하기로 설정이 되어 있다면
		if(mConfigPref.getInt(Constants.CONFIG_PREF_WIDGET_DISPLAY_INDEX, Setting.PREF_WIDGET_DISPLAY_DAILY) 
				== Setting.PREF_WIDGET_DISPLAY_FAVORITE) {
			ArrayList<FamousData> items = Utils.getFavoriteData(mContext);
			Utils.updateFavoriteWidgetData(mContext, items);
		}
		
		SharedPreferences.Editor editor = mConfigPref.edit();
		editor.putInt(Constants.CONFIG_PREF_TODAY_INDEX, randIndex);
		editor.commit();
		
		Utils.logPrint(getClass(), "알람 매니저 작동 완료");
		
	}
	
	@Override
	protected void onStart() {
		finish();
		
		super.onStart();
	}
	

}





