package madcat.studio.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import madcat.studio.tagsaying.Setting;
import madcat.studio.utils.Utils;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;

public class DailyAlarmService {

	public static final int ALARM_CODE									=	151;
	
	public static void registeUpdateService(Context context, int mode) {
		Utils.logPrint(DailyAlarmService.class, "알람을 등록합니다. 설정 모드 (" + mode + ")");
		
		// 알람 등록
		AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		Intent dailyIntent = new Intent(context, DailyUpdateService.class);
		PendingIntent dailyPIntent = PendingIntent.getActivity(context, ALARM_CODE, dailyIntent, 0);
		
		Calendar dailyCalendar = Calendar.getInstance();
		dailyCalendar.add(Calendar.DATE, 1);
		dailyCalendar.set(Calendar.HOUR_OF_DAY, 0);
		dailyCalendar.set(Calendar.MINUTE, 0);
		dailyCalendar.set(Calendar.SECOND, 0);
		
		Utils.logPrint(DailyAlarmService.class, "설정된 시간 : " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dailyCalendar.getTime()));
		
		switch(mode) {
			case Setting.PREF_WIDGET_UPDATE_DAILY:
				am.setRepeating(AlarmManager.RTC_WAKEUP, dailyCalendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, dailyPIntent);
				break;
				
			case Setting.PREF_WIDGET_UPDATE_3_HOUR:
				am.setRepeating(AlarmManager.RTC_WAKEUP, dailyCalendar.getTimeInMillis(), AlarmManager.INTERVAL_HOUR * 3, dailyPIntent);
				break;
				
			case Setting.PREF_WIDGET_UPDATE_6_HOUR:
				am.setRepeating(AlarmManager.RTC_WAKEUP, dailyCalendar.getTimeInMillis(), AlarmManager.INTERVAL_HOUR * 6, dailyPIntent);
				break;
				
			case Setting.PREF_WIDGET_UPDATE_12_HOUR:
				am.setRepeating(AlarmManager.RTC_WAKEUP, dailyCalendar.getTimeInMillis(), AlarmManager.INTERVAL_HALF_DAY, dailyPIntent);
				break;
			}
		
	}
	
	public static void reRegisteUpdateService(Context context, int mode) {
		Utils.logPrint(DailyAlarmService.class, "알람을 재설정 합니다. 설정할 모드 (" + mode + ")");
		
		unRegisteUpdateService(context);
		registeUpdateService(context, mode);
	}
	
	public static void unRegisteUpdateService(Context context) {
		
		Utils.logPrint(DailyAlarmService.class, "알람 등록 취소");
		
		Intent dailyIntent = new Intent(context, DailyUpdateService.class);
		PendingIntent pIntent = PendingIntent.getBroadcast(context, ALARM_CODE, dailyIntent, 0);
		AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		
		am.cancel(pIntent);
	}
	
}
