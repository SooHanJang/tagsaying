package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.FamousData;
import madcat.studio.tagsaying.R;
import madcat.studio.tagsaying.Setting;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AdapterFavoriteList extends ArrayAdapter<FamousData> {
	
	private final String TAG										=	"AdapterFavoriteList";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private int mResId, mLngType;
	private ArrayList<FamousData> mItems;
	private LayoutInflater mInflater;
	
	private ArrayList<FamousData> mUnCheckedItems;
	
	
	public AdapterFavoriteList(Context context, int resId, ArrayList<FamousData> items, int lngType) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLngType = lngType;
		this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		this.mUnCheckedItems = new ArrayList<FamousData>();
		mUnCheckedItems.clear();
		
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		
		if(convertView == null) {
			convertView = mInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			
			holder.liRoot = (LinearLayout)convertView.findViewById(R.id.favorite_linear_root);
			holder.tvSaying = (TextView)convertView.findViewById(R.id.favorite_saying);
			holder.tvCategory = (TextView)convertView.findViewById(R.id.favorite_category);
			holder.tvAuthor = (TextView)convertView.findViewById(R.id.favorite_author);
			holder.cbFavorite = (CheckBox)convertView.findViewById(R.id.favorite_check);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		switch(mLngType) {
			case Setting.PREF_LNG_CHANGE_KOR:
				holder.tvSaying.setText(mItems.get(position).getKText());
				holder.tvCategory.setText("[" + mItems.get(position).getKCategory() + "]");
				holder.tvAuthor.setText(mItems.get(position).getKAuthor());
				
				break;
			case Setting.PREF_LNG_CHANGE_ENG:
				holder.tvSaying.setText(mItems.get(position).getEText());
				holder.tvCategory.setText("[" + mItems.get(position).getECategory() + "]");
				holder.tvAuthor.setText(mItems.get(position).getEAuthor());
				
				break;
		}
		
		if(mItems.get(position).getFavoriteFlagEnable()) {
			holder.cbFavorite.setChecked(true);
		} else {
			holder.cbFavorite.setChecked(false);
		}
		
		holder.liRoot.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				holder.cbFavorite.setChecked(!holder.cbFavorite.isChecked());
				setEnable(holder.cbFavorite, holder.cbFavorite.isChecked(), position);
			}
		});
		
		holder.cbFavorite.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				setEnable(holder.cbFavorite, holder.cbFavorite.isChecked(), position);
				
			}
		});
		
		
		
		return convertView;
	}
	
	private void setEnable(CheckBox cb, boolean check, int position) {
		if(cb.isChecked()) {
			mItems.get(position).setFavoriteEnable(true);
			
			for(int i=0; i < mUnCheckedItems.size(); i++) {
				if(mItems.get(position).getId() == mUnCheckedItems.get(i).getId()) {
					mUnCheckedItems.remove(i);
				}
			}
			
			
		} else {
			mItems.get(position).setFavoriteEnable(false);

			mUnCheckedItems.add(mItems.get(position));
			
		}
	}
	
	public ArrayList<FamousData> getUnCheckedItem() {
		return mUnCheckedItems;
	}
	
	
	class ViewHolder {
		private LinearLayout liRoot;
		private TextView tvSaying;
		private TextView tvCategory;
		private TextView tvAuthor;
		private CheckBox cbFavorite;
	}

}







