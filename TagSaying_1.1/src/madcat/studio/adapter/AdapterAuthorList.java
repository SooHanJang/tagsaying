package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.adapter.AdapterAuthorQuotesList.ViewHolder;
import madcat.studio.data.AuthorData;
import madcat.studio.data.FamousData;
import madcat.studio.tagsaying.R;
import madcat.studio.tagsaying.Setting;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AdapterAuthorList extends ArrayAdapter<AuthorData> {
	
	private Context mContext;
	private int mResId, mLngType;
	private ArrayList<AuthorData> mItems;
	private LayoutInflater mInflater;

	public AdapterAuthorList(Context context, int resId, ArrayList<AuthorData> items, int lngType) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLngType = lngType;
		this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			
			holder.authorImg = (ImageView)convertView.findViewById(R.id.author_list_img);
			holder.authorName = (TextView)convertView.findViewById(R.id.author_list_name);
			holder.authorJob = (TextView)convertView.findViewById(R.id.author_list_job);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		switch(mLngType) {
			case Setting.PREF_LNG_CHANGE_KOR:
				
				holder.authorName.setText(mItems.get(position).getKName());
				holder.authorJob.setText(mItems.get(position).getKJob());
				
				break;
				
			case Setting.PREF_LNG_CHANGE_ENG:

				holder.authorName.setText(mItems.get(position).getEName());
				holder.authorJob.setText(mItems.get(position).getEJob());
				
				break;
		}
		
		holder.authorName.setSelected(true);
		holder.authorJob.setSelected(true);
		
		int resId = mContext.getResources().getIdentifier("author_img_" + mItems.get(position).getId(), "drawable", mContext.getPackageName());
		
		Utils.logPrint(getClass(), "가져온 이미지 : " + resId);

		if(resId != 0) {
			holder.authorImg.setBackgroundResource(resId);
		} else {
			holder.authorImg.setBackgroundResource(R.drawable.author_img_default);
		}
		
		
		return convertView;
	}
	
	class ViewHolder {
		private ImageView authorImg;
		private TextView authorName;
		private TextView authorJob;
	}

}






















