package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.CategoryList;
import madcat.studio.tagsaying.R;
import madcat.studio.tagsaying.Setting;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdaperCategoryList extends ArrayAdapter<CategoryList>{
	
	private ArrayList<CategoryList> mItems;
	private Context mContext;
	private LayoutInflater mInflater;
	private int mResId, mLngType;

	public AdaperCategoryList(Context context, int resId, ArrayList<CategoryList> items, int lngType) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLngType = lngType;
		this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			
			holder.imgCategory = (ImageView)convertView.findViewById(R.id.category_row_icon);
			holder.textName = (TextView)convertView.findViewById(R.id.category_row_name);
			holder.textSize = (TextView)convertView.findViewById(R.id.category_row_size);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder)convertView.getTag();
		}
		
		switch(mLngType) {
			case Setting.PREF_LNG_CHANGE_KOR:
				holder.textName.setText(mItems.get(position).getKName());
				break;
			case Setting.PREF_LNG_CHANGE_ENG:
				holder.textName.setText(mItems.get(position).getEName());
				break;
		}

		int resId = mContext.getResources().getIdentifier("category_icon_" + mItems.get(position).getId(), "drawable", mContext.getPackageName());
		holder.imgCategory.setBackgroundResource(resId);
		
		holder.textSize.setText("(" + mItems.get(position).getSize() + ")");
		
		return convertView;
	}
	
	
	class ViewHolder {
		private ImageView imgCategory;
		private TextView textName;
		private TextView textSize;
	}

}
