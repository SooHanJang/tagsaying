package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.tagsaying.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterShareList extends ArrayAdapter<String> {
	
	private final String TAG										=	"AdapterShareList";
	
	private Context mContext;
	private int mResId;
	private String[] mItems;
	private LayoutInflater mInflater;
	
	public AdapterShareList(Context context, int resId, String[] items) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mInflater.inflate(mResId, null);
			
			holder = new ViewHolder();

			
			holder.imageIcon = (ImageView)convertView.findViewById(R.id.share_list_img_icon);
			holder.tvTitle = (TextView)convertView.findViewById(R.id.share_list_tv_title);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		switch(position) {
			case Constants.SHARE_SMS:
				holder.imageIcon.setBackgroundResource(R.drawable.share_icon_sms);
				holder.tvTitle.setText(mItems[Constants.SHARE_SMS]);
				
				break;
			case Constants.SHARE_FACEBOOK:
				holder.imageIcon.setBackgroundResource(R.drawable.share_icon_facebook);
				holder.tvTitle.setText(mItems[Constants.SHARE_FACEBOOK]);
				
				break;
			case Constants.SHARE_TWITTER:
				holder.imageIcon.setBackgroundResource(R.drawable.share_icon_twitter);
				holder.tvTitle.setText(mItems[Constants.SHARE_TWITTER]);
				
				break;
			case Constants.SHARE_METOODAY:
				holder.imageIcon.setBackgroundResource(R.drawable.share_icon_me2day);
				holder.tvTitle.setText(mItems[Constants.SHARE_METOODAY]);
				
				break;
			case Constants.SHARE_KAKAOTALK:
				holder.imageIcon.setBackgroundResource(R.drawable.share_icon_kakaotalk);
				holder.tvTitle.setText(mItems[Constants.SHARE_KAKAOTALK]);
				
				break;
		}
		
		return convertView;
	}
	
	private class ViewHolder {
		private ImageView imageIcon;
		private TextView tvTitle;
	}
	

}
