package madcat.studio.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;

import madcat.studio.data.FamousData;
import madcat.studio.tagsaying.R;
import madcat.studio.tagsaying.Setting;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AdapterAuthorQuotesList extends ArrayAdapter<FamousData> {
	
	private Context mContext;
	private MessagePool mMessagePool;
	private int mResId, mLngType;
	private ArrayList<FamousData> mItems;
	private LayoutInflater mInflater;
	
	public AdapterAuthorQuotesList(Context context, int resId, ArrayList<FamousData> items, int lngType) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLngType = lngType;
		this.mMessagePool = (MessagePool) mContext.getApplicationContext();
		this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		
		if(convertView == null) {
			convertView = mInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			
			holder.liRoot = (LinearLayout)convertView.findViewById(R.id.author_quotes_linear);
			holder.tvSaying = (TextView)convertView.findViewById(R.id.author_quotes_saying);
			holder.tvCategory = (TextView)convertView.findViewById(R.id.author_quotes_category);
			holder.cbFavorite = (CheckBox)convertView.findViewById(R.id.author_quotes_check);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		switch(mLngType) {
			case Setting.PREF_LNG_CHANGE_KOR:
				holder.tvSaying.setText(mItems.get(position).getKText());
				holder.tvCategory.setText("[" + mItems.get(position).getKCategory() + mContext.getString(R.string.relate_author_postfix) + "]");
				
				break;
			case Setting.PREF_LNG_CHANGE_ENG:
				holder.tvSaying.setText(mItems.get(position).getEText());
				holder.tvCategory.setText("[" + mItems.get(position).getECategory() + mContext.getString(R.string.relate_author_postfix) + "]");
				
				break;
		}
		
		if(mItems.get(position).getFavoriteFlagEnable()) {
			holder.cbFavorite.setChecked(true);
		} else {
			holder.cbFavorite.setChecked(false);
		}
		
		holder.liRoot.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				holder.cbFavorite.setChecked(!holder.cbFavorite.isChecked());
				setEnableSaying(holder.cbFavorite, holder.cbFavorite.isChecked(), position);
			}
		});
		
		holder.cbFavorite.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				setEnableSaying(holder.cbFavorite, holder.cbFavorite.isChecked(), position);
			}
		});
		
		
		return convertView;
	}
	
	private Comparator<FamousData> famousSort = new Comparator<FamousData>() {
		
		public int compare(FamousData obj1, FamousData obj2) {

			return (obj1.getId() > obj2.getId() ? 1 : -1);
		}
		
	};
	
	private void setEnableSaying(CheckBox cb, boolean flag, int position) {
		mItems.get(position).setFavoriteEnable(cb.isChecked());

		ArrayList<FamousData> famousData = mMessagePool.getDataArray();
		
		// 1. ��ȯ
		Hashtable<Integer, FamousData> famousHash = new Hashtable<Integer, FamousData>();
		for(FamousData data : famousData) {
			famousHash.put(data.getId(), data);
		}

		famousHash.put(mItems.get(position).getId(), mItems.get(position));
		
		ArrayList<FamousData> changeData = new ArrayList<FamousData>(famousHash.values());
		Collections.sort(changeData, famousSort);

		mMessagePool.setDataArray(changeData);
		
		Utils.updateCheckFavorite(mContext, mItems.get(position), cb.isChecked());
		
		if(!cb.isChecked()) {
			Utils.checkToFavoriteWidget(mContext, mItems);
		}
		
	}
	
	class ViewHolder {
		private LinearLayout liRoot;
		private TextView tvSaying;
		private TextView tvCategory;
		private CheckBox cbFavorite;
	}
	
	

}
