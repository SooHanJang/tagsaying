package madcat.studio.dialog;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import madcat.studio.constants.Constants;
import madcat.studio.data.FamousData;
import madcat.studio.data.MemoData;
import madcat.studio.facebook.AsyncFacebookRunner.RequestListener;
import madcat.studio.facebook.Facebook;
import madcat.studio.kakao.KakaoLink;
import madcat.studio.me2day.CreatePostPoster;
import madcat.studio.me2day.Me2dayInfo;
import madcat.studio.tagsaying.MainMenu.MessageRequestListener;
import madcat.studio.tagsaying.R;
import madcat.studio.tagsaying.Setting;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.http.AccessToken;
import twitter4j.http.OAuthAuthorization;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MemoDialog extends Dialog implements OnClickListener {
	
	private final String TAG										=	"MemoDialog";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private final float DIALOG_SIZE									=	0.8f;
	
	public static final int DIALOG_MODE_VIEWER						=	1;
	public static final int DIALOG_MODE_WRITE						=	2;
	public static final int DIALOG_MODE_MODIFY						=	3;
	public static final int DIALOG_MODE_SHARE_ME2DAY				=	4;
	public static final int DIALOG_MODE_SHARE_FACE_BOOK				=	5;
	public static final int DIALOG_MODE_SHARE_TWITTER				=	6;
	public static final int DIALOG_MODE_SHARE_KAKAO					=	7;
	public static final int DIALOG_MODE_SHARE_SMS					=	8;
	
	public static final int DIALOG_OK								=	1;
	public static final int DIALOG_CANCEL							=	2;
	
	private Context mContext;
	private MessagePool mMessagePool;
	
	private LinearLayout mBtnOkLinear, mBtnCancelLinear, mBtnModifyLinear;
	private EditText mEditMemo;
	private TextView mTvSaying, mTvAuthor;
	private ImageButton mBtnOk, mBtnCancel, mBtnModify;
	
	private FamousData mFamousData;
	private MemoData mMemoData;
	
	// 미투데이 관련 변수
	private int mLngType;
	private int mDigState											=	DIALOG_CANCEL;
	private int mDigMode											=	DIALOG_MODE_WRITE;
	private String mUserId, mFullToken;
	
	// 페이스북 관련 변수
	private MessageRequestListener mMessageRequestListener;
	private Facebook mFaceBook;
	
	
	/**
	 * 메모 작성 시 호출하는 생성자
	 * 
	 * @param context
	 * @param famousData
	 * @param mode
	 * @param lngType
	 */
	public MemoDialog(Context context, FamousData famousData, int mode, int lngType) {
		super(context);
		this.mContext = context;
		this.mMessagePool = (MessagePool) mContext.getApplicationContext();
		this.mFamousData = famousData;
		this.mDigMode = mode;
		this.mLngType = lngType;
	}
	
	/**
	 * 메모 데이터 수정, 트위터, 카카오톡, 문자 전송 포스팅 시 호출하는 생성자
	 * 
	 * @param context
	 * @param famousData
	 * @param memoData
	 * @param mode
	 * @param lngType
	 */
	public MemoDialog(Context context, FamousData famousData, MemoData memoData, int mode, int lngType) {
		super(context);
		this.mContext = context;
		this.mMessagePool = (MessagePool) mContext.getApplicationContext();
		this.mFamousData = famousData;
		this.mMemoData = memoData;
		this.mDigMode = mode;
		this.mLngType = lngType;
	}
	
	/**
	 * 미투데이 사용시 호출하는 생성자
	 * 
	 * @param context
	 * @param famousData
	 * @param memoData
	 * @param mode
	 * @param lngType
	 * @param userId
	 * @param fullToken
	 */
	public MemoDialog(Context context, FamousData famousData, MemoData memoData, int mode, int lngType, 
			String userId, String fullToken) {
		super(context);
		this.mContext = context;
		this.mMessagePool = (MessagePool) mContext.getApplicationContext();
		this.mFamousData = famousData;
		this.mMemoData = memoData;
		this.mDigMode = mode;
		this.mLngType = lngType;
		this.mUserId = userId;
		this.mFullToken = fullToken;
	}
	
	
	/**
	 * 페이스북 사용시 호출하는 생성자
	 * 
	 * @param context
	 * @param asyncRunner
	 * @param famousData
	 * @param memoData
	 * @param mode
	 * @param lngType
	 */
	public MemoDialog(Context context, Facebook faceBook, MessageRequestListener messageRequestListener, FamousData famousData, MemoData memoData, int mode, int lngType) { 
		super(context);
		this.mContext = context;
		this.mMessagePool = (MessagePool) mContext.getApplicationContext();
		this.mFaceBook = faceBook;
		this.mMessageRequestListener = messageRequestListener;
		this.mFamousData = famousData;
		this.mMemoData = memoData;
		this.mDigMode = mode;
		this.mLngType = lngType;
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_memo);
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		mBtnOkLinear = (LinearLayout)findViewById(R.id.dialog_memo_btn_ok_layout);
		mBtnCancelLinear = (LinearLayout)findViewById(R.id.dialog_memo_btn_cancle_layout);
		mBtnModifyLinear = (LinearLayout)findViewById(R.id.dialog_memo_btn_modify_layout);
		mTvSaying = (TextView)findViewById(R.id.dialog_memo_saying_text);
		mTvAuthor = (TextView)findViewById(R.id.dialog_memo_author);
		mEditMemo = (EditText)findViewById(R.id.dialog_memo_input_memo);
		mBtnOk = (ImageButton)findViewById(R.id.dialog_memo_btn_ok);
		mBtnCancel = (ImageButton)findViewById(R.id.dialog_memo_btn_cancle);
		mBtnModify = (ImageButton)findViewById(R.id.dialog_memo_btn_modify);
		
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		mBtnModify.setOnClickListener(this);
		
		
		// 메모를 작성하기 위한 기본 값 설정
		
		setDialogMode(mLngType, mDigMode);
		
	}
	
	private void setDialogMode(int lngType, int mode) {
		switch(lngType) {
			case Setting.PREF_LNG_CHANGE_KOR:
				mTvSaying.setText(mFamousData.getKText());
				mTvAuthor.setText(mFamousData.getKAuthor());
				break;
			case Setting.PREF_LNG_CHANGE_ENG:
				mTvSaying.setText(mFamousData.getEText());
				mTvAuthor.setText(mFamousData.getEAuthor());
				break;
		}
		
		
		switch(mode) {
			case DIALOG_MODE_VIEWER:

				mBtnModifyLinear.setVisibility(View.VISIBLE);
				mBtnOkLinear.setVisibility(View.INVISIBLE);
				mBtnCancel.setVisibility(View.INVISIBLE);
				
				mEditMemo.setText(mMemoData.getMemoText());
				mEditMemo.setFocusableInTouchMode(false);
				
				break;
			case DIALOG_MODE_WRITE:
				
				mBtnModifyLinear.setVisibility(View.GONE);
				mBtnOkLinear.setVisibility(View.VISIBLE);
				mBtnCancel.setVisibility(View.VISIBLE);
				
				
				mEditMemo.setFocusableInTouchMode(true);
				mEditMemo.setText("");
				
				break;
			case DIALOG_MODE_MODIFY:
				
				mBtnModifyLinear.setVisibility(View.GONE);
				mBtnOkLinear.setVisibility(View.VISIBLE);
				mBtnCancel.setVisibility(View.VISIBLE);
				
				mEditMemo.setFocusableInTouchMode(true);
				mEditMemo.setText(mMemoData.getMemoText());
				
				break;
			case DIALOG_MODE_SHARE_ME2DAY:
			case DIALOG_MODE_SHARE_FACE_BOOK:
			case DIALOG_MODE_SHARE_TWITTER:
			case DIALOG_MODE_SHARE_KAKAO:
			case DIALOG_MODE_SHARE_SMS:

				mBtnModifyLinear.setVisibility(View.GONE);
				mBtnOkLinear.setVisibility(View.VISIBLE);
				mBtnCancel.setVisibility(View.VISIBLE);
				
				mEditMemo.setFocusableInTouchMode(true);
				
				
				if(mMemoData != null) {
					mEditMemo.setText(mMemoData.getMemoText());
				}
				
				break;
				
		}
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dialog_memo_btn_ok:

				String memoTxt = mEditMemo.getText().toString().trim();
				
				if(memoTxt.length() != 0) {
					
					switch(mDigMode) {
						case DIALOG_MODE_WRITE:
							
							ArrayList<FamousData> dataArray = mMessagePool.getDataArray();
							int dataSize = dataArray.size();
							
							Utils.insertMemoToDb(mContext, mFamousData.getId(), memoTxt);
							
							for(int i=0; i < dataSize; i++) {
								if(dataArray.get(i).getId() == mFamousData.getId()) {
									dataArray.get(i).setMemoEnable(true);
								}
							}
							
							mMessagePool.setDataArray(dataArray);
							
							mDigState = DIALOG_OK;
							
							dismiss();
							
							break;
						case DIALOG_MODE_MODIFY:
							
							Utils.updateMemoToDb(mContext, mMemoData.getFamouseId(), mMemoData.getMemoDate(), memoTxt);
							
							mDigState = DIALOG_OK;
							
							dismiss();
							
							break;
						
						case DIALOG_MODE_SHARE_ME2DAY:
						case DIALOG_MODE_SHARE_FACE_BOOK:
						case DIALOG_MODE_SHARE_TWITTER:
						case DIALOG_MODE_SHARE_KAKAO:
						case DIALOG_MODE_SHARE_SMS:
							PostSNSAsync postSNSAsync = new PostSNSAsync(memoTxt, true);
							postSNSAsync.execute();
							
							break;
							
					}
					
				} else {
					
					switch(mDigMode) {
						case DIALOG_MODE_SHARE_ME2DAY:
						case DIALOG_MODE_SHARE_FACE_BOOK:
						case DIALOG_MODE_SHARE_TWITTER:
						case DIALOG_MODE_SHARE_KAKAO:
						case DIALOG_MODE_SHARE_SMS:
							PostSNSAsync postSNSAsync = new PostSNSAsync(memoTxt, false);
							postSNSAsync.execute();
							
							break;
							
							
						default:
							Toast.makeText(mContext, mContext.getString(R.string.dialog_memo_toast_caution), Toast.LENGTH_SHORT).show();
							break;
					}
					
				}
				
				break;
				
			case R.id.dialog_memo_btn_cancle:
				
				mDigState = DIALOG_CANCEL;
				
				dismiss();
				
				break;
				
			case R.id.dialog_memo_btn_modify:
				
				mDigMode = DIALOG_MODE_MODIFY;

				setDialogMode(mLngType, mDigMode);
				
				
				break;
		}
	}
	
	public int getDialogState() {
		return mDigState;
	}
	
	private String createNonLineBreakPostText(FamousData famousData, String memoTxt, int lngType) {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("『");
		
		switch(lngType) {
			case Setting.PREF_LNG_CHANGE_KOR:
				buffer.append(famousData.getKText());
				buffer.append("』");
				buffer.append("『");
				buffer.append(famousData.getKAuthor());
				break;
				
			case Setting.PREF_LNG_CHANGE_ENG:
				buffer.append(famousData.getEText());
				buffer.append(famousData.getEAuthor());
				
				break;
		}
		
		buffer.append("』");
		
		if(memoTxt.trim().toString().length() != 0) {
			buffer.append(" - ");
			buffer.append(memoTxt);
		}
		
		return buffer.toString();
		
	}
	
	private String createLineBreakPostText(FamousData famousData, String memoTxt, int lngType) {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("『");
		
		switch(lngType) {
			case Setting.PREF_LNG_CHANGE_KOR:
				buffer.append(famousData.getKText());
				buffer.append("』");
				buffer.append("『");
				buffer.append(famousData.getKAuthor());
				break;
				
			case Setting.PREF_LNG_CHANGE_ENG:
				buffer.append(famousData.getEText());
				buffer.append("』");
				buffer.append("『");
				buffer.append(famousData.getEAuthor());
				
				break;
		}
		
		buffer.append("』");
		
		if(memoTxt.trim().toString().length() != 0) {
			buffer.append("\n\n");
			buffer.append(memoTxt);
		}
		
		return buffer.toString();
	}
	
	class PostSNSAsync extends AsyncTask<Void, Void, Void> {

		ProgressDialog proDig;
		String memoTxt;
		boolean memoExist;
		KakaoLink mKakaoLink;
		
		public PostSNSAsync(String memoTxt, boolean memoExist) {
			this.memoTxt = memoTxt;
			this.memoExist = memoExist;
		}
		
		@Override
		protected void onPreExecute() {
			proDig = ProgressDialog.show(mContext, "", mContext.getString(R.string.memo_share_uploading));
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			
			if(memoExist) {
				if(mMemoData != null) {		//	기존의 메모가 존재하지만, 메모의 내용이 변경되었을 경우 (DB Update)
					if(memoTxt != mMemoData.getMemoText().trim().toString()) {
						Utils.updateMemoToDb(mContext, mMemoData.getFamouseId(), mMemoData.getMemoDate(), memoTxt);
					}
				} else {					//	메모가 존재하지 않아서, 처음 데이터베이스에 저장하는 경우 (DB Insert)
					ArrayList<FamousData> dataArray = mMessagePool.getDataArray();
					int dataSize = dataArray.size();
					
					Utils.insertMemoToDb(mContext, mFamousData.getId(), memoTxt);
					
					for(int i=0; i < dataSize; i++) {
						if(dataArray.get(i).getId() == mFamousData.getId()) {
							dataArray.get(i).setMemoEnable(true);
						}
					}
					
					mMessagePool.setDataArray(dataArray);
				}
			} 
			
			switch(mDigMode) {
				case DIALOG_MODE_SHARE_ME2DAY:
					Me2dayInfo.setLoginId(mUserId);
		        	
		        	try {
		    			CreatePostPoster createPost = new CreatePostPoster();
		    			
		    			createPost.setBody(createNonLineBreakPostText(mFamousData, memoTxt, mLngType));
		    			createPost.setTag("Android App - FamousSaying Widget with Tag");

		    			HttpParams httpParams = new BasicHttpParams();
		    			HttpConnectionParams.setConnectionTimeout(httpParams, Me2dayInfo.TIMEOUT);
		    			HttpConnectionParams.setSoTimeout(httpParams, Me2dayInfo.TIMEOUT);

		    			DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
		    			
		    			HttpPost postMethod = (HttpPost)createPost.createHttpMehtod(createPost.create_post(Me2dayInfo.getLoginId()));
		    			
		    			createPost.settingHttpClient(postMethod, httpClient, Me2dayInfo.loginId, mFullToken);
		    			HttpResponse response = httpClient.execute(postMethod);
		    			
		    		} catch (ClientProtocolException e) {
		    			e.printStackTrace();
		    		} catch (IOException e) {
		    			e.printStackTrace();
		    		}
					
					break;
					
				case DIALOG_MODE_SHARE_FACE_BOOK:
					
					Bundle faceParams = new Bundle();   
					faceParams.putString("message", createLineBreakPostText(mFamousData, memoTxt, mLngType));
				 
					String graphPath = "me/feed";
                    String httpMethod = "POST";
                    RequestListener listener = mMessageRequestListener;
                    Object state = null;
					
					try {
					 String resp = mFaceBook.request(graphPath, faceParams, httpMethod);
		                listener.onComplete(resp, state);
	                } catch (FileNotFoundException e) {
	                    listener.onFileNotFoundException(e, state);
	                } catch (MalformedURLException e) {
	                    listener.onMalformedURLException(e, state);
	                } catch (IOException e) {
	                    listener.onIOException(e, state);
	                }
					
					break;
			
				case DIALOG_MODE_SHARE_TWITTER:
					
					try {
						SharedPreferences configPref = mContext.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
						
						if(DEVELOPE_MODE) {
							Log.d(TAG, "불러온 액세스 토큰 : " + configPref.getString(Constants.CONFIG_PREF_SHARE_TWITTER_ACCESS_TOKEN, ""));
							Log.d(TAG, "불러온 액세스 시크릿 토큰 : " + configPref.getString(Constants.CONFIG_PREF_SHARE_TWITTER_ACCESS_SECRET, ""));
						}
						
						AccessToken accessToken = new AccessToken(configPref.getString(Constants.CONFIG_PREF_SHARE_TWITTER_ACCESS_TOKEN, ""), 
								configPref.getString(Constants.CONFIG_PREF_SHARE_TWITTER_ACCESS_SECRET, ""));
						
						ConfigurationBuilder cb = new ConfigurationBuilder();
			    	    cb.setOAuthAccessToken(accessToken.getToken());
			    	    cb.setOAuthAccessTokenSecret(accessToken.getTokenSecret());
			    	    cb.setOAuthConsumerKey(Constants.TWITTER_CONSUMER_KEY);
			    	    cb.setOAuthConsumerSecret(Constants.TWITTER_CONSUMER_SECRET);
			    	    Configuration config = cb.build();
			    	    OAuthAuthorization auth = new OAuthAuthorization(config);
			    	     
			    	    TwitterFactory tFactory = new TwitterFactory(config);
			    	    Twitter twitter = tFactory.getInstance();
						
						twitter.updateStatus(createNonLineBreakPostText(mFamousData, memoTxt, mLngType));
					} catch (TwitterException e) {
						e.printStackTrace();
					}
		    	    
					break;
					
				case DIALOG_MODE_SHARE_KAKAO:
					
					try {
						ArrayList<Map<String, String>> arrMetaInfo = new ArrayList<Map<String, String>>();

				        // If application is support Android platform. 
				        Map <String, String> metaInfoAndroid = new Hashtable <String, String>(1);
				        metaInfoAndroid.put("os", "android");
				        metaInfoAndroid.put("devicetype", "phone");
				        metaInfoAndroid.put("installurl", Constants.KAKAO_STR_INSTALL_URL);
				        metaInfoAndroid.put("executeurl", "etoile://");
				        arrMetaInfo.add(metaInfoAndroid);
						
						mKakaoLink = new KakaoLink(mContext, Constants.KAKAO_STR_URL, Constants.KAKAO_STR_APPID, 
								Constants.KAKAO_STR_APPVER, createLineBreakPostText(mFamousData, memoTxt, mLngType), 
								Constants.KAKAO_STR_APPNAME, arrMetaInfo, "UTF-8");
						
			            mContext.startActivity(mKakaoLink.getIntent());
						
					} catch (UnsupportedEncodingException e) {
						Toast.makeText(mContext, mContext.getString(R.string.memo_share_kakao_send_error), Toast.LENGTH_SHORT).show();
					}
					
					break;
					
				case DIALOG_MODE_SHARE_SMS:
					
					Intent smsIntent = new Intent(Intent.ACTION_VIEW);
					smsIntent.putExtra("sms_body", createLineBreakPostText(mFamousData, memoTxt, mLngType));
					smsIntent.setType("vnd.android-dir/mms-sms");
					mContext.startActivity(smsIntent);
					
					break;
					
			}
			
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			
			if(proDig.isShowing()) {
				proDig.dismiss();
			}
			
			mDigState = DIALOG_OK;
			
			dismiss();
			
		}
		
		
	}

}








