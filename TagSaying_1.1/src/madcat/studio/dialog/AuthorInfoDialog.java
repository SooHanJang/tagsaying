package madcat.studio.dialog;

import madcat.studio.data.AuthorData;
import madcat.studio.tagsaying.R;
import madcat.studio.tagsaying.Setting;
import madcat.studio.utils.Utils;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class AuthorInfoDialog extends Dialog {
	
	private Context mContext;

	private ImageView mImgAuthor;
	private TextView mTvAuthorName, mTvAuthorJob, mTvAuthorDate, mTvAuthorInfo;
	
	private AuthorData mAuthorData;
	private int mLngType;
	
	public AuthorInfoDialog(Context context, int lngType, AuthorData authorData) {
		super(context);
		this.mContext = context;
		this.mLngType = lngType;
		this.mAuthorData = authorData;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_author_info);
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		mImgAuthor = (ImageView)findViewById(R.id.dialog_img_author);
		mTvAuthorName = (TextView)findViewById(R.id.dialog_tv_author_name);
		mTvAuthorJob = (TextView)findViewById(R.id.dialog_tv_author_job);
		mTvAuthorDate = (TextView)findViewById(R.id.dialog_tv_author_date);
		mTvAuthorInfo = (TextView)findViewById(R.id.dialog_tv_author_info);
		
		mTvAuthorName.setSelected(true);
		mTvAuthorJob.setSelected(true);
		mTvAuthorDate.setSelected(true);
		mTvAuthorInfo.setSelected(true);
		
		int resId = mContext.getResources().getIdentifier("author_img_" + mAuthorData.getId(), "drawable", mContext.getPackageName());

		// 저자 이미지 설정
		if(resId != 0) {
			mImgAuthor.setBackgroundResource(resId);
		} else {
			mImgAuthor.setBackgroundResource(R.drawable.author_img_default);
		}
		
		// 저자 프로필 및 정보 설정
		switch(mLngType) {
			case Setting.PREF_LNG_CHANGE_KOR:
				mTvAuthorName.setText(mAuthorData.getKName());
				mTvAuthorJob.setText(mAuthorData.getKJob());
				mTvAuthorDate.setText(mAuthorData.getDate());
				mTvAuthorInfo.setText(mAuthorData.getKInfo());
				
				break;
				
			case Setting.PREF_LNG_CHANGE_ENG:
				mTvAuthorName.setText(mAuthorData.getEName());
				mTvAuthorJob.setText(mAuthorData.getEJob());
				mTvAuthorDate.setText(mAuthorData.getDate());
				mTvAuthorInfo.setText(mAuthorData.getEInfo());
				
				
				break;
		}
		
	}
	
	

}













