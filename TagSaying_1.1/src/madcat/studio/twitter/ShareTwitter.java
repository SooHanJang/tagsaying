package madcat.studio.twitter;

import madcat.studio.constants.Constants;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.http.AccessToken;
import twitter4j.http.RequestToken;

public class ShareTwitter {

	private static ShareTwitter mInstance									=	new ShareTwitter();
	
	private Twitter mTwitter;
	private AccessToken mAccessToken;
	private RequestToken mRequestToken;

	private ShareTwitter() {}
	
	public boolean login() {
		mTwitter = new TwitterFactory().getInstance();
    	mTwitter.setOAuthConsumer(Constants.TWITTER_CONSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET);
    	
    	mRequestToken = null;

		try {
	    	mRequestToken = mTwitter.getOAuthRequestToken();
	    	return true;
		} catch (TwitterException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public RequestToken getRequestToken() {
		return mRequestToken;
	}

	public Twitter getTwitter() {
		return mTwitter;
	}
	
	public void setTwitter(Twitter twitter) {
		this.mTwitter = twitter;
	}
	
	public static ShareTwitter getInstance() {
		return mInstance;
	}

}
