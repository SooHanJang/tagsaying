package madcat.studio.twitter;

import madcat.studio.constants.Constants;
import madcat.studio.tagsaying.MainMenu;
import madcat.studio.tagsaying.R;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.http.AccessToken;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class ShareTwitterLogin extends Activity {
	
	private final String TAG										=	"ShareTwitterLogin";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private final int START_MAIN_MENU_OK							=	1;
	private final int START_MAIN_MENU_ERROR							=	2;
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private WebView mWebView;

	private ShareTwitter mShareTwitter;
	private String mUrl;
	private String mBody;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share_twitter_login);
		this.mContext = this;
		this.mShareTwitter = ShareTwitter.getInstance();
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		this.getIntenter();
		
		mWebView = (WebView)findViewById(R.id.share_twitter_login_webview);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.addJavascriptInterface(new JavaScriptInterface(), "PINCODE");
		mWebView.loadUrl(mUrl);
		mWebView.setWebViewClient(new ConnWebViewClient());
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			mUrl = intent.getExtras().getString(Constants.PUT_SHARE_TWITTER_REQUEST_URL);
		}
	}
	
	private class ConnWebViewClient extends WebViewClient {
		private ProgressDialog proDig;

    	public void onPageStarted(WebView view, String url, Bitmap favicon) {
    		proDig = ProgressDialog.show(mContext, "", getString(R.string.memo_share_page_loading));
    	}
    	
    	public void onReceivedError(WebView view, int errorCode,
    			String description, String failingUrl) {
    		if(proDig != null) {
	    		if(proDig.isShowing()) {
	    			proDig.dismiss();
	    		}
    		}
    		
    		Toast.makeText(mContext, getString(R.string.memo_share_page_loading_error), Toast.LENGTH_SHORT).show();
    	}
    	
    	public void onPageFinished(WebView view, String url) {
    		if(proDig != null) {
	    		if(proDig.isShowing()) {
	    			proDig.dismiss();
	    		}
    		}
    		
    		view.loadUrl("javascript:window.PINCODE.getPinCode(document.getElementById('oauth_pin').innerHTML);");
    	}
    }
	
	private class JavaScriptInterface {
		public void getPinCode(String pin) {
			if(pin.length() > 0) {
				
				String result_pin = pin.split("<code>")[1].trim().split("</code>")[0];
				
				try {
					Twitter twitter = mShareTwitter.getTwitter();
					AccessToken accessToken = twitter.getOAuthAccessToken(mShareTwitter.getRequestToken(), result_pin);
					twitter.setOAuthAccessToken(accessToken);
					mShareTwitter.setTwitter(twitter);
					
					if(DEVELOPE_MODE) {
						Log.d(TAG, "핀 코드 : " + result_pin);
						Log.d(TAG, "액세스 토큰 : " + accessToken.getToken());
						Log.d(TAG, "시크릿 토큰 : " + accessToken.getTokenSecret());
					}
					
					// 로그인시 인증받은 PINCODE, AccessToken, AccessSecretToken 저장
					SharedPreferences.Editor configEditor = mConfigPref.edit();
					configEditor.putString(Constants.CONFIG_PREF_SHARE_TWITTER_PIN_CODE, result_pin);
					configEditor.putString(Constants.CONFIG_PREF_SHARE_TWITTER_ACCESS_TOKEN, accessToken.getToken());
					configEditor.putString(Constants.CONFIG_PREF_SHARE_TWITTER_ACCESS_SECRET, accessToken.getTokenSecret());
					
					configEditor.commit();
					
					startMainMenu(START_MAIN_MENU_OK);
					
					
				} catch (TwitterException e) {
					e.printStackTrace();
					
					Toast.makeText(mContext, "Error : " + e.toString(), Toast.LENGTH_SHORT).show();

					startMainMenu(START_MAIN_MENU_ERROR);
				}
				
			} else {
				// pin code 를 실패했을 경우를 출력
				Toast.makeText(mContext, getString(R.string.memo_share_twitter_failed_pincode), Toast.LENGTH_SHORT).show();
				
				startMainMenu(START_MAIN_MENU_ERROR);
			}
		}
	}
	
	private void startMainMenu(int result) {
		Intent mainIntent = new Intent(ShareTwitterLogin.this, MainMenu.class);
		
		switch(result) {
			case START_MAIN_MENU_OK:
				mainIntent.setData(Uri.parse(ShareTwitterLogin.class.toString()));
				break;
			case START_MAIN_MENU_ERROR:
				break;
		}
		
		startActivity(mainIntent);
		overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
		
		finish();
	}

}
