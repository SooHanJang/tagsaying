package madcat.studio.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.TypeVariable;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import madcat.studio.constants.Constants;
import madcat.studio.data.AuthorData;
import madcat.studio.data.CategoryList;
import madcat.studio.data.FamousData;
import madcat.studio.data.MemoData;
import madcat.studio.data.MemoListData;
import madcat.studio.dialog.AuthorInfoDialog;
import madcat.studio.me2day.Me2dayInfo;
import madcat.studio.tagsaying.R;
import madcat.studio.tagsaying.Setting;
import madcat.studio.widget.TagWidget;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.w3c.dom.Node;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

public class Utils {
	
	public static final String TAG											=	"Utils";
	public static final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	public static void logPrint(Class cls, String text) {
		if(Constants.DEVELOPE_MODE) {
			Log.d(cls.toString(), text);
		}
	}
	
	public static boolean isFavoriteEmpty(Context context) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String emptySql = "SELECT " + Constants.DATA_F_ENABLE + " " +
						  "FROM " + Constants.TABLE_DATA + " " + 
						  "WHERE " + Constants.DATA_F_ENABLE + " = 1";
		
		Cursor emptyCursor = sdb.rawQuery(emptySql, null);
		
		int count = emptyCursor.getCount();
		
		emptyCursor.close();
		sdb.close();
		
		if(count <= 0) {
			return true;
		}
		
		return false;
		
	}
	
	public static void setLocale(Context context, int lngIndex) {
		Utils.logPrint(Utils.class, "setLocale 호출 (호출 값 : " + lngIndex + ")");
		
		Locale locale = null;
		
		switch(lngIndex) {
			case Setting.PREF_LNG_CHANGE_KOR:
				locale = new Locale("ko");
				break;
			case Setting.PREF_LNG_CHANGE_ENG:
				locale = new Locale("en");
				break;
		}
		
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
	}
	
	public static void checkToFavoriteWidget(Context context, ArrayList<FamousData> famousData) {
		SharedPreferences mConfigPref = context.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		if(mConfigPref.getInt(Constants.CONFIG_PREF_WIDGET_DISPLAY_INDEX, Setting.PREF_WIDGET_DISPLAY_DAILY) 
				== Setting.PREF_WIDGET_DISPLAY_FAVORITE) {
			
			ArrayList<FamousData> items = Utils.getFavoriteData(context);
			
			// 즐겨찾기 표시로 설정되어 있는 상태에서, 즐겨찾기 값이 전부 삭제되었을 경우 설정 값을 [오늘의 명언 표시] 로 변경
			// 즐겨 찾기 값이 존재하고, 값만 변경 되었을 경우 랜덤 값 재설정
			
			if(items.isEmpty()) {
				SharedPreferences.Editor configEditor = mConfigPref.edit();
				configEditor.putInt(Constants.CONFIG_PREF_WIDGET_DISPLAY_INDEX, Setting.PREF_WIDGET_DISPLAY_DAILY);
				configEditor.commit();

				Utils.updateWidgetData(context, famousData.get(mConfigPref.getInt(Constants.CONFIG_PREF_TODAY_INDEX, 0)));
			} else {
				Utils.updateFavoriteWidgetData(context, items);
			}
		}
	}
	
	/**
	 * 데이터베이스에서 저자 정보를 가져와서 ArrayList 타입으로 반환합니다.
	 * 
	 * @param context
	 * @param lngType
	 * @return
	 */
	public static ArrayList<AuthorData> getAuthorLazyList(Context context, int count, int offset) {
		
		long startTime = System.currentTimeMillis();

		ArrayList<AuthorData> items = new ArrayList<AuthorData>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);

		String authoListSql = null;
		int totalCount = (count + offset) - 1;
		
		if(totalCount <= 700) {
			authoListSql = "SELECT * FROM " + Constants.TABLE_AUTHOR + " WHERE _id BETWEEN " + count + " AND " + totalCount;
		} else {
			authoListSql = "SELECT * FROM " + Constants.TABLE_AUTHOR + " WHERE _id BETWEEN " + count + " AND " + 751;
		}
							  
		Cursor authorListCursor = sdb.rawQuery(authoListSql, null);
		
		while(authorListCursor.moveToNext()) {
			AuthorData data = new AuthorData();
			data.setId(authorListCursor.getInt(0));
			data.setEName(authorListCursor.getString(1));
			data.setKName(authorListCursor.getString(2));
			data.setDate(authorListCursor.getString(3));
			data.setKJob(authorListCursor.getString(4));
			data.setEJob(authorListCursor.getString(5));
			data.setKText(authorListCursor.getString(6));
			data.setEText(authorListCursor.getString(7));
			
			items.add(data);
		}
		
		authorListCursor.close();
		sdb.close();
		
		long endTime = System.currentTimeMillis();
		
		Utils.logPrint(Utils.class, "저자 목록 응답 시간 : " + (endTime - startTime));
		
		return items;
		
		
	}
	
	/**
	 * 저자 정보의 영어 이름을 이용하여 데이터베이스에서 해당 저자의 정보를 String 타입으로 반환합니다.
	 * 
	 * @param context
	 * @param e_name (영어 이름)
	 * @param lngType
	 * @return
	 */
	public static AuthorData getAuthorInfo(Context context, String e_name, int lngType) {
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String authoInfoSql = "SELECT * FROM " + Constants.TABLE_AUTHOR + " WHERE " + Constants.AUTHOR_E_NAME + " =\"" + e_name + "\"";
		Cursor authoInfoCurosr = sdb.rawQuery(authoInfoSql, null);
		
		authoInfoCurosr.moveToFirst();

		AuthorData result = new AuthorData();
		
		result.setId(authoInfoCurosr.getInt(0));
		result.setEName(authoInfoCurosr.getString(1));
		result.setKName(authoInfoCurosr.getString(2));
		result.setDate(authoInfoCurosr.getString(3));
		result.setKJob(authoInfoCurosr.getString(4));
		result.setEJob(authoInfoCurosr.getString(5));
		result.setKText(authoInfoCurosr.getString(6));
		result.setEText(authoInfoCurosr.getString(7));
		
		authoInfoCurosr.close();
		sdb.close();
		
		return result;
		
	}
	
	public static ArrayList<FamousData> getRelateAuthorSaying(Context context, String name) {
		ArrayList<FamousData> items = new ArrayList<FamousData>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String relateSql = "SELECT * FROM " + Constants.TABLE_DATA + " WHERE " + Constants.DATA_E_AUTHOR + " =\"" + name + "\"";
		Cursor relateCurosr = sdb.rawQuery(relateSql, null);
		
		while(relateCurosr.moveToNext()) {
			FamousData data = new FamousData();
			
			data.setId(relateCurosr.getInt(0));
			data.setKCategory(relateCurosr.getString(1));
			data.setECategory(relateCurosr.getString(2));
			data.setKText(relateCurosr.getString(3));
			data.setKAuthor(relateCurosr.getString(4));
			data.setKTag1(relateCurosr.getString(5));
			data.setKTag2(relateCurosr.getString(6));
			data.setKTag3(relateCurosr.getString(7));
			data.setKTag4(relateCurosr.getString(8));
			data.setKTag5(relateCurosr.getString(9));
			data.setKTag6(relateCurosr.getString(10));
			data.setEText(relateCurosr.getString(11));
			data.setEAuthor(relateCurosr.getString(12));
			data.setETag1(relateCurosr.getString(13));
			data.setETag2(relateCurosr.getString(14));
			data.setETag3(relateCurosr.getString(15));
			data.setETag4(relateCurosr.getString(16));
			data.setETag5(relateCurosr.getString(17));
			data.setETag6(relateCurosr.getString(18));
			data.setFavoriteEnable(relateCurosr.getInt(19));
			data.setMemoEnable(relateCurosr.getInt(20));
			
			items.add(data);
		}
		
		relateCurosr.close();
		sdb.close();
		
		return items;
	}
	
	public static void deleteMemoEnableToDb(Context context, ArrayList<MemoListData> dataArray, boolean flag) {
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues updateValues = new ContentValues();
		updateValues.put(Constants.DATA_M_ENABLE, 0);
		
		for(int i=0; i < dataArray.size(); i++) {
			sdb.delete(Constants.TABLE_MEMO, Constants.MEMO_FAMOUS_ID + " = " + dataArray.get(i).getFamousData().getId(), null);
			sdb.update(Constants.TABLE_DATA, updateValues, Constants.DATA_ID + " = " + dataArray.get(i).getFamousData().getId(), null);
		}
		
		sdb.close();
		
		
	}
	
	public static ArrayList<MemoListData> getMemoListData(Context context) {
		
		ArrayList<MemoListData> items = new ArrayList<MemoListData>();

		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);

		String memoListSql = "SELECT m." + Constants.MEMO_DATE + ", m." + Constants.MEMO_FAMOUS_ID + ", m." + Constants.MEMO_MEMO_TEXT + ", f.* " + 
							 "FROM " + Constants.TABLE_MEMO + " m, " + Constants.TABLE_DATA + " f " +
							 "WHERE m." + Constants.MEMO_FAMOUS_ID + " = f." + Constants.DATA_ID;
		
		Cursor memoListCursor = sdb.rawQuery(memoListSql, null);
		
		while(memoListCursor.moveToNext()) {
			MemoData memoData = new MemoData(memoListCursor.getString(0), memoListCursor.getInt(1), memoListCursor.getString(2));
			FamousData famousData = new FamousData();
			
			famousData.setId(memoListCursor.getInt(3));
			famousData.setKCategory(memoListCursor.getString(4));
			famousData.setECategory(memoListCursor.getString(5));
			famousData.setKText(memoListCursor.getString(6));
			famousData.setKAuthor(memoListCursor.getString(7));
			famousData.setKTag1(memoListCursor.getString(8));
			famousData.setKTag2(memoListCursor.getString(9));
			famousData.setKTag3(memoListCursor.getString(10));
			famousData.setKTag4(memoListCursor.getString(11));
			famousData.setKTag5(memoListCursor.getString(12));
			famousData.setKTag6(memoListCursor.getString(13));
			famousData.setEText(memoListCursor.getString(14));
			famousData.setEAuthor(memoListCursor.getString(15));
			famousData.setETag1(memoListCursor.getString(16));
			famousData.setETag2(memoListCursor.getString(17));
			famousData.setETag3(memoListCursor.getString(18));
			famousData.setETag4(memoListCursor.getString(19));
			famousData.setETag5(memoListCursor.getString(20));
			famousData.setETag6(memoListCursor.getString(21));
			famousData.setFavoriteEnable(memoListCursor.getInt(22));
			famousData.setMemoEnable(memoListCursor.getInt(23));
			
			MemoListData memoListData = new MemoListData(memoData, famousData);
			items.add(memoListData);
		}
		
		memoListCursor.close();
		sdb.close();
		
		
		return items;
		
	}
	
	
	public static MemoData getMemoData(Context context, int dataId) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String memoSql = "SELECT * FROM " + Constants.TABLE_MEMO + " WHERE " + Constants.MEMO_FAMOUS_ID + " = " + dataId;

		Cursor memoCursor = sdb.rawQuery(memoSql, null);
		memoCursor.moveToFirst();
		
		MemoData memoData = new MemoData(memoCursor.getString(1), memoCursor.getInt(2), memoCursor.getString(3));
		
		memoCursor.close();
		sdb.close();
		
		return memoData;
		
	}
	
	public static void insertMemoToDb(Context context, int dataId, String memoTxt) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		
		// 메모 테이블에 삽입
		ContentValues dataValues = new ContentValues();
		dataValues.put(Constants.MEMO_DATE, getTodayDate());
		dataValues.put(Constants.MEMO_FAMOUS_ID, dataId);
		dataValues.put(Constants.MEMO_MEMO_TEXT, memoTxt);
		
		sdb.insert(Constants.TABLE_MEMO, null, dataValues);
		
		
		// 데이터 테이블에 메모가 존재한다고 변경
		dataValues.clear();
		dataValues.put(Constants.DATA_M_ENABLE, 1);
		
		sdb.update(Constants.TABLE_DATA, dataValues, Constants.DATA_ID + " = " + dataId, null);
		
		sdb.close();
		
	}
	
	public static void updateMemoToDb(Context context, int dataId, String dataDate, String memoTxt) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		// 메모 테이블을 업데이트
		ContentValues updateValues = new ContentValues();
		updateValues.put(Constants.MEMO_MEMO_TEXT, memoTxt);
		
		sdb.update(Constants.TABLE_MEMO, updateValues, 
				Constants.MEMO_FAMOUS_ID + " = " + dataId + " AND " + Constants.MEMO_DATE + " = \"" + dataDate + "\"", null);
		
		sdb.close();
		
		
	}
	
	/**
	 * 명언의 랜덤 값을 구한 뒤, 위젯에 표시할 명언 텍스트를 SharedPreference 에 설정한 다. 그 후, 해당 랜덤 값을 int 형으로 반환한다. 
	 * 
	 * @param context
	 * @return
	 */
	public static int getMainRandomIndex(Context context, int updateTarget) {
		
		SharedPreferences configPref = context.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		ArrayList<FamousData> items = Utils.getFamouseData(context);
		int index = 0;
		
		switch(updateTarget) {
			case Setting.PREF_ALL_UPDATE:
				Random random = new Random(System.currentTimeMillis());
				index = random.nextInt(items.size());
				
				break;
			case Setting.PREF_WIDGET_ONLY_UPDATE:
				index = configPref.getInt(Constants.CONFIG_PREF_TODAY_INDEX, 0); 
				
				break;
		}
		
		if(configPref.getInt(Constants.CONFIG_PREF_WIDGET_DISPLAY_INDEX, Setting.PREF_WIDGET_DISPLAY_DAILY)
				== Setting.PREF_WIDGET_DISPLAY_DAILY) {
			updateWidgetData(context, items.get(index));
		}
		
		return index;
	}
	
	public static void updateFavoriteWidgetData(Context context, ArrayList<FamousData> items) {
		Utils.logPrint(Utils.class, "updateFavoriteWidgetData 호출");
		
		Random random = new Random(System.currentTimeMillis());
		
		int index = random.nextInt(items.size());
		
		SharedPreferences configPref = context.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		if(configPref.getInt(Constants.CONFIG_PREF_WIDGET_DISPLAY_INDEX, Setting.PREF_WIDGET_DISPLAY_DAILY)
				== Setting.PREF_WIDGET_DISPLAY_FAVORITE) {
			updateWidgetData(context, items.get(index));
		}
	}
	
	/**
	 * 위젯에 쓰일 명언 데이터를 Shared 에 저장한 뒤, 위젯을 업데이트 한다.
	 * 
	 * @param context
	 * @param items
	 */
	public static void updateWidgetData(Context context, FamousData items) {
		Utils.logPrint(Utils.class, "updateWidgetData 호출");
		
		// 위젯에 쓰일 데이터를 SharedPreference 에 저장해 놓는다.
		SharedPreferences configPref = context.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		SharedPreferences.Editor configEditor = configPref.edit();
		
		configEditor.putString(Constants.CONFIG_PREF_TODAY_K_SAYING, items.getKText());
		configEditor.putString(Constants.CONFIG_PREF_TODAY_E_SAYING, items.getEText());
		configEditor.putString(Constants.CONFIG_PREF_TODAY_K_AUTHOR, items.getKAuthor());
		configEditor.putString(Constants.CONFIG_PREF_TODAY_E_AUTHOR, items.getEAuthor());
		
		configEditor.commit();
		
		// 변경된 데이터를 위젯에 알림
		sendBroadCasting(context, Constants.ACTION_WIDGET_UPDATE_SAYING);
		
	}
	
	public static ArrayList<FamousData> getCategoryArrayData(Context context, String category) {
		ArrayList<FamousData> items = new ArrayList<FamousData>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String categorySql = "SELECT * FROM " + Constants.TABLE_DATA + " WHERE " + Constants.DATA_K_CATEGORY + "=\"" + category + "\"";
		Cursor categoryCursor = sdb.rawQuery(categorySql, null);
		
		while(categoryCursor.moveToNext()) {
			FamousData categoryData = new FamousData();
			categoryData.setId(categoryCursor.getInt(0));
			categoryData.setKCategory(categoryCursor.getString(1));
			categoryData.setECategory(categoryCursor.getString(2));
			categoryData.setKText(categoryCursor.getString(3));
			categoryData.setKAuthor(categoryCursor.getString(4));
			categoryData.setKTag1(categoryCursor.getString(5));
			categoryData.setKTag2(categoryCursor.getString(6));
			categoryData.setKTag3(categoryCursor.getString(7));
			categoryData.setKTag4(categoryCursor.getString(8));
			categoryData.setKTag5(categoryCursor.getString(9));
			categoryData.setKTag6(categoryCursor.getString(10));
			categoryData.setEText(categoryCursor.getString(11));
			categoryData.setEAuthor(categoryCursor.getString(12));
			categoryData.setETag1(categoryCursor.getString(13));
			categoryData.setETag2(categoryCursor.getString(14));
			categoryData.setETag3(categoryCursor.getString(15));
			categoryData.setETag4(categoryCursor.getString(16));
			categoryData.setETag5(categoryCursor.getString(17));
			categoryData.setETag6(categoryCursor.getString(18));
			categoryData.setFavoriteEnable(categoryCursor.getInt(19));
			categoryData.setMemoEnable(categoryCursor.getInt(20));
			
			items.add(categoryData);
		}
		
		categoryCursor.close();
		sdb.close();
		
		return items;
		
	}
	
	
	public static ArrayList<FamousData> getFavoriteData(Context context) {
	
		ArrayList<FamousData> items = new ArrayList<FamousData>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String favoriteSql = "SELECT * FROM " + Constants.TABLE_DATA + " WHERE " + Constants.DATA_F_ENABLE + " = 1";
		Cursor favoriteCursor = sdb.rawQuery(favoriteSql, null);
		
		while(favoriteCursor.moveToNext()) {
			FamousData favoriteData = new FamousData();
			favoriteData.setId(favoriteCursor.getInt(0));
			favoriteData.setKCategory(favoriteCursor.getString(1));
			favoriteData.setECategory(favoriteCursor.getString(2));
			favoriteData.setKText(favoriteCursor.getString(3));
			favoriteData.setKAuthor(favoriteCursor.getString(4));
			favoriteData.setKTag1(favoriteCursor.getString(5));
			favoriteData.setKTag2(favoriteCursor.getString(6));
			favoriteData.setKTag3(favoriteCursor.getString(7));
			favoriteData.setKTag4(favoriteCursor.getString(8));
			favoriteData.setKTag5(favoriteCursor.getString(9));
			favoriteData.setKTag6(favoriteCursor.getString(10));
			favoriteData.setEText(favoriteCursor.getString(11));
			favoriteData.setEAuthor(favoriteCursor.getString(12));
			favoriteData.setETag1(favoriteCursor.getString(13));
			favoriteData.setETag2(favoriteCursor.getString(14));
			favoriteData.setETag3(favoriteCursor.getString(15));
			favoriteData.setETag4(favoriteCursor.getString(16));
			favoriteData.setETag5(favoriteCursor.getString(17));
			favoriteData.setETag6(favoriteCursor.getString(18));
			favoriteData.setFavoriteEnable(favoriteCursor.getInt(19));
			favoriteData.setMemoEnable(favoriteCursor.getInt(20));
			
			items.add(favoriteData);
		}
		
		favoriteCursor.close();
		sdb.close();
		
		return items;
		
	}
	

	/**
	 * 명언 데이터의 즐겨찾기 상태를 업데이트 합니다.
	 * 
	 * @param context
	 * @param data
	 * @param checked
	 */
	
	public static void updateCheckFavorite(Context context, FamousData data, boolean checked) {
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues updateValues = new ContentValues();
		updateValues.put(Constants.DATA_F_ENABLE, (checked==true ? 1 : 0));
		
		sdb.update(Constants.TABLE_DATA, updateValues, Constants.DATA_ID + " = " + data.getId(), null);
		
		sdb.close();
		
	}
	
	
	/**
	 * 카테고리의 활성화 유무를 업데이트 합니다.
	 * 
	 * @param context
	 * @param category
	 * @param enable
	 */
	
	public static void updateCategoryEnable(Context context, String[] category, boolean[] enable) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);

		
		ContentValues updateValues = new ContentValues();
		
		for(int i=0; i < category.length; i++) {
			updateValues.put(Constants.CATEGORY_ENABLE, enable[i]);
			
			sdb.update(Constants.TABLE_CATEGORY, updateValues, Constants.CATEGORY_K_NAME + "=\"" + category[i] + "\"", null);
			
			updateValues.clear();
		}
		
		sdb.close();
	}
	
	/**
	 * 명언 카테고리의 카테고리명과 사이즈를 가져와 ArrayList 형태로 리턴합니다.
	 * 
	 * @param context
	 * @return
	 */
	public static ArrayList<CategoryList> getCategoryArrayList(Context context) {
		ArrayList<CategoryList> items = new ArrayList<CategoryList>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String listSql = "SELECT * FROM " + Constants.TABLE_CATEGORY;
		
		Cursor listCursor = sdb.rawQuery(listSql, null);
		
		while(listCursor.moveToNext()) {
			CategoryList categoryList = new CategoryList(listCursor.getInt(0), listCursor.getString(1), listCursor.getString(2), listCursor.getInt(3), listCursor.getInt(4));
			items.add(categoryList);
		}
		
		listCursor.close();
		sdb.close();
		
		return items;
	}
	
	
	/**
	 * 활성화 된 명언 데이터를 ArrayList 형태로 리턴합니다.
	 * 
	 * @param context
	 * @return
	 */
	public static ArrayList<FamousData> getFamouseData(Context context) {
		Utils.logPrint(Utils.class, "getFamousData 호출");
		
		ArrayList<FamousData> items = new ArrayList<FamousData>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String famousSql = "SELECT b.* FROM " + Constants.TABLE_CATEGORY + " a, " + Constants.TABLE_DATA + " b " +
						   "WHERE a." + Constants.CATEGORY_K_NAME + " = b." + Constants.DATA_K_CATEGORY + " " +
						   "AND a." + Constants.CATEGORY_ENABLE + " = 1 ORDER BY b." + Constants.DATA_ID + " ASC";
		
		Cursor famousCursor = sdb.rawQuery(famousSql, null);
		
		while(famousCursor.moveToNext()) {
			FamousData famousData = new FamousData();
			famousData.setId(famousCursor.getInt(0));
			famousData.setKCategory(famousCursor.getString(1));
			famousData.setECategory(famousCursor.getString(2));
			famousData.setKText(famousCursor.getString(3));
			famousData.setKAuthor(famousCursor.getString(4));
			famousData.setKTag1(famousCursor.getString(5));
			famousData.setKTag2(famousCursor.getString(6));
			famousData.setKTag3(famousCursor.getString(7));
			famousData.setKTag4(famousCursor.getString(8));
			famousData.setKTag5(famousCursor.getString(9));
			famousData.setKTag6(famousCursor.getString(10));
			famousData.setEText(famousCursor.getString(11));
			famousData.setEAuthor(famousCursor.getString(12));
			famousData.setETag1(famousCursor.getString(13));
			famousData.setETag2(famousCursor.getString(14));
			famousData.setETag3(famousCursor.getString(15));
			famousData.setETag4(famousCursor.getString(16));
			famousData.setETag5(famousCursor.getString(17));
			famousData.setETag6(famousCursor.getString(18));
			famousData.setFavoriteEnable(famousCursor.getInt(19));
			famousData.setMemoEnable(famousCursor.getInt(20));
			
			items.add(famousData);
		}
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "current data size : " + items.size());
		}
		
		
		famousCursor.close();
		sdb.close();
		
		return items;
		
		
	}
	
	/**
	 * 오늘의 날짜를 String 형태로 리턴합니다. 
	 * @return
	 */
	public static String getTodayDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
        return dateFormat.format(new java.util.Date());
	}
	
	/**
	 * Object 타입을 byte 타입으로 Serialize 합니다.
	 * 
	 * @param object
	 * @return
	 */
	
	public static byte[] serializeObject(Object object) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "직렬화 시작");
		}
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		try {
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(object);
			oos.close();
			
			byte[] buf = bos.toByteArray();

			if(DEVELOPE_MODE) {
				Log.d(TAG, "직렬화 성공");
			}
			
			return buf;
		} catch (IOException e) {
			e.printStackTrace();
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "직렬화 실패");
			}
			
			return null;
		}
	}
	
	/**
	 * byte 타입을 해당 Object 로 deSerialize 합니다. 
	 * 
	 * @param b
	 * @return
	 */
	
	public static Object deSerializeObject(byte[] b) {
		try {
			ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(b));
			Object object = ois.readObject();
			
			ois.close();
			
			return object;
		} catch(IOException ie) {
			return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * dip 를 pixel 로 변환합니다.
	 * 
	 * @param context
	 * @param dip
	 * @return int형의 pixel 값
	 */
	
	public static int pixelFromDip(Context context, int dip) {
		Resources resources = context.getResources();
		int pixel = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, resources.getDisplayMetrics());
		return pixel;
	}
	
	/**
	 * 핸드폰 화면의 가로 크기를 dip 단위로 가져옵니다.
	 * 
	 * @param context
	 * @return int형의 핸드폰 화면의 가로 크기 dip
	 */
	
	public static int getScreenDipWidthSize(Context context) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		
		int dipWidth = (int) (displayMetrics.widthPixels / displayMetrics.density);
		
		return dipWidth;
	}

	/**
	 * 핸드폰 화면의 세로 크기를 dip 단위로 가져옵니다.
	 * 
	 * @param context
	 * @return int형의 핸드폰 화면의 세로 크기 dip
	 */
	
	public static int getScreenDipHeightSize(Context context) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		
		int dipHeight = (int) (displayMetrics.heightPixels / displayMetrics.density);
		
		return dipHeight;
	}
	
	/**
	 * 파라미터로 넘어온 action 을 BroadCasting 한다.
	 * 
	 * @param context
	 * @param action
	 */
	
	public static void sendBroadCasting(Context context, String action) {
		Intent intent = new Intent(action);
		context.sendBroadcast(intent);
	}
	
	/**
	 * Wifi의 상태를 체크하여 boolean 타입으로 반환합니다.
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isWifiAvailable(Context context) {
		ConnectivityManager manager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		boolean connect = false;
		
		try {
			if(manager == null) {
				return false;
			}
			
			NetworkInfo info = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			connect = (info.isAvailable() && info.isConnected());
		} catch(Exception e) {
			return false;
		}
		
		return connect;
	}
	
	/**
	 * 3G 상태를 체크하여 boolean 타입으로 반환합니다.
	 * 
	 * @param context
	 * @return
	 */
	public static boolean is3GAvailable(Context context) {
		ConnectivityManager manager= (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		boolean connect = false;
		try {
			if( manager == null ) {
				return false;
			}
			NetworkInfo info = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			connect = (info.isAvailable() && info.isConnected());
		} catch(Exception e) {
			return false;
		}

		return connect;
	}
	
	public static boolean isSDCardMounted() {
		return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
	}
	
	/**
	 * 부모 View를 기준으로 자식 View를 재귀 호출하면서 할당된 이미지를 해제한다.
	 * @param root
	 */
	
	public static void recursiveRecycle(View root) {
		if(root == null) {
			return;
		}
		
		if(root instanceof ViewGroup) {
			ViewGroup group = (ViewGroup)root;
			int count = group.getChildCount();
			
			for(int i=0; i < count; i++) {
				recursiveRecycle(group.getChildAt(i));
			}
			
			if(!(root instanceof AdapterView)) {
				group.removeAllViews();
			}
		}
		
		if(root instanceof ImageView) {
			((ImageView)root).setImageDrawable(null);
		}
		
		root.setBackgroundDrawable(null);
		
		root = null;
		
		return;
	}
	
	public static void appendSigUrl(StringBuffer url, boolean appAmp) {
		if (appAmp == true) {
			url.append("&");
		}
		url.append("akey=");
		url.append(Me2dayInfo.APP_KEY);
	}
	
	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		 
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			
		}		 
		return sb.toString();
	}
	
	public static final String getElementValue(Node element) {		
		if (element == null) 
			return null;
		
		if (element.getNodeType() != Node.ELEMENT_NODE) 
			return null;
		
		String value = element.getNodeValue();
		if (value == null) {
			Node child = element.getFirstChild();
			while (child != null) {
				
				switch (child.getNodeType()) {
				case Node.CDATA_SECTION_NODE:
				case Node.TEXT_NODE:
					value = child.getNodeValue();
					return value;
				}				
				child = child.getNextSibling();
			}
		}
		return value;
	}
	
	public static HttpRequestBase createHttpGetMehtod(String url) {
		return new HttpGet(url);
	}
	
	public static HttpRequestBase createHttpPostMehtod(String url) {
		return new HttpPost(url);
	}
	
	// 백업 시에는 먼저 db 경로에서 db 파일을 복사해 온 다음, db 와 같이 압축을 한 뒤, db 파일은 삭제한다.
	public static boolean dataBackUp(Context context) throws Exception {
		int ZIP_COMPRESS_LEVEL										=	8;
		
		if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
		
			// 파일명을 날짜로 설정
			Date nowTime = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
			
			// 파일 시스템 컨트롤에 사용할 변수
			String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();	//	SD 카드 경로
			File dbFile = context.getDatabasePath(Constants.DATABASE_NAME);						//	원본 데이터베이스가 저장된 경로
			
			String targetPath = sdCardPath + Constants.BACKUP_PATH;								//	압축 파일을 저장할 경로
			String zipOutPut = Constants.BACKUP_ZIP_PREFIX + dateFormat.format(nowTime) + Constants.BACKUP_ZIP_POSTFIX;		//	압축할 파일명
			String dbOutPut = Constants.DATABASE_NAME;											//	데이터베이스 파일명
	
			// 백업할 경로가 존재하는지를 판단하여, 존재하지 않으면 경로를 생성한다.
			File dir = null;
			dir = new File(targetPath);
			if(!dir.exists()) {
				dir.mkdirs();
			}
			
			// 압축 대상이 될 파일 경로를 설정한다.
			// 데이터베이스 파일을 백업 폴더에 먼저 복사를 한다.
			databaseCopy(dbFile, targetPath, dbOutPut);
				
			// 데이터베이스 복사가 완료되면, 파일 백업 시작한다.
			FileOutputStream fos = null;
			BufferedOutputStream bos = null;
			ZipOutputStream zos = null;
			
			try {
				fos = new FileOutputStream(targetPath + "/" + zipOutPut);					
				bos = new BufferedOutputStream(fos);										
				zos = new ZipOutputStream(bos);
				zos.setLevel(ZIP_COMPRESS_LEVEL);
				zipEntry(dbFile, targetPath, zos);
				zos.finish();
			} finally {
				if(zos != null) {
					zos.close();
				}
				if(bos != null) {
					bos.close();
				}
				if(fos != null) {
					fos.close();
				}
			}
			
			// 백업이 끝나면 복사한 DB 파일은 삭제한다.
			File dbDelFile = new File(targetPath + "/" + dbOutPut);
			dbDelFile.delete();
			
			return true;
		} else {
			Toast.makeText(context, "SD 카드 상태를 확인해주세요.", Toast.LENGTH_SHORT).show();
			return false;
		}
	} 
	
	/**
	 * 해당 dbFile 을 targetPath 경로에 output 형태로 복사합니다.
	 * 
	 * @param dbFile
	 * @param targetPath
	 * @param output
	 * @throws IOException
	 */
	
	public static void databaseCopy(File dbFile, String targetPath, String output) throws IOException {
		if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			FileChannel inChannel = new FileInputStream(dbFile).getChannel();
			FileChannel outChannel = new FileOutputStream(targetPath + "/" + output).getChannel();
			
			try {
				int maxCount = (64 * 1024 * 1024) - (32 * 1024);
				long size = inChannel.size();
				long position = 0;
				
				while(position < size) {
					position += inChannel.transferTo(position, maxCount, outChannel);
				}
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} finally {
				if(inChannel != null) {
					inChannel.close();
				}
				
				if(outChannel != null) {
					outChannel.close();
				}
			}
		}
	}
	
	/**
	 * 압축할 sourceFile 과 압축 경로인 sourcePath 에 맞는 zipEntry List를 생성한 뒤, 설정된 zos 경로에 압축 파일을 생성한다.
	 * 
	 * @param sourceFile
	 * @param sourcePath
	 * @param zos
	 * @throws Exception
	 */
	
	public static void zipEntry(File sourceFile, String sourcePath, ZipOutputStream zos) throws Exception {
		int BUFFER_SIZE = 1024 * 2;
		
		if(sourceFile.isDirectory()) {
			if(sourceFile.getName().equalsIgnoreCase(".metadata")) {
				return;
			}
			
			File[] fileArray = sourceFile.listFiles();
			for(int i=0; i < fileArray.length; i++) {
				zipEntry(fileArray[i], sourcePath, zos);
			}
		} else {
			BufferedInputStream bis = null;
			
			try {
				
				// 1-1. 일반 파일인 경우는 다음과 SD 카드의 경로와 외장 파일이 들어 있는 경로를 파싱하여 압축할 파일 명을 뽑아낸다.
//				String sFilePath = sourceFile.getPath();
//				String zipEntryName = sFilePath.substring(sourcePath.length() + 1, sFilePath.length());
				
				// 1-2. 데이터베이스만을 복사하여 압축할 때는 하단의 처럼 압축할 파일 명을 직접 지정
				String zipEntryName = Constants.DATABASE_NAME;
				
				bis = new BufferedInputStream(new FileInputStream(sourceFile));
				ZipEntry zentry = new ZipEntry(zipEntryName);
				zos.putNextEntry(zentry);
				
				byte[] buffer = new byte[BUFFER_SIZE];
				int cnt = 0;
				while((cnt = bis.read(buffer, 0, BUFFER_SIZE)) != -1) {
					zos.write(buffer, 0, cnt);
				}
				zos.closeEntry();
			} finally {
				if(bis != null) {
					bis.close();
				}
			}
		}
	}
	
	/**
	 * Zip 파일의 압축을 푼다.
	 * 
	 * @param zipFile - 압축 풀 Zip 파일
	 * @param targetDir	- 압축 푼 파일이 들어갈 디렉토리
	 * @param fileNameToLowerCase - 파일명을 소문자로 바꿀지에 대한 여부
	 * @throws Exception
	 */
	
	public static void unZip(File zipFile, File targetDir, boolean fileNameToLowerCase) throws Exception {
		FileInputStream fis = null;
		ZipInputStream zis = null;
		ZipEntry zentry = null;
		
		try {
			fis = new FileInputStream(zipFile);
			zis = new ZipInputStream(fis);
			
			while((zentry = zis.getNextEntry()) != null) {
				String fileNameToUnZip = zentry.getName();
				if(fileNameToLowerCase) {
					fileNameToUnZip = fileNameToUnZip.toLowerCase();
				}
				
				File targetFile = new File(targetDir, fileNameToUnZip);
				
				if(zentry.isDirectory()) {		//	디렉토리인 경우
					targetFile.mkdirs();		//	디렉토리를 생성한다.
				} else {						//	파일인 경우
					unZipEntry(zis, targetFile);
				}
			}
		} finally {
			if(zis != null) {
				zis.close();
			}
			
			if(fis != null) {
				fis.close();
			}
		}
	}
	
	public static File unZipEntry(ZipInputStream zis, File targetFile) throws Exception {
		int BUFFER_SIZE									=	1024 * 2;
		
		FileOutputStream fos = null;
		
		try {
			fos = new FileOutputStream(targetFile);
			
			byte[] buffer = new byte[BUFFER_SIZE];
			int len = 0;
			
			while((len = zis.read(buffer)) != -1) {
				fos.write(buffer, 0, len);
			}
		} finally {
			if(fos != null) {
				fos.close();
			}
		}
		
		return targetFile;
	}
	
	/**
	 * 해당 List 안에 AllThatIdea 파일명이 있는 지를 체크하여 필터링 합니다.
	 * 
	 * @param list
	 * @return
	 */
	
	public static ArrayList<String> filterFileName(ArrayList<String> list) {
		ArrayList<String> fileFilterArray = new ArrayList<String>();
		
		for(int i=0; i < list.size(); i++) {
			String[] splitFileName = list.get(i).split("_");
			
			if(splitFileName.length == 7 && splitFileName[0].equals("QuotesWithTag")) {
				fileFilterArray.add(list.get(i));
			}
		}
		
		return fileFilterArray;
	}
	
	public static String[] splitFileName(Context context, ArrayList<String> list) {
		String[] splitFileArray = new String[list.size()];
		
		for(int i=0; i < list.size(); i++) {
			String[] splitFileName = list.get(i).split("_");
			
			splitFileArray[i] = splitFileName[1] + "." + splitFileName[2] + "." + splitFileName[3] + " " +
								splitFileName[4] + context.getString(R.string.pref_backup_suffix_date_hour) + 
								splitFileName[5] + context.getString(R.string.pref_backup_suffix_date_min) + 
								context.getString(R.string.pref_backup_suffix_data);
		}
		
		return splitFileArray;
	}
	
	/**
	 * sourceFileName으로 넘어온 파일명에 읽어들여 정해진 sourcePath 에 복구합니다.
	 * 
	 * @param context
	 * @param sourceFileName - 복구할 파일명
	 * @return
	 */
	public static boolean dataRestore(Context context, String sourceFileName) {
		String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		String sourcePath = sdCardPath + Constants.BACKUP_PATH + "/" + sourceFileName;
		
		File sourceFile = new File(sourcePath);
		File dbPath = context.getDatabasePath(Constants.DATABASE_NAME);
		File targetPath = new File(dbPath.getParent());
		
		try {
			Utils.unZip(sourceFile, targetPath, false);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static int getTextViewLineBreak(Context context, String text, int widthDp) {
		
		Paint paint = new Paint();
		ArrayList<String> cutStr = new ArrayList<String>();
		
		paint.setSubpixelText(true);
		
		int end = 0;
		
		do {
			end = paint.breakText(text, true, widthDp, null);
			
			if(end > 0) {
				cutStr.add(text.substring(0, end));
				text = text.substring(end);
				
			}
		} while(end > 0);
		
		Utils.logPrint(Utils.class, "cut size : " + cutStr.size());

		return cutStr.size();
		
	}
	
	public static float getWidgetFontToFloat(int lineBreak) {
		switch(lineBreak) {
			case 1:
				return 18f;
			case 2:
				return 16f;
			case 3:
				return 12.5f;
			case 4:
				return 11f;
			default:
				return 10.5f;
				
		}
	}
	
	public static void finishActivity(Activity activity) {
			activity.finish();
			activity.overridePendingTransition(0, 0);
	}
}





