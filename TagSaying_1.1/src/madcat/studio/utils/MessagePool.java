package madcat.studio.utils;

import java.util.ArrayList;

import twitter4j.Twitter;

import madcat.studio.data.FamousData;

import android.app.Application;

public class MessagePool extends Application {
	
	private ArrayList<FamousData> mItems;

	public void setDataArray(ArrayList<FamousData> items) {
		this.mItems = items;
	}

	public ArrayList<FamousData> getDataArray() {
		return mItems;
	}

}
