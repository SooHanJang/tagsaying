package madcat.studio.widget;

import madcat.studio.constants.Constants;
import madcat.studio.service.DailyAlarmService;
import madcat.studio.tagsaying.Loading;
import madcat.studio.tagsaying.R;
import madcat.studio.tagsaying.Setting;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViews;

public class TagWidget extends AppWidgetProvider {
	
	
	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);

		Utils.logPrint(getClass(), "받은 액션 : " + intent.getAction());
		
		if(intent.getAction().equals(Constants.ACTION_WIDGET_UPDATE_SAYING)) {
			updateWidget(context);
		} else if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			SharedPreferences configPref = context.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
			
			int alarmModeIndex = configPref.getInt(Constants.CONFIG_PREF_WIDGET_UPDATE_INDEX, Setting.PREF_WIDGET_UPDATE_DAILY);
			
			Utils.logPrint(getClass(), "부트 완료 액션을 받음. 알람 재등록을 시작합니다.");
			Utils.logPrint(getClass(), "알람 설정 모드 : " + alarmModeIndex);
			
			DailyAlarmService.reRegisteUpdateService(context, alarmModeIndex);
		}
		
	}
	
	private void updateWidget(Context context) {
		ComponentName thisAppWidget = new ComponentName(context.getPackageName(), TagWidget.class.getName());
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
		
		onUpdate(context, appWidgetManager, appWidgetIds);
	}
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		for(int i=0; i < appWidgetIds.length; i++) {
			int appwidgetId = appWidgetIds[i];
			updateAppWidget(context, appWidgetManager, appwidgetId);		// 위젯을 업데이트 하는 메소드
		}
	}
	
	public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
		SharedPreferences configPref = context.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		RemoteViews updateViews = new RemoteViews(context.getPackageName(), R.layout.tag_widget);
		

		// 언어에 따른 위젯 설정
		switch(configPref.getInt(Constants.CONFIG_PREF_LNG_INDEX, Setting.PREF_LNG_CHANGE_KOR)) {
			case Setting.PREF_LNG_CHANGE_KOR:
				
				String quotesKor = configPref.getString(Constants.CONFIG_PREF_TODAY_K_SAYING, context.getString(R.string.widget_toast_wait));
				
				updateViews.setTextViewText(R.id.widget_tag_text, quotesKor);
				updateViews.setTextViewText(R.id.widget_tag_author, 
						configPref.getString(Constants.CONFIG_PREF_TODAY_K_AUTHOR, context.getString(R.string.widget_toast_wait)));

				// 폰트 크기 수정
				float fontKrSize = Utils.getWidgetFontToFloat(Utils.getTextViewLineBreak(context, quotesKor, 210));
				
				Utils.logPrint(TagWidget.class, "fontSize : " + fontKrSize);
				
				updateViews.setFloat(R.id.widget_tag_text, "setTextSize", fontKrSize);
				
				break;
				
			case Setting.PREF_LNG_CHANGE_ENG:
				String quotesEng = configPref.getString(Constants.CONFIG_PREF_TODAY_E_SAYING, context.getString(R.string.widget_toast_wait));
				
				updateViews.setTextViewText(R.id.widget_tag_text, quotesEng);
				updateViews.setTextViewText(R.id.widget_tag_author, 
						configPref.getString(Constants.CONFIG_PREF_TODAY_E_AUTHOR, context.getString(R.string.widget_toast_wait)));
				
				// 폰트 크기 수정
				float fontEnSize = Utils.getWidgetFontToFloat(Utils.getTextViewLineBreak(context, quotesEng, 210));
				
				Utils.logPrint(TagWidget.class, "fontSize : " + fontEnSize);
				
				updateViews.setFloat(R.id.widget_tag_text, "setTextSize", fontEnSize);
				
				break;
		}
		
		updateViews.setOnClickPendingIntent(R.id.widget_tag_bg, PendingIntent.getActivity(context, 0, new Intent(context, Loading.class), 0));
		
		appWidgetManager.updateAppWidget(appWidgetId, updateViews);
	}

}






















