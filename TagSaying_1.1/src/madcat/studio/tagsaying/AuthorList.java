package madcat.studio.tagsaying;

import java.util.ArrayList;

import madcat.studio.adapter.AdapterAuthorList;
import madcat.studio.constants.Constants;
import madcat.studio.data.AuthorData;
import madcat.studio.quickaction.QuickAction;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

public class AuthorList extends ListActivity implements OnClickListener, OnScrollListener {
	
	private final int READ_AUTHOR_DATA_OFFSET										=	50;

	private Context mContext;
	private SharedPreferences mConfigPref;
	private LayoutInflater mInflater;
	
	private ArrayList<AuthorData> mItems;
	private AdapterAuthorList mAdapter;
	
	private ImageButton mBtnMemo, mBtnAuthor, mBtnCategory, mBtnFavorite, mBtnSetting;
	private QuickAction mQuickShare;
	private View mFooterView;
	
	private int mLngType;
	private boolean mLockListView;
	
	private int mStartCount;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.author_list);
		
		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		this.mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mLockListView = false;
		this.mStartCount = 1;
		
		// Resource Id 설정
		mBtnMemo = (ImageButton)findViewById(R.id.main_btn_memo);
		mBtnAuthor = (ImageButton)findViewById(R.id.main_btn_author);
		mBtnCategory = (ImageButton)findViewById(R.id.main_btn_category);
		mBtnFavorite = (ImageButton)findViewById(R.id.main_btn_favorite);
		mBtnSetting = (ImageButton)findViewById(R.id.main_btn_setting);
		mFooterView = mInflater.inflate(R.layout.list_lazy_footer, null);

		// 메인 메뉴 아이콘 설정
		mBtnMemo.setBackgroundResource(R.drawable.main_menu_icon_today_off);
		mBtnAuthor.setBackgroundResource(R.drawable.main_menu_icon_author_on);
		
		// 레이지 로딩바를 footer 에 등록
		
		this.getListView().addFooterView(mFooterView);
		
		
		// Event Listener 설정
		mBtnMemo.setOnClickListener(this);
		mBtnAuthor.setOnClickListener(this);
		mBtnCategory.setOnClickListener(this);
		mBtnFavorite.setOnClickListener(this);
		mBtnSetting.setOnClickListener(this);
		this.getListView().setOnScrollListener(this);
		
		// Adapter 설정
		mLngType = mConfigPref.getInt(Constants.CONFIG_PREF_LNG_INDEX, Setting.PREF_LNG_CHANGE_KOR);
		mItems = Utils.getAuthorLazyList(mContext, mStartCount, READ_AUTHOR_DATA_OFFSET);
		mAdapter = new AdapterAuthorList(mContext, R.layout.author_list_row, mItems, mLngType);
		
		setListAdapter(mAdapter);
		
	}
	
	@Override
	protected void onDestroy() {
		
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Utils.logPrint(getClass(), "클릭된 저자 목록 인덱스 : " + position + ", 현재 startCount : " + mStartCount);

		if(position == (mStartCount + READ_AUTHOR_DATA_OFFSET -1)) {
			if(position < 750) {
				return;
			}
		}
		
		Intent quotesIntent = new Intent(AuthorList.this, AuthorQuotesList.class);
		quotesIntent.putExtra(Constants.PUT_RELATE_AUTHOR_NAME, mItems.get(position).getEName());
		quotesIntent.putExtra(Constants.PUT_RELATE_LNG_TYPE, mLngType);
		
		startActivity(quotesIntent);
		Utils.finishActivity(this);
		
		super.onListItemClick(l, v, position, id);
		
	}
	
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.main_btn_memo:
				Intent mainIntent = new Intent(AuthorList.this, MainMenu.class);
				startActivity(mainIntent);
				Utils.finishActivity(this);
				
				break;
			case R.id.main_btn_category:
				Intent listIntent = new Intent(AuthorList.this, Category.class);
				startActivity(listIntent);
				Utils.finishActivity(this);
				
				break;
			case R.id.main_btn_favorite:
				if(Utils.isFavoriteEmpty(mContext)) {
					Toast.makeText(mContext, getString(R.string.favorite_toast_empty), Toast.LENGTH_SHORT).show();
				} else {
					Intent favoriteIntent = new Intent(AuthorList.this, Favorite.class);
					startActivity(favoriteIntent);
					Utils.finishActivity(this);
				}
				
				break;
				
			case R.id.main_btn_setting:
				
				Intent settingIntent = new Intent(AuthorList.this, Setting.class);
				startActivity(settingIntent);
				Utils.finishActivity(this);
				
				break;
		}
	}

	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		
		int count = totalItemCount - visibleItemCount;
		
		Utils.logPrint(getClass(), "firstVisibleItem : " + firstVisibleItem + ", visibleItemCount : " + visibleItemCount);
		Utils.logPrint(getClass(), "totalItemCount : " + totalItemCount + ", count : " + count);
		
		// 리스트 뷰의 총 아이템 개수가 751개 미만이면, 레이지 로드를 호출하고 751개 이상이면 호출하지 않는다.
		if(totalItemCount < 751) {
			Utils.logPrint(getClass(), "불러올 수 있는 아이템이 존재");
			
			if(firstVisibleItem >= count && totalItemCount != 0 && mLockListView == false) {
				addAuthorRow(mStartCount);
			}
		} else {
			this.getListView().removeFooterView(mFooterView);
		}
	}

	public void onScrollStateChanged(AbsListView view, int scrollState) {}
	
	private void addAuthorRow(final int firstCount) {
		Utils.logPrint(getClass(), "addAuthorRow 호출");
		
		mLockListView = true;
		
		Runnable run = new Runnable() {
			public void run() {
				
				mStartCount = mStartCount + READ_AUTHOR_DATA_OFFSET;	//	51
				Utils.logPrint(getClass(), "startCount : " + mStartCount);
				
				ArrayList<AuthorData> addData = Utils.getAuthorLazyList(mContext, mStartCount, READ_AUTHOR_DATA_OFFSET);
				int addDataSize = addData.size();
				
				for(int i=0; i < addDataSize; i++) {
					Utils.logPrint(getClass(), "가져온 값 : " + addData.get(i).getId());
					
					mItems.add(addData.get(i));
				}
				
				mAdapter.notifyDataSetChanged();
				mLockListView = false;
				
			}
		};
		
		Handler handler = new Handler();
		handler.postDelayed(run, 2 * 1000);
		
	}

}














