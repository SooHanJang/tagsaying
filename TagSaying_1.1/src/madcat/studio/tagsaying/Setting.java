package madcat.studio.tagsaying;

import java.io.File;
import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.CategoryList;
import madcat.studio.data.FamousData;
import madcat.studio.quickaction.QuickAction;
import madcat.studio.service.DailyAlarmService;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

public class Setting extends PreferenceActivity implements OnPreferenceClickListener, OnClickListener {
	
	public static final int PREF_TAG_CHANGE_ON						=	0;
	public static final int PREF_TAG_CHANGE_OFF						=	1;
	
	public static final int PREF_LNG_CHANGE_KOR						=	0;
	public static final int PREF_LNG_CHANGE_ENG						=	1;
	
	public static final int PREF_DATA_BACKUP						=	10;
	public static final int PREF_DATA_RECOVERY						=	20;
	
	public static final int PREF_WIDGET_UPDATE_DAILY				=	0;
	public static final int PREF_WIDGET_UPDATE_3_HOUR				=	1;
	public static final int PREF_WIDGET_UPDATE_6_HOUR				=	2;
	public static final int PREF_WIDGET_UPDATE_12_HOUR				=	3;
	
	public static final int PREF_WIDGET_DISPLAY_DAILY				=	0;
	public static final int PREF_WIDGET_DISPLAY_FAVORITE			=	1;
	
	public static final int PREF_ALL_UPDATE							=	100;
	public static final int PREF_WIDGET_ONLY_UPDATE					=	200;
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	private MessagePool mMessagePool;
	
	private Preference mPrefUpdateCategory, mPrefTagChange, mPrefLngChange, mPrefWidgetDisplay, mPrefWidgetUpdate, mPrefBackup, mPrefRecovery;
//	private Preference mPrefThemeWidget;
	private ImageButton mBtnMemo, mBtnAuthor, mBtnCategory, mBtnFavorite, mBtnSetting;
	private QuickAction mQuickShare;
	
	private int mCurrentLngIndex, mTempLngIndex;
	private int mWidgetUpdateIndex, mTempWidgetUpdateIndex;
	private int mWidgetDisplayIndex, mTempWidgetDisplayInex;
	private ArrayList<String> mFilterFileName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preference);
		setContentView(R.layout.setting);
		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		this.mMessagePool = (MessagePool)this.getApplicationContext();
		
		mPrefUpdateCategory = (Preference)findPreference("pref_update_category");
		mPrefTagChange = (Preference)findPreference("pref_tag_change");
		mPrefLngChange = (Preference)findPreference("pref_lng_change");
		mPrefWidgetDisplay = (Preference)findPreference("pref_widget_display");
		mPrefWidgetUpdate = (Preference)findPreference("pref_widget_update");
//		mPrefThemeWidget = (Preference)findPreference("pref_theme_widget");
		mPrefBackup = (Preference)findPreference("pref_memo_backup");
		mPrefRecovery = (Preference)findPreference("pref_memo_recovery");
		
		// Resource Id 설정
		mBtnMemo = (ImageButton)findViewById(R.id.main_btn_memo);
		mBtnAuthor = (ImageButton)findViewById(R.id.main_btn_author);
		mBtnCategory = (ImageButton)findViewById(R.id.main_btn_category);
		mBtnFavorite = (ImageButton)findViewById(R.id.main_btn_favorite);
		mBtnSetting = (ImageButton)findViewById(R.id.main_btn_setting);

		// 메뉴바 아이콘 설정
		mBtnMemo.setBackgroundResource(R.drawable.main_menu_icon_today_off);
		mBtnSetting.setBackgroundResource(R.drawable.main_menu_icon_settings_on);
		
		// Event Listener 설정
		mPrefUpdateCategory.setOnPreferenceClickListener(this);
		mPrefTagChange.setOnPreferenceClickListener(this);
		mPrefLngChange.setOnPreferenceClickListener(this);
		mPrefBackup.setOnPreferenceClickListener(this);
		mPrefRecovery.setOnPreferenceClickListener(this);
//		mPrefThemeWidget.setOnPreferenceClickListener(this);
		mPrefWidgetDisplay.setOnPreferenceClickListener(this);
		mPrefWidgetUpdate.setOnPreferenceClickListener(this);
		
		mBtnMemo.setOnClickListener(this);
		mBtnAuthor.setOnClickListener(this);
		mBtnCategory.setOnClickListener(this);
		mBtnFavorite.setOnClickListener(this);
		mBtnSetting.setOnClickListener(this);
		
		// 태그 설정 정보 로드
		updateTagChange(mConfigPref.getInt(Constants.CONFIG_PREF_TAG_CHANGE, PREF_TAG_CHANGE_ON));
		
		// 언어 설정 정보 로드
		mCurrentLngIndex = mConfigPref.getInt(Constants.CONFIG_PREF_LNG_INDEX, PREF_LNG_CHANGE_KOR); 
		mTempLngIndex = mCurrentLngIndex;
		updateLngChange(mCurrentLngIndex);

		// 위젯 표시 정보 로드
		mWidgetDisplayIndex = mConfigPref.getInt(Constants.CONFIG_PREF_WIDGET_DISPLAY_INDEX, PREF_WIDGET_DISPLAY_DAILY);
		mTempWidgetDisplayInex = mWidgetDisplayIndex;
		updateWidgetDisplay(mWidgetDisplayIndex);
		
		// 위젯 업데이트 주기 정보 로드
		mWidgetUpdateIndex = mConfigPref.getInt(Constants.CONFIG_PREF_WIDGET_UPDATE_INDEX, PREF_WIDGET_UPDATE_DAILY);
		mTempWidgetUpdateIndex = mWidgetUpdateIndex;
		updateWidgetPeriod(mWidgetUpdateIndex);
		
		
	}
	
	@Override
	protected void onDestroy() {
		
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	private void updateTagChange(int index) {
		switch(index) {
			case PREF_TAG_CHANGE_ON:
				mPrefTagChange.setTitle(getString(R.string.pref_tag_change_on));
				break;
				
			case PREF_TAG_CHANGE_OFF:
				mPrefTagChange.setTitle(getString(R.string.pref_tag_change_off));
				
				break;
		}
	}
	
	private void updateLngChange(int index) {
		switch(index) {
			case PREF_LNG_CHANGE_KOR:
				mPrefLngChange.setTitle(getString(R.string.pref_lng_kor));
				break;
			case PREF_LNG_CHANGE_ENG:
				mPrefLngChange.setTitle(getString(R.string.pref_lng_eng));
				break;
		}
	}
	
	private void updateWidgetPeriod(int index) {
		switch(index) {
			case PREF_WIDGET_UPDATE_DAILY:
				mPrefWidgetUpdate.setTitle(getString(R.string.pref_widget_update_title_daily));
				
				break;
			case PREF_WIDGET_UPDATE_3_HOUR:
				mPrefWidgetUpdate.setTitle(getString(R.string.pref_widget_update_title_3_hour));
				
				break;
			case PREF_WIDGET_UPDATE_6_HOUR:
				mPrefWidgetUpdate.setTitle(getString(R.string.pref_widget_update_title_6_hour));
				
				break;
			case PREF_WIDGET_UPDATE_12_HOUR:
				mPrefWidgetUpdate.setTitle(getString(R.string.pref_widget_update_title_12_hour));
				
				break;
		}
	}
	
	private void updateWidgetDisplay(int index) {
		switch(index) {
			case PREF_WIDGET_DISPLAY_DAILY:
				mPrefWidgetDisplay.setTitle(getString(R.string.pref_widget_display_title_daily));
				break;
			case PREF_WIDGET_DISPLAY_FAVORITE:
				mPrefWidgetDisplay.setTitle(getString(R.string.pref_widget_display_title_favorite));
				break;
		}
	}
	
	public boolean onPreferenceClick(Preference preference) {
		if(preference.getKey().equals("pref_update_category")) {

			ArrayList<CategoryList> categoryArray = Utils.getCategoryArrayList(mContext);
			
			int ctSize = categoryArray.size();
			final String[] categoryList = new String[ctSize];
			final boolean[] originSelectList = new boolean[ctSize];
			final boolean[] copySelectList = new boolean[ctSize];
			
			for(int i=0; i < ctSize; i++) {
				categoryList[i] = categoryArray.get(i).getKName();
				originSelectList[i] = categoryArray.get(i).getEnableFlag();
				copySelectList[i] = originSelectList[i];
			}
			
			
			final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			
			builder.setTitle(getString(R.string.pref_update_category));
			builder.setMultiChoiceItems(categoryList, copySelectList, new OnMultiChoiceClickListener() {
				public void onClick(DialogInterface dialog, int which, boolean isChecked) {
					copySelectList[which] = isChecked;
				}
			});
			
			builder.setPositiveButton(getString(R.string.etc_btn_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					
					boolean changeFlag = false;

					for(int i=0; i < originSelectList.length; i++) {
						if(originSelectList[i] != copySelectList[i]) {
							changeFlag = true;
							break;
						}
					}
					
					if(changeFlag) {
						boolean integrityFlag = false;
					
						for(int i=0; i < originSelectList.length; i++) {
							if(copySelectList[i]) {
								integrityFlag = true;
								break;
							}
						}
					
						if(integrityFlag) {			// 카테고리 설정을 변경
							
							UpdateRegistDataAsync updateRegisteDataAsync = new UpdateRegistDataAsync(categoryList, copySelectList);
							updateRegisteDataAsync.execute();
							
						} else {
							Toast.makeText(mContext, getString(R.string.pref_toast_caution_category), Toast.LENGTH_SHORT).show();
							
							// 원래 상태로 복구
							for(int i=0; i < originSelectList.length; i++) {
								copySelectList[i] = originSelectList[i];
							}
							
							builder.show();
							
						}
					}
					
				}
			});
			
			builder.setNegativeButton(getString(R.string.etc_btn_cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {}
			});
			
			builder.show();
			
			
		} else if(preference.getKey().equals("pref_tag_change")) {
			
			String[] items = {getString(R.string.pref_tag_change_on), getString(R.string.pref_tag_change_off)};
			
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setTitle(getString(R.string.pref_fnc_title_tag));
			builder.setSingleChoiceItems(items, 
					mConfigPref.getInt(Constants.CONFIG_PREF_TAG_CHANGE, PREF_TAG_CHANGE_ON), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					SharedPreferences.Editor editor = mConfigPref.edit();
					editor.putInt(Constants.CONFIG_PREF_TAG_CHANGE, which);
					editor.commit();
					
					updateTagChange(which);
					
					dialog.dismiss();
				}
			});
			
			builder.show();
			
		} else if(preference.getKey().equals("pref_lng_change")) {
			
			
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setTitle(getString(R.string.pref_fnc_title_lng));
			builder.setSingleChoiceItems(getResources().getTextArray(R.array.lng_array), mCurrentLngIndex, 
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							mTempLngIndex = which;
						}
					});
			
			builder.setPositiveButton(getString(R.string.etc_btn_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if(mCurrentLngIndex != mTempLngIndex) {
						
						// 언어 설정을 변경
						SharedPreferences.Editor configEditor = mConfigPref.edit();
						configEditor.putInt(Constants.CONFIG_PREF_LNG_INDEX, mTempLngIndex);
						configEditor.commit();
						
						mCurrentLngIndex = mTempLngIndex;
						updateLngChange(mCurrentLngIndex);
						
						// 전체 언어 시스템을 변경해준다. (구현 해야 함)
						Utils.setLocale(mContext, mCurrentLngIndex);
						
						Utils.sendBroadCasting(mContext, Constants.ACTION_WIDGET_UPDATE_SAYING);
					}
				}
			});
			
			builder.setNegativeButton(getString(R.string.etc_btn_cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			
			builder.show();
			
		} else if(preference.getKey().equals("pref_memo_backup")) {
			
			// 백업 코드 실행
			if(Utils.isSDCardMounted()) {
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
				builder.setTitle(getString(R.string.pref_backup_title));
				builder.setMessage(getString(R.string.pref_backup_message));
				builder.setPositiveButton(getString(R.string.etc_btn_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						DataBackupRecoveryAsync dataBackUpAsync = new DataBackupRecoveryAsync(PREF_DATA_BACKUP);
						dataBackUpAsync.execute();
					}
				});
				builder.setNegativeButton(getString(R.string.etc_btn_cancel), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {}});
				
				builder.show();
			} else {
				Toast.makeText(mContext, getString(R.string.pref_sdcard_unmounted), Toast.LENGTH_SHORT).show();
			}
			
			
		} else if(preference.getKey().equals("pref_memo_recovery")) {
			
			// 복구 코드 실행
			
			if(Utils.isSDCardMounted()) {
			
				// 파일이 존재하는지 확인
				String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();
				String targetPath = sdCardPath + Constants.BACKUP_PATH;
				
				File fileList = new File(targetPath);
				
				if(fileList.listFiles() == null) {
					Toast.makeText(mContext, getString(R.string.pref_recovery_not_exist), Toast.LENGTH_SHORT).show();
				} else {
					// 먼저 디렉토리에 있는 파일명을 읽어온다.
					
					ArrayList<String> fileName = new ArrayList<String>();
					mFilterFileName = new ArrayList<String>();
					
					for(File file : fileList.listFiles()) {
						fileName.add(file.getName());
					}
					
					// 조건에 따라, 정확한 복원 파일을 필터링 해서 저장한다.
					mFilterFileName = Utils.filterFileName(fileName);
					final String[] listfileItems = Utils.splitFileName(mContext, mFilterFileName);
					
					if(listfileItems.length != 0) {
					
						AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
						builder.setTitle(getString(R.string.pref_recovery_title));
						builder.setItems(listfileItems, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, final int position) {
								AlertDialog.Builder confirmDialog = new AlertDialog.Builder(mContext);
								confirmDialog.setTitle(getString(R.string.pref_recovery_title));
								confirmDialog.setMessage(listfileItems[position] + getString(R.string.pref_recovery_message));
								confirmDialog.setPositiveButton(getString(R.string.etc_btn_ok), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										Utils.logPrint(getClass(), "복구할 파일 명 : " + mFilterFileName.get(position));
										
										DataBackupRecoveryAsync dataRecoverAsync = 
											new DataBackupRecoveryAsync(PREF_DATA_RECOVERY, mFilterFileName.get(position));
										dataRecoverAsync.execute();
									}
								});
								
								confirmDialog.setNegativeButton(getString(R.string.etc_btn_cancel), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {}});
								
								confirmDialog.show();
							}
						});
						
						builder.show();
					} else {
						Toast.makeText(mContext, getString(R.string.pref_recovery_toast_not_exist), Toast.LENGTH_SHORT).show();
					}
				} 
			} else {
				Toast.makeText(mContext, getString(R.string.pref_sdcard_unmounted), Toast.LENGTH_SHORT).show();
			}
			
			
		} else if(preference.getKey().equals("pref_widget_display")) {
			// 위젯 표시 설정
			
			String[] displayItems = getResources().getStringArray(R.array.pref_widget_display_array);
			
			final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setTitle(getString(R.string.pref_widget_display_title));
			builder.setSingleChoiceItems(displayItems, mWidgetDisplayIndex, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					mTempWidgetDisplayInex = which;
				}
			});
			builder.setPositiveButton(getString(R.string.etc_btn_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					
					if(mWidgetDisplayIndex != mTempWidgetDisplayInex) {
						
						ArrayList<FamousData> items = Utils.getFavoriteData(mContext);
						
						if(items.isEmpty()) {
							Toast.makeText(mContext, getString(R.string.pref_widget_display_toast), Toast.LENGTH_SHORT).show();
							
							builder.show();
						} else {
							mWidgetDisplayIndex = mTempWidgetDisplayInex;
							
							Utils.logPrint(getClass(), "위젯 표시 설정이 변경이 변경 (변경 값 : " + mWidgetDisplayIndex + ")");
							
							SharedPreferences.Editor configEditor = mConfigPref.edit();
							configEditor.putInt(Constants.CONFIG_PREF_WIDGET_DISPLAY_INDEX, mWidgetDisplayIndex);
							configEditor.commit();
							
							updateWidgetDisplay(mWidgetDisplayIndex);
							
							// 같은 값을 설정했을 시 실행하지 않도록 하는 코드 추가해야 합니다.
							// 위젯에 업데이트 하는 값을 변경한다. (여기에 변경 코드를 넣어줘야 할 듯)
							switch(mWidgetDisplayIndex) {
								case Setting.PREF_WIDGET_DISPLAY_DAILY:
									Utils.getMainRandomIndex(mContext, Setting.PREF_WIDGET_ONLY_UPDATE);
									
									break;
									
								case Setting.PREF_WIDGET_DISPLAY_FAVORITE:
									Utils.updateFavoriteWidgetData(mContext, items);
									
									break;
							}
						}
					}
				}
			});
			builder.setNegativeButton(getString(R.string.etc_btn_cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					mTempWidgetDisplayInex = mWidgetDisplayIndex;
				}}
			);
			
			builder.show();
			
			
			
			
		} else if(preference.getKey().equals("pref_widget_update")) {		
			// 위젯 업데이트 설정
			
			String[] updateItems = getResources().getStringArray(R.array.pref_widget_update_array);
			
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setTitle(getString(R.string.pref_widget_update_title));
			builder.setSingleChoiceItems(updateItems, mWidgetUpdateIndex, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					mTempWidgetUpdateIndex = which;
				}
			});
			builder.setPositiveButton(getString(R.string.etc_btn_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if(mWidgetUpdateIndex != mTempWidgetUpdateIndex) {
					
						mWidgetUpdateIndex = mTempWidgetUpdateIndex;
						
						Utils.logPrint(getClass(), "위젯 업데이트 주기 변경 (변경 값 : " + mWidgetUpdateIndex + ")");
						
						SharedPreferences.Editor configEditor = mConfigPref.edit();
						configEditor.putInt(Constants.CONFIG_PREF_WIDGET_UPDATE_INDEX, mWidgetUpdateIndex);
						configEditor.commit();
						
						updateWidgetPeriod(mWidgetUpdateIndex);
						
						// 기존에 존재하는 알람 취소하고, 재설정 합니다.
						DailyAlarmService.reRegisteUpdateService(mContext, mWidgetUpdateIndex);
						
						Toast.makeText(mContext, getString(R.string.pref_widget_update_toast), Toast.LENGTH_SHORT).show();
						
					}
				}
			});
			builder.setNegativeButton(getString(R.string.etc_btn_cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					mTempWidgetUpdateIndex = mWidgetUpdateIndex;
				}
			});
			
			builder.show();
			
			
		} else if(preference.getKey().equals("pref_theme_widget")) {
			// 위젯 테마 설정
		} 
		
		
		return false;
	}
	

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.main_btn_memo:
				Intent mainIntent = new Intent(Setting.this, MainMenu.class);
				startActivity(mainIntent);
				Utils.finishActivity(this);
				
				break;
			case R.id.main_btn_author:
				Intent authorIntent = new Intent(Setting.this, AuthorList.class);
				startActivity(authorIntent);
				Utils.finishActivity(this);
				
				break;
			case R.id.main_btn_category:
				Intent listIntent = new Intent(Setting.this, Category.class);
				startActivity(listIntent);
				Utils.finishActivity(this);
				
				break;
			case R.id.main_btn_favorite:
				if(Utils.isFavoriteEmpty(mContext)) {
					Toast.makeText(mContext, getString(R.string.favorite_toast_empty), Toast.LENGTH_SHORT).show();
				} else {
					Intent favoriteIntent = new Intent(Setting.this, Favorite.class);
					startActivity(favoriteIntent);
					Utils.finishActivity(this);
				}
				
				break;
		}
	}

	class UpdateRegistDataAsync extends AsyncTask<Void, Void, Void> {
		
		ProgressDialog prgDig;
		
		String[] categoryList;
		boolean[] copySelectList;
		
		public UpdateRegistDataAsync(String[] categoryList, boolean[] copySelectList) {
			this.categoryList = categoryList;
			this.copySelectList = copySelectList;
		}
		
		@Override
		protected void onPreExecute() {
			prgDig = ProgressDialog.show(mContext, "", getString(R.string.pref_prg_chage_data));
		}
		
		
		@Override
		protected Void doInBackground(Void... params) {
			// db 에 저장
			Utils.updateCategoryEnable(mContext, categoryList, copySelectList);
			
			// 변경된 카테고리에 따른 Data Array 를 재등록
			mMessagePool.setDataArray(Utils.getFamouseData(mContext));
			
			SharedPreferences.Editor configEditor = mConfigPref.edit();
			configEditor.putInt(Constants.CONFIG_PREF_TODAY_INDEX, Utils.getMainRandomIndex(mContext, Setting.PREF_ALL_UPDATE));
			configEditor.commit();
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(prgDig.isShowing()) {
				prgDig.dismiss();
			}
			
			Toast.makeText(mContext, getString(R.string.pref_toast_complete_change), Toast.LENGTH_SHORT).show();
		}
	}
	
	class DataBackupRecoveryAsync extends AsyncTask<Void, Void, Void> {
		
		ProgressDialog prgDig;
		boolean succeedFlag;
		int stateFlag;
		String restoreFileName;
		
		public DataBackupRecoveryAsync(int state) {
			this.stateFlag = state;
		}
		
		public DataBackupRecoveryAsync(int state, String restoreFileName) {
			this.stateFlag = state;
			this.restoreFileName = restoreFileName;
		}
		
		@Override
		protected void onPreExecute() {
			
			switch(stateFlag) {
				case PREF_DATA_BACKUP:
					prgDig = ProgressDialog.show(mContext, "", "데이터를 백업 중입니다. 잠시만 기다려 주세요.");
					break;
				case PREF_DATA_RECOVERY:
					prgDig = ProgressDialog.show(mContext, "", "데이터를 복구 중입니다. 잠시만 기다려 주세요.");
					break;
			}
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			
			switch(stateFlag) {
				case PREF_DATA_BACKUP:
					try {
						succeedFlag = Utils.dataBackUp(mContext);
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;

				case PREF_DATA_RECOVERY:
					Utils.logPrint(getClass(), "복구할 파일 명 : " + restoreFileName);
					
					succeedFlag = Utils.dataRestore(mContext, restoreFileName);	
					
					// 현재 메시지 풀에 저장된 명언 데이터 재설정
					if(succeedFlag) {
						mMessagePool.setDataArray(Utils.getFamouseData(mContext));
					}
					
					break;
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(prgDig.isShowing()) {
				prgDig.dismiss();
			}
			
			Utils.logPrint(getClass(), "성공 유무 : " + succeedFlag);
			
			switch(stateFlag) {
				case PREF_DATA_BACKUP:
					if(succeedFlag) {
						Toast.makeText(mContext, getString(R.string.pref_backup_toast_complete), Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(mContext, getString(R.string.pref_backup_toast_failed), Toast.LENGTH_SHORT).show();
					}
					
					break;
				case PREF_DATA_RECOVERY:
					if(succeedFlag) {
						Toast.makeText(mContext, getString(R.string.pref_recovery_toast_complete), Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(mContext, getString(R.string.pref_recovery_toast_failed), Toast.LENGTH_SHORT).show();
					}
					
					
					break;
			}
			
		}
	}

}










