package madcat.studio.tagsaying;

import java.util.ArrayList;

import madcat.studio.adapter.AdaperCategoryList;
import madcat.studio.constants.Constants;
import madcat.studio.data.CategoryList;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

public class Category extends ListActivity implements OnClickListener {
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private AdaperCategoryList mAdapter;
	private ArrayList<CategoryList> mItems;
	
	private ImageButton mBtnMemo, mBtnAuthor, mBtnCategory, mBtnFavorite, mBtnSetting;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.category);
		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		// Resource Id 설정
		mBtnMemo = (ImageButton)findViewById(R.id.main_btn_memo);
		mBtnAuthor = (ImageButton)findViewById(R.id.main_btn_author);
		mBtnCategory = (ImageButton)findViewById(R.id.main_btn_category);
		mBtnFavorite = (ImageButton)findViewById(R.id.main_btn_favorite);
		mBtnSetting = (ImageButton)findViewById(R.id.main_btn_setting);

		// 메인 메뉴 아이콘 설정
		mBtnMemo.setBackgroundResource(R.drawable.main_menu_icon_today_off);
		mBtnCategory.setBackgroundResource(R.drawable.main_menu_icon_category_on);
		
		mBtnMemo.setOnClickListener(this);
		mBtnAuthor.setOnClickListener(this);
		mBtnCategory.setOnClickListener(this);
		mBtnFavorite.setOnClickListener(this);
		mBtnSetting.setOnClickListener(this);

		// Adapter 설정
		updateList();
		
	}
	
	@Override
	protected void onDestroy() {
		
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	private void updateList() {
		if(mAdapter != null) {
			mAdapter.clear();
		}

		this.mItems = Utils.getCategoryArrayList(mContext);
		this.mAdapter = new AdaperCategoryList(mContext, R.layout.category_row, mItems, 
				mConfigPref.getInt(Constants.CONFIG_PREF_LNG_INDEX, Setting.PREF_LNG_CHANGE_KOR));
		setListAdapter(mAdapter);
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.main_btn_memo:		//	오늘의 명언 메인
				Intent todayIntent = new Intent(Category.this, MainMenu.class);
				startActivity(todayIntent);
				Utils.finishActivity(this);
				
				
				break;
			
			case R.id.main_btn_author:
				Intent authorIntent = new Intent(Category.this, AuthorList.class);
				startActivity(authorIntent);
				Utils.finishActivity(this);
				
				break;
				
			case R.id.main_btn_favorite:
				if(Utils.isFavoriteEmpty(mContext)) {
					Toast.makeText(mContext, getString(R.string.favorite_toast_empty), Toast.LENGTH_SHORT).show();
				} else {
					Intent favoriteIntent = new Intent(Category.this, Favorite.class);
					startActivity(favoriteIntent);
					Utils.finishActivity(this);
				}
				break;
				
			case R.id.main_btn_setting:
				Intent settingIntent = new Intent(Category.this, Setting.class);
				startActivity(settingIntent);
				Utils.finishActivity(this);
				
				break;
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		
		Utils.logPrint(getClass(), "클릭 된 position : " + position);
		
		Intent detailIntent = new Intent(Category.this, CategoryDetail.class);
		detailIntent.putExtra(Constants.PUT_FAMOUS_CATEGORY, mItems.get(position).getKName());
		startActivity(detailIntent);
		Utils.finishActivity(this);
		
		super.onListItemClick(l, v, position, id);
	}

}
