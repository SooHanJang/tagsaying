package madcat.studio.tagsaying;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.FamousData;
import madcat.studio.data.MemoListData;
import madcat.studio.dialog.MemoDialog;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MemoList extends ListActivity implements OnClickListener {
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	private MessagePool mMessagePool;
	
	private ArrayList<MemoListData> mItems;
	
	private AdapterMemoList mAdapter;

	private ImageButton mBtnMemo, mBtnAuthor, mBtnCategory, mBtnFavorite, mBtnSetting;
	
	private int mLngType;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.memo_list);
		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		this.mMessagePool = (MessagePool) getApplicationContext();
		
		// Resource Id 설정
		mBtnMemo = (ImageButton)findViewById(R.id.main_btn_memo);
		mBtnAuthor = (ImageButton)findViewById(R.id.main_btn_author);
		mBtnCategory = (ImageButton)findViewById(R.id.main_btn_category);
		mBtnFavorite = (ImageButton)findViewById(R.id.main_btn_favorite);
		mBtnSetting = (ImageButton)findViewById(R.id.main_btn_setting);

		// 메뉴바 아이콘 설정
		mBtnMemo.setBackgroundResource(R.drawable.main_menu_icon_memo_on);
		
		// Event Listener 설정
		mBtnMemo.setOnClickListener(this);
		mBtnAuthor.setOnClickListener(this);
		mBtnCategory.setOnClickListener(this);
		mBtnFavorite.setOnClickListener(this);
		mBtnSetting.setOnClickListener(this);
		
		// 언어 설정
		mLngType = mConfigPref.getInt(Constants.CONFIG_PREF_LNG_INDEX, Setting.PREF_LNG_CHANGE_KOR);
		
		// Adapter 설정
		updateMemoAdapterList();
		
	}
	
	@Override
	protected void onDestroy() {
		
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	private void updateMemoAdapterList() {
		if(mAdapter != null) {
			mAdapter.clear();
		}

		mItems = Utils.getMemoListData(mContext);
		
		mAdapter = new AdapterMemoList(mContext, R.layout.memo_list_row, mItems, mLngType);
		setListAdapter(mAdapter);
	}
	
	public void onClick(final View v) {
		
		final ArrayList<MemoListData> unCheckedItems = mAdapter.getUnCheckedItem();
		
		if(unCheckedItems.isEmpty()) {
			startMenu(v);
		} else {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setTitle(getString(R.string.memo_list_change));
			builder.setMessage(getString(R.string.favorite_dig_msg));
			builder.setPositiveButton(getString(R.string.etc_btn_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					
					ChangeCheckedDataAsync changeDataAsync = new ChangeCheckedDataAsync(v, unCheckedItems);
					changeDataAsync.execute();
					
					
				}
			});
			
			builder.setNegativeButton(getString(R.string.etc_btn_cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					startMenu(v);
				}
			});
			
			builder.show();
		}
		
	}
	
	private void startMenu(View v) {
		switch(v.getId()) {
			case R.id.main_btn_memo:		//	오늘의 명언 메인
				Intent todayIntent = new Intent(MemoList.this, MainMenu.class);
				startActivity(todayIntent);
				Utils.finishActivity(this);
				
				break;
				
			case R.id.main_btn_author:
				Intent authorIntent = new Intent(MemoList.this, AuthorList.class);
				startActivity(authorIntent);
				Utils.finishActivity(this);
				
				break;
				
			case R.id.main_btn_category:
				Intent listIntent = new Intent(MemoList.this, Category.class);
				startActivity(listIntent);
				Utils.finishActivity(this);
				break;
				
			case R.id.main_btn_favorite:
				if(Utils.isFavoriteEmpty(mContext)) {
					Toast.makeText(mContext, getString(R.string.favorite_toast_empty), Toast.LENGTH_SHORT).show();
				} else {
					Intent favoriteIntent = new Intent(MemoList.this, Favorite.class);
					startActivity(favoriteIntent);
					Utils.finishActivity(this);
				}
				break;
			
			case R.id.main_btn_setting:
				Intent settingIntent = new Intent(MemoList.this, Setting.class);
				startActivity(settingIntent);
				Utils.finishActivity(this);
				break;
		}
	}
	
	@Override
	public void onBackPressed() {
		Intent todayIntent = new Intent(MemoList.this, MainMenu.class);
		startActivity(todayIntent);

		Utils.finishActivity(this);
		
	}
	
	class ChangeCheckedDataAsync extends AsyncTask<Void, Void, Void> {

		ProgressDialog proDig;
		View v;
		ArrayList<MemoListData> unCheckedItems;
		
		
		public ChangeCheckedDataAsync(View v, ArrayList<MemoListData> unCheckedItems) {
			this.v = v;
			this.unCheckedItems = unCheckedItems;
		}
		
		@Override
		protected void onPreExecute() {
			proDig = ProgressDialog.show(mContext, "", getString(R.string.favorite_prg_title));
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			ArrayList<FamousData> famousData = mMessagePool.getDataArray();
			
			for(int i=0; i < unCheckedItems.size(); i++) {
				for(int j=0; j < famousData.size(); j++) {
					if(unCheckedItems.get(i).getFamousData().getId() == famousData.get(j).getId()) {
						famousData.get(j).setMemoEnable(false);
					}
				}
			}
			
			Utils.deleteMemoEnableToDb(mContext, unCheckedItems, false);
			
			mMessagePool.setDataArray(famousData);
			
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(proDig.isShowing()) {
				proDig.dismiss();
			}
			
			startMenu(v);
		}
	}

	/*
	 * Inner 클래스로 설계
	 *  > 체크 박스 이벤트가 선행되어, 리스트 아이템 클릭 이벤트를 무시하며, 이벤트를 직접 외부 클래스로 설계시 어댑터 리플레쉬가 불가능 하기 때문이다.
	 */
	class AdapterMemoList extends ArrayAdapter<MemoListData> {
		
		private Context mContext;
		private int mResId, mLngType;
		private ArrayList<MemoListData> mItems;
		private LayoutInflater mInflater;
		
		private ArrayList<MemoListData> mUnCheckedItems;
		
		public AdapterMemoList(Context context, int resId, ArrayList<MemoListData> items, int lngType) {
			super(context, resId, items);
			
			this.mContext = context;
			this.mResId = resId;
			this.mItems = items;
			this.mLngType = lngType;
			this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.mUnCheckedItems = new ArrayList<MemoListData>();
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			
			if(convertView == null) {
				convertView = mInflater.inflate(mResId, null);
				
				holder = new ViewHolder();
				
				holder.liLayout = (LinearLayout)convertView.findViewById(R.id.memo_list_linear);
				holder.tvSaying = (TextView)convertView.findViewById(R.id.memo_list_saying);
				holder.tvAuthor = (TextView)convertView.findViewById(R.id.memo_list_author);
				holder.tvDate = (TextView)convertView.findViewById(R.id.memo_list_date);
				holder.cbChecked = (CheckBox)convertView.findViewById(R.id.memo_list_checked);
				
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			switch(mLngType) {
				case Setting.PREF_LNG_CHANGE_KOR:
					holder.tvSaying.setText(mItems.get(position).getFamousData().getKText());
					holder.tvAuthor.setText(mItems.get(position).getFamousData().getKAuthor());
					
					break;
					
				case Setting.PREF_LNG_CHANGE_ENG:
					holder.tvSaying.setText(mItems.get(position).getFamousData().getEText());
					holder.tvAuthor.setText(mItems.get(position).getFamousData().getEAuthor());
					
					break;
			}
			
			holder.tvDate.setText(" [" + mItems.get(position).getMemoData().getMemoDate() + " " + 
					mContext.getString(R.string.memo_list_written_postfix) + " ]");
			
			if(mItems.get(position).getFamousData().getMemoFlagEnable()) {
				holder.cbChecked.setChecked(true);
			} else {
				holder.cbChecked.setChecked(false);
			}
			
			holder.liLayout.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					final MemoDialog memoDialog = new MemoDialog(mContext, mItems.get(position).getFamousData(), mItems.get(position).getMemoData(),
					MemoDialog.DIALOG_MODE_VIEWER, mLngType);
					memoDialog.show();
					
					memoDialog.setOnDismissListener(new OnDismissListener() {
						public void onDismiss(DialogInterface dialog) {
							if(memoDialog.getDialogState() == MemoDialog.DIALOG_OK) {
								updateMemoAdapterList();
							}
						}
					});
				}
			});
			
			holder.cbChecked.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					
					if(holder.cbChecked.isChecked()) {
						mItems.get(position).getFamousData().setMemoEnable(true);
						
						for(int i=0; i < mUnCheckedItems.size(); i++) {
							if(mItems.get(position).getFamousData().getId() == mUnCheckedItems.get(i).getFamousData().getId()) {
								mUnCheckedItems.remove(i);
							}
						}
						
						
					} else {
						mItems.get(position).getFamousData().setMemoEnable(false);

						mUnCheckedItems.add(mItems.get(position));
						
					}
					
				}
			});
			
			return convertView;
		}
		
		public ArrayList<MemoListData> getUnCheckedItem() {
			return mUnCheckedItems;
		}
		
		class ViewHolder {
			private LinearLayout liLayout;
			private TextView tvSaying;
			private TextView tvAuthor;
			private TextView tvDate;
			private CheckBox cbChecked;
		}

	}


}



















