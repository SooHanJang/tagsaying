package madcat.studio.tagsaying;

import java.util.ArrayList;

import madcat.studio.adapter.AdapterCategoryDetailList;
import madcat.studio.constants.Constants;
import madcat.studio.data.FamousData;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

public class CategoryDetail extends ListActivity implements OnClickListener {
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	private String mReceiveCategory;
	
	private ImageButton mBtnMemo, mBtnAuthor, mBtnCategory, mBtnFavorite, mBtnSetting;
	
	private ArrayList<FamousData> mItems;
	private AdapterCategoryDetailList mDetailAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.category_detail);
		
		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		this.getIntenter();
		
		// Resource Id 설정
		mBtnMemo = (ImageButton)findViewById(R.id.main_btn_memo);
		mBtnAuthor = (ImageButton)findViewById(R.id.main_btn_author);
		mBtnCategory = (ImageButton)findViewById(R.id.main_btn_category);
		mBtnFavorite = (ImageButton)findViewById(R.id.main_btn_favorite);
		mBtnSetting = (ImageButton)findViewById(R.id.main_btn_setting);

		// 메인 메뉴 아이콘 설정
		mBtnMemo.setBackgroundResource(R.drawable.main_menu_icon_today_off);
		mBtnCategory.setBackgroundResource(R.drawable.main_menu_icon_category_on);
		
		// Event Listener 설정
		mBtnMemo.setOnClickListener(this);
		mBtnAuthor.setOnClickListener(this);
		mBtnCategory.setOnClickListener(this);
		mBtnFavorite.setOnClickListener(this);
		mBtnSetting.setOnClickListener(this);
		
		
		// Adapter 설정
		updateList();
		
	}
	
	@Override
	protected void onDestroy() {
		
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	private void updateList() {
		if(mDetailAdapter != null) {
			mDetailAdapter.clear();
		}

		this.mItems = Utils.getCategoryArrayData(mContext, mReceiveCategory);
		this.mDetailAdapter = new AdapterCategoryDetailList(mContext, R.layout.category_detail_row,	mItems, 
				mConfigPref.getInt(Constants.CONFIG_PREF_LNG_INDEX, Setting.PREF_LNG_CHANGE_KOR));
		setListAdapter(mDetailAdapter);
	}
	
	private void getIntenter() {
		Intent intent = this.getIntent();
		
		if(intent != null) {
			mReceiveCategory = intent.getExtras().getString(Constants.PUT_FAMOUS_CATEGORY);
		}
	}
	
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.main_btn_memo:
				Intent mainIntent = new Intent(CategoryDetail.this, MainMenu.class);
				startActivity(mainIntent);
				Utils.finishActivity(this);
				
				break;
			case R.id.main_btn_author:
				Intent authorIntent = new Intent(CategoryDetail.this, AuthorList.class);
				startActivity(authorIntent);
				Utils.finishActivity(this);
				
				break;
				
			case R.id.main_btn_category:
				Intent listIntent = new Intent(CategoryDetail.this, Category.class);
				startActivity(listIntent);
				Utils.finishActivity(this);
				
				break;
			case R.id.main_btn_favorite:
				if(Utils.isFavoriteEmpty(mContext)) {
					Toast.makeText(mContext, getString(R.string.favorite_toast_empty), Toast.LENGTH_SHORT).show();
				} else {
					Intent favoriteIntent = new Intent(CategoryDetail.this, Favorite.class);
					startActivity(favoriteIntent);
					Utils.finishActivity(this);
				}
				
				break;
				
			case R.id.main_btn_setting:
				Intent settingIntent = new Intent(CategoryDetail.this, Setting.class);
				startActivity(settingIntent);
				Utils.finishActivity(this);
				
				break;
		}
	}

	@Override
	public void onBackPressed() {
		Intent listIntent = new Intent(CategoryDetail.this, Category.class);
		startActivity(listIntent);
		Utils.finishActivity(this);
	}
}





