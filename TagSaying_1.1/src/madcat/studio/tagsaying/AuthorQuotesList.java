package madcat.studio.tagsaying;

import java.util.ArrayList;

import madcat.studio.adapter.AdapterAuthorQuotesList;
import madcat.studio.constants.Constants;
import madcat.studio.data.AuthorData;
import madcat.studio.data.FamousData;
import madcat.studio.dialog.AuthorInfoDialog;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class AuthorQuotesList extends ListActivity implements OnClickListener {
	
	private Context mContext;
	private MessagePool mMessagePool;
	
	private ArrayList<FamousData> mItems;
	private AdapterAuthorQuotesList mAdpater;
	
	private String mAuthor;
	private AuthorData mAuthorData;
	private int mLngType;
	
	private ImageButton mBtnAuthorInfo;
	
	private TextView mTvAuthor;
	private ImageButton mBtnMemo, mBtnAuthor, mBtnCategory, mBtnFavorite, mBtnSetting;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.author_quotes_list);
		this.mContext = this;
		this.mMessagePool = (MessagePool) getApplicationContext();
		this.getIntenter();
		
		// Resource Id 설정
		mTvAuthor = (TextView)findViewById(R.id.author_quotes_name);
		
		mBtnAuthorInfo = (ImageButton)findViewById(R.id.author_quotes_info);
		
		mBtnMemo = (ImageButton)findViewById(R.id.main_btn_memo);
		mBtnAuthor = (ImageButton)findViewById(R.id.main_btn_author);
		mBtnCategory = (ImageButton)findViewById(R.id.main_btn_category);
		mBtnFavorite = (ImageButton)findViewById(R.id.main_btn_favorite);
		mBtnSetting = (ImageButton)findViewById(R.id.main_btn_setting);

		// 메인 메뉴 아이콘 설정
		mBtnMemo.setBackgroundResource(R.drawable.main_menu_icon_today_off);
		mBtnAuthor.setBackgroundResource(R.drawable.main_menu_icon_author_on);
		
		// Event Listener 설정
		mBtnAuthorInfo.setOnClickListener(this);
		
		mBtnMemo.setOnClickListener(this);
		mBtnAuthor.setOnClickListener(this);
		mBtnCategory.setOnClickListener(this);
		mBtnFavorite.setOnClickListener(this);
		mBtnSetting.setOnClickListener(this);
		
		mItems = Utils.getRelateAuthorSaying(mContext, mAuthor);
		
		// 해당 저자와 관련된 명언이 한개 이하라면 토스트 출력하고 종료
//		if(mItems.size() <= 1) {
//			Toast.makeText(mContext, getString(R.string.relate_author_list_not_exist), Toast.LENGTH_SHORT).show();
//			finish();
//		}
		
		// 저자명을 한/영에 따라 설정
		switch(mLngType) {
			case Setting.PREF_LNG_CHANGE_KOR:
				mTvAuthor.setText(mItems.get(0).getKAuthor());
				break;
			case Setting.PREF_LNG_CHANGE_ENG:
				mTvAuthor.setText(mItems.get(0).getEAuthor());
				break;
		}
		
		// 저자 정보를 Database 에서 미리 가져온 뒤, 값이 존재하지 않는다면 저자 정보 버튼을 감춘다.
		mAuthorData = Utils.getAuthorInfo(mContext, mAuthor, mLngType);

		switch(mLngType) {
			case Setting.PREF_LNG_CHANGE_KOR:
				if(mAuthorData.getKInfo().length() > 1) {
					mBtnAuthorInfo.setVisibility(View.VISIBLE);
				} else {
					mBtnAuthorInfo.setVisibility(View.GONE);
				}
				
				break;
				
			case Setting.PREF_LNG_CHANGE_ENG:
				if(mAuthorData.getEInfo().length() > 1) {
					mBtnAuthorInfo.setVisibility(View.VISIBLE);
				} else {
					mBtnAuthorInfo.setVisibility(View.GONE);
				}
				
				break;
		}
		
		// 어댑터 설정
		mAdpater = new AdapterAuthorQuotesList(mContext, R.layout.author_quotes_list_row, mItems, mLngType);
		this.setListAdapter(mAdpater);
		
	}
	
	@Override
	protected void onDestroy() {
		
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			mAuthor = intent.getExtras().getString(Constants.PUT_RELATE_AUTHOR_NAME);
			mLngType = intent.getExtras().getInt(Constants.PUT_RELATE_LNG_TYPE);
		}
	}
	
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.main_btn_memo:
				Intent mainIntent = new Intent(AuthorQuotesList.this, MainMenu.class);
				startActivity(mainIntent);
				Utils.finishActivity(this);
				
				break;
				
			case R.id.main_btn_author:
				Intent authorIntent = new Intent(AuthorQuotesList.this, AuthorList.class);
				startActivity(authorIntent);
				Utils.finishActivity(this);
				
				break;
				
			case R.id.main_btn_category:
				Intent listIntent = new Intent(AuthorQuotesList.this, Category.class);
				startActivity(listIntent);
				Utils.finishActivity(this);
				
				break;
				
			case R.id.main_btn_favorite:
				if(Utils.isFavoriteEmpty(mContext)) {
					Toast.makeText(mContext, getString(R.string.favorite_toast_empty), Toast.LENGTH_SHORT).show();
				} else {
					Intent favoriteIntent = new Intent(AuthorQuotesList.this, Favorite.class);
					startActivity(favoriteIntent);
					Utils.finishActivity(this);
				}
				
				break;
				
			case R.id.main_btn_setting:
				Intent settingIntent = new Intent(AuthorQuotesList.this, Setting.class);
				startActivity(settingIntent);
				Utils.finishActivity(this);
				
				break;
				
			case R.id.author_quotes_info:
				
				AuthorInfoDialog authorInfoDialog = new AuthorInfoDialog(mContext, mLngType, mAuthorData);
				authorInfoDialog.show();
				
				break;
		}
	}
	
	
	@Override
	public void onBackPressed() {
		Intent mainIntent = new Intent(AuthorQuotesList.this, AuthorList.class);
		startActivity(mainIntent);
		Utils.finishActivity(this);
		
	}
}













