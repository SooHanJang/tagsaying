package madcat.studio.tagsaying;

import java.util.ArrayList;

import madcat.studio.adapter.AdapterFavoriteList;
import madcat.studio.constants.Constants;
import madcat.studio.data.FamousData;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class Favorite extends ListActivity implements OnClickListener {
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	private MessagePool mMessagePool;
	
	private ImageButton mBtnMemo, mBtnAuthor, mBtnCategory, mBtnFavorite, mBtnSetting;
	
	private ArrayList<FamousData> mItems;
	private AdapterFavoriteList mFavoriteAdapter;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Utils.logPrint(getClass(), "onCreate 호출");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favorite);
		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		this.mMessagePool = (MessagePool) getApplicationContext();
		
		// Resource Id 설정
		mBtnMemo = (ImageButton)findViewById(R.id.main_btn_memo);
		mBtnAuthor = (ImageButton)findViewById(R.id.main_btn_author);
		mBtnCategory = (ImageButton)findViewById(R.id.main_btn_category);
		mBtnFavorite = (ImageButton)findViewById(R.id.main_btn_favorite);
		mBtnSetting = (ImageButton)findViewById(R.id.main_btn_setting);

		// 메인 메뉴 아이콘 설정
		mBtnMemo.setBackgroundResource(R.drawable.main_menu_icon_today_off);
		mBtnFavorite.setBackgroundResource(R.drawable.main_menu_icon_favorites_on);
		
		// Event Listener 설정
		mBtnMemo.setOnClickListener(this);
		mBtnAuthor.setOnClickListener(this);
		mBtnCategory.setOnClickListener(this);
		mBtnFavorite.setOnClickListener(this);
		mBtnSetting.setOnClickListener(this);
		
		// Adapter 설정
		updateList();
		
	}
	
	@Override
	protected void onDestroy() {
		
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	private void updateList() {
		if(mFavoriteAdapter != null) {
			mFavoriteAdapter.clear();
		}
		
		this.mItems = Utils.getFavoriteData(mContext);
		this.mFavoriteAdapter = new AdapterFavoriteList(mContext, R.layout.favorite_row, mItems, 
				mConfigPref.getInt(Constants.CONFIG_PREF_LNG_INDEX, Setting.PREF_LNG_CHANGE_KOR));
		setListAdapter(mFavoriteAdapter);
	}
	
	private void startMenu(View v) {
		switch(v.getId()) {
			case R.id.main_btn_memo:
				Intent mainIntent = new Intent(Favorite.this, MainMenu.class);
				startActivity(mainIntent);
				Utils.finishActivity(this);
				
				break;
			case R.id.main_btn_author:
				Intent authorIntent = new Intent(Favorite.this, AuthorList.class);
				startActivity(authorIntent);
				Utils.finishActivity(this);
				
				break;
				
			case R.id.main_btn_category:
				Intent listIntent = new Intent(Favorite.this, Category.class);
				startActivity(listIntent);
				Utils.finishActivity(this);
				
				break;
			case R.id.main_btn_setting:
				Intent settingIntent = new Intent(Favorite.this, Setting.class);
				startActivity(settingIntent);
				Utils.finishActivity(this);
				
				break;
		}
	}
	
	public void onClick(final View v) {
		
		switch(v.getId()) {
			case R.id.main_btn_memo:
			case R.id.main_btn_author:
			case R.id.main_btn_category:
			case R.id.main_btn_setting:
				
				final ArrayList<FamousData> unCheckedItems = mFavoriteAdapter.getUnCheckedItem();
				
				if(unCheckedItems.isEmpty()) {
					startMenu(v);
				} else {
				
					AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
					builder.setTitle(getString(R.string.favorite_dig_title));
					builder.setMessage(getString(R.string.favorite_dig_msg));
					builder.setPositiveButton(getString(R.string.etc_btn_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							
							ChangeCheckedDataAsync changeDataAsync = new ChangeCheckedDataAsync(v, unCheckedItems);
							changeDataAsync.execute();
							
							
						}
					});
					
					builder.setNegativeButton(getString(R.string.etc_btn_cancel), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							startMenu(v);
						}
					});
					
					builder.show();
				}
			
			break;
		}
		
	}
	
	class ChangeCheckedDataAsync extends AsyncTask<Void, Void, Void> {

		ProgressDialog proDig;
		View v;
		ArrayList<FamousData> unCheckedItems;
		
		
		public ChangeCheckedDataAsync(View v, ArrayList<FamousData> unCheckedItems) {
			this.v = v;
			this.unCheckedItems = unCheckedItems;
		}
		
		@Override
		protected void onPreExecute() {
			proDig = ProgressDialog.show(mContext, "", getString(R.string.favorite_prg_title));
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			ArrayList<FamousData> famousData = mMessagePool.getDataArray();
			
			for(int i=0; i < unCheckedItems.size(); i++) {
				for(int j=0; j < famousData.size(); j++) {
					if(unCheckedItems.get(i).getId() == famousData.get(j).getId()) {
						famousData.get(j).setFavoriteEnable(false);
					}
				}
				
				Utils.updateCheckFavorite(mContext, unCheckedItems.get(i), false);
			}
			
			Utils.checkToFavoriteWidget(mContext, famousData);
			
			mMessagePool.setDataArray(famousData);
			
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(proDig.isShowing()) {
				proDig.dismiss();
			}
			
			startMenu(v);
		}
	}
}













