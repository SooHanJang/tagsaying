package madcat.studio.tagsaying;

import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Random;

import madcat.studio.adapter.AdapterShareList;
import madcat.studio.constants.Constants;
import madcat.studio.data.FamousData;
import madcat.studio.data.MemoData;
import madcat.studio.dialog.MemoDialog;
import madcat.studio.facebook.BaseRequestListener;
import madcat.studio.facebook.FaceBookLogin;
import madcat.studio.facebook.FaceBookUtil;
import madcat.studio.kakao.KakaoLink;
import madcat.studio.link.LinkEnabledTextView;
import madcat.studio.link.TextLinkClickListener;
import madcat.studio.me2day.GetAuthUrlWorker;
import madcat.studio.me2day.LoginWebActivity;
import madcat.studio.me2day.Me2dayInfo;
import madcat.studio.quickaction.ActionItem;
import madcat.studio.quickaction.QuickAction;
import madcat.studio.quickaction.QuickAction.OnActionItemClickListener;
import madcat.studio.twitter.ShareTwitter;
import madcat.studio.twitter.ShareTwitterLogin;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainMenu extends Activity 
	implements OnClickListener, OnCheckedChangeListener, OnActionItemClickListener, OnPageChangeListener, android.widget.PopupWindow.OnDismissListener {
	
	private final int TAG_LANGUAGE_KOR								=	Setting.PREF_LNG_CHANGE_KOR;
	private final int TAG_LANGUAGE_ENG								=	Setting.PREF_LNG_CHANGE_ENG;
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	private MessagePool mMessagePool;
	
	private ImageButton mBtnMemo, mBtnAuthor, mBtnCategory, mBtnFavorite, mBtnSetting, mBtnLanguage; 
	private CheckBox mCheckFavorite;
	private TextView mTvTagAuthor;
	private ImageView mImgTodayMark, mImgMemoMark;
	
	private QuickAction mQuickMemo;
	
	private ArrayList<FamousData> mArrayFamous;
	
	private int mSavedIndex;
	private int mCurrentPosition;
	private int mLngIndex;

	private ViewPager mPager;
	private AdapterSayingPager mPagerAdapter;
	
	private ArrayList<FamousData> mItems;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_menu);
		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_APPEND);
		this.mMessagePool = (MessagePool)this.getApplicationContext();
		
		// Resource Id 설정
		mBtnMemo = (ImageButton)findViewById(R.id.main_btn_memo);
		mBtnAuthor = (ImageButton)findViewById(R.id.main_btn_author);
		mBtnCategory = (ImageButton)findViewById(R.id.main_btn_category);
		mBtnFavorite = (ImageButton)findViewById(R.id.main_btn_favorite);
		mBtnSetting = (ImageButton)findViewById(R.id.main_btn_setting);
		
		mBtnLanguage = (ImageButton)findViewById(R.id.main_btn_lng);
		
		mCheckFavorite = (CheckBox)findViewById(R.id.main_cb_favorite);
		
		mTvTagAuthor = (TextView)findViewById(R.id.main_tv_tag_author);
		mImgTodayMark = (ImageView)findViewById(R.id.main_img_today_mark);
		mImgMemoMark = (ImageView)findViewById(R.id.main_img_memo_mark);

		mPager = (ViewPager)findViewById(R.id.main_pager);
		
		// Memo Quick Action 설정
		mQuickMemo = new QuickAction(mContext);
		ActionItem qItemAddMemo = new ActionItem(Constants.QUICK_ADD_MEMO,
				getString(R.string.main_quick_add_memo), getResources().getDrawable(R.drawable.quickaction_icon_memo_add));
		ActionItem qItemMngMemo = new ActionItem(Constants.QUICK_MNG_MEMO,
				getString(R.string.main_quick_mng_memo), getResources().getDrawable(R.drawable.quickaction_icon_memo_mng));
		ActionItem qItemShareMemo = new ActionItem(Constants.QUICK_SHARE_MEMO,
				getString(R.string.main_quick_share_memo), getResources().getDrawable(R.drawable.quickaction_icon_memo_share));
		
		mQuickMemo.addActionItem(qItemAddMemo);
		mQuickMemo.addActionItem(qItemMngMemo);
		mQuickMemo.addActionItem(qItemShareMemo);
		

		// Event Listener 설정
		mBtnMemo.setOnClickListener(this);
		mBtnAuthor.setOnClickListener(this);
		mBtnCategory.setOnClickListener(this);
		mBtnFavorite.setOnClickListener(this);
		mBtnSetting.setOnClickListener(this);
		mTvTagAuthor.setOnClickListener(this);
		mBtnLanguage.setOnClickListener(this);
		mImgMemoMark.setOnClickListener(this);
		
		mCheckFavorite.setOnCheckedChangeListener(this);
		
		mQuickMemo.setOnActionItemClickListener(this);
		mQuickMemo.setOnDismissListener(this);
				
		
		mPager.setOnPageChangeListener(this);
		
		mItems = mMessagePool.getDataArray();

		Utils.logPrint(getClass(), "명언 사이즈 : " + mMessagePool.getDataArray());

		
		if(mConfigPref.getBoolean(Constants.CONFIG_PREF_APP_FIRST, true)) {
			Utils.logPrint(getClass(), "앱을 초기화 합니다.");
			
			mSavedIndex = Utils.getMainRandomIndex(mContext, Setting.PREF_ALL_UPDATE);
			SharedPreferences.Editor configEditor = mConfigPref.edit();
			configEditor.putInt(Constants.CONFIG_PREF_TODAY_INDEX, mSavedIndex);
			configEditor.putBoolean(Constants.CONFIG_PREF_APP_FIRST, false);
			configEditor.commit();
		} else {
			
			Utils.logPrint(getClass(), "이미 초기화가 완료 되었습니다.");
			
			mSavedIndex = mConfigPref.getInt(Constants.CONFIG_PREF_TODAY_INDEX, 0);
			
		}
		
		// 페이지 설정
		Utils.logPrint(getClass(), "현재 페이지 위치 : " + mSavedIndex);
		
		mCurrentPosition = mSavedIndex;
		
		// 언어 설정
		mLngIndex = mConfigPref.getInt(Constants.CONFIG_PREF_LNG_INDEX, Setting.PREF_LNG_CHANGE_KOR);
		

		// 화면 표시 설정
		setUpdateTagPage(mItems, mCurrentPosition, mLngIndex);
		
		
		// 메인 메뉴가 트위터 인증 성공에서 호출 되었는지를 위해 twitterGetIntenter() 를 호출한다.
		this.getTwitterIntenter();
		
	}
	
	@Override
	protected void onDestroy() {
		
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	/**
	 * 화면에 표시할 명언과 관련한 설정을 합니다.
	 */
	private void setUpdateTagPage(ArrayList<FamousData> items, int position, int lngIndex) {
		setSayingText(items, position, lngIndex);
		
		mPagerAdapter = new AdapterSayingPager();
		mPager.setAdapter(mPagerAdapter);
		mPager.setCurrentItem(position, false);
		
	}
	
	private void setSayingText(ArrayList<FamousData> items, int position, int lngIndex) {

		// 오늘의 명언 마크 설정
		if(mSavedIndex == position) {
			mImgTodayMark.setVisibility(View.VISIBLE);
		} else {
			mImgTodayMark.setVisibility(View.INVISIBLE);
		}
		
		// 명언 저자 설정
		switch(lngIndex) {
			case Setting.PREF_LNG_CHANGE_KOR:
				mTvTagAuthor.setText(items.get(position).getKAuthor());
				mBtnLanguage.setBackgroundResource(R.drawable.main_icon_lng_kr);
				break;
				
			case Setting.PREF_LNG_CHANGE_ENG:
				mTvTagAuthor.setText(items.get(position).getEAuthor());
				mBtnLanguage.setBackgroundResource(R.drawable.main_icon_lng_en);
				break;
		}
		
		// 메모 존재 마크 설정
		if(items.get(position).getMemoFlagEnable()) {
			mImgMemoMark.setVisibility(View.VISIBLE);
		} else {
			mImgMemoMark.setVisibility(View.INVISIBLE);
		}
		
		// 즐겨찾기 Check 설정
		if(items.get(position).getFavoriteEnable() == 1) {
			mCheckFavorite.setChecked(true);
		} else {
			mCheckFavorite.setChecked(false);
		}
		
		
	}
	
	/**
	 * 트위터에서 인증을 받은 뒤 메인메뉴가 호출이 되었는지를 파악하는 함수입니다.
	 * 
	 */
	private void getTwitterIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			String twitterAuthClass = intent.getDataString();
			
			Utils.logPrint(getClass(), "value : " + intent.getDataString());
			
			// 첫 로그인 시, 트위터 인증을 받았다면 메모 다이얼로그를 호출한다.
			if(ShareTwitterLogin.class.toString().equals(twitterAuthClass)) {
				Utils.logPrint(getClass(), "첫 인증을 받은 뒤, 메모 다이얼로그를 호출합니다.");
				
				Utils.logPrint(getClass(), "mCurrentPosition : " + mCurrentPosition);

				MemoData memoData = null;
				if(mItems.get(mCurrentPosition).getMemoFlagEnable()) {
					memoData = Utils.getMemoData(mContext, mItems.get(mCurrentPosition).getId());
				}
				
				final MemoDialog postTwitterMemo = new MemoDialog(
						mContext, mItems.get(mCurrentPosition), memoData, MemoDialog.DIALOG_MODE_SHARE_TWITTER, mLngIndex);
				postTwitterMemo.show();
				
				
				postTwitterMemo.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(postTwitterMemo.getDialogState() == postTwitterMemo.DIALOG_OK) {
							mItems = mMessagePool.getDataArray();
							setSayingText(mItems, mCurrentPosition, mLngIndex);
						}
					}
				});
				
			}
		}
	}
	
	public void onDismiss() {
		mBtnMemo.setBackgroundResource(R.drawable.main_menu_icon_memo_off);
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.main_btn_memo:
				mBtnMemo.setBackgroundResource(R.drawable.main_menu_icon_memo_on);
				mQuickMemo.show(v);
				
				break;
				
			case R.id.main_btn_author:
				Intent authorIntent = new Intent(MainMenu.this, AuthorList.class);
				startActivity(authorIntent);
				Utils.finishActivity(this);

				break;
				
			case R.id.main_btn_category:
				Intent listIntent = new Intent(MainMenu.this, Category.class);
				startActivity(listIntent);
				Utils.finishActivity(this);
				
				
				break;
			case R.id.main_btn_favorite:
				if(Utils.isFavoriteEmpty(mContext)) {
					Toast.makeText(mContext, getString(R.string.favorite_toast_empty), Toast.LENGTH_SHORT).show();
				} else {
					Intent favoriteIntent = new Intent(MainMenu.this, Favorite.class);
					startActivity(favoriteIntent);
					Utils.finishActivity(this);
					
				}
				
				break;
			case R.id.main_btn_setting:
				Intent settingIntent = new Intent(MainMenu.this, Setting.class);
				startActivity(settingIntent);
				Utils.finishActivity(this);
				
				break;
				
			case R.id.main_btn_lng:
				
				// 언어 변경 버튼 클릭
				
				switch(mLngIndex) {
					case TAG_LANGUAGE_KOR:
						mLngIndex = TAG_LANGUAGE_ENG;
						break;
					case TAG_LANGUAGE_ENG:
						mLngIndex = TAG_LANGUAGE_KOR;
						break;
				}


				// Adapter 및 TextView 재설정
				setUpdateTagPage(mItems, mCurrentPosition, mLngIndex);
				
				break;
				
			case R.id.main_img_memo_mark:		//	메모 존재 마크를 눌렀을 경우 실행
				
				MemoData memoData = Utils.getMemoData(mContext, mItems.get(mCurrentPosition).getId());
				
				Utils.logPrint(getClass(), "작성된 메모 : " + memoData.getMemoText() + " (" + memoData.getMemoDate() + ")");
				
				
				MemoDialog memoViewDialog = new MemoDialog(mContext, mItems.get(mCurrentPosition), 
						memoData, MemoDialog.DIALOG_MODE_VIEWER, mLngIndex);
				memoViewDialog.show();
					
				
				break;
				
			case R.id.main_tv_tag_author:
				
				if(mItems.get(mCurrentPosition).getKCategory().equals("기도")) {
					Toast.makeText(mContext, getString(R.string.main_toast_caution_author_not_exist), Toast.LENGTH_SHORT).show();
				} else {
					Intent relateAuthorIntent = new Intent(MainMenu.this, AuthorQuotesList.class);
					relateAuthorIntent.putExtra(Constants.PUT_RELATE_AUTHOR_NAME, mItems.get(mCurrentPosition).getEAuthor());
					relateAuthorIntent.putExtra(Constants.PUT_RELATE_LNG_TYPE, mLngIndex);
					startActivity(relateAuthorIntent);

					Utils.finishActivity(this);
				}
				
				break;
		}
	}
	
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

		Utils.logPrint(getClass(), "isCheck 발생");

		mItems.get(mCurrentPosition).setFavoriteEnable((isChecked==true ? 1 : 0));
		Utils.updateCheckFavorite(mContext, mItems.get(mCurrentPosition), isChecked);
		
		if(!isChecked) {
			Utils.checkToFavoriteWidget(mContext, mItems);
		}
		
	}

	public void onItemClick(QuickAction source, int pos, int actionId) {
		switch(actionId) {
			case Constants.QUICK_ADD_MEMO:
				
				// 메모가 존재하지 않는다면,
				if(!mItems.get(mCurrentPosition).getMemoFlagEnable()) {
					final MemoDialog memoDialog = new MemoDialog(mContext, mItems.get(mCurrentPosition), MemoDialog.DIALOG_MODE_WRITE, mLngIndex);
					memoDialog.show();
					
					memoDialog.setOnDismissListener(new OnDismissListener() {
						public void onDismiss(DialogInterface dialog) {
							if(memoDialog.getDialogState() == MemoDialog.DIALOG_OK) {
								mItems = mMessagePool.getDataArray();
								setSayingText(mItems, mCurrentPosition, mLngIndex);
							}
						}
					});
				} else {		//	메모가 존재한다면,
					MemoData memoData = Utils.getMemoData(mContext, mItems.get(mCurrentPosition).getId());
					
					final MemoDialog memoDialog = new MemoDialog(mContext, mItems.get(mCurrentPosition), memoData, MemoDialog.DIALOG_MODE_MODIFY, mLngIndex);
					memoDialog.show();
					
				}
				
				break;
			case Constants.QUICK_MNG_MEMO:
				
				if(Utils.getMemoListData(mContext).isEmpty()) {
					Toast.makeText(mContext, R.string.memo_list_not_exist, Toast.LENGTH_SHORT).show();
				} else {
					Intent mngMemoIntent = new Intent(MainMenu.this, MemoList.class);
					startActivity(mngMemoIntent);
	
					Utils.finishActivity(this);
				}
				
				break;
			case Constants.QUICK_SHARE_MEMO:
				
				final String items[] = {getString(R.string.share_sms), getString(R.string.share_facebook), getString(R.string.share_twitter),
										getString(R.string.share_me2day), getString(R.string.share_kakaotalk)};
				
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
				builder.setTitle(getString(R.string.share_dig_title));
				builder.setAdapter(new AdapterShareList(mContext, R.layout.share_list_row, items), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						switch(which) {
							case Constants.SHARE_SMS:

								MemoData memoSMSData = null;
					        	
					        	if(mItems.get(mCurrentPosition).getMemoFlagEnable()) {
					        		memoSMSData = Utils.getMemoData(mContext, mItems.get(mCurrentPosition).getId());
					        	}
								
								final MemoDialog postSMSMemo = new MemoDialog(
										mContext, mItems.get(mCurrentPosition), memoSMSData, MemoDialog.DIALOG_MODE_SHARE_SMS, mLngIndex);
								postSMSMemo.show();
								
								postSMSMemo.setOnDismissListener(new OnDismissListener() {
									public void onDismiss(DialogInterface dialog) {
										if(postSMSMemo.getDialogState() == postSMSMemo.DIALOG_OK) {
											mItems = mMessagePool.getDataArray();
											setSayingText(mItems, mCurrentPosition, mLngIndex);
										}
									}
								});
								
								
								break;
								
								
							case Constants.SHARE_FACEBOOK:
								
								if(!Utils.isWifiAvailable(getApplicationContext()) && !Utils.is3GAvailable(getApplicationContext())) {
									Toast.makeText(mContext, getString(R.string.main_toast_caution_wifi_state), Toast.LENGTH_SHORT).show();
								} else {
									if (Constants.FACEBOOK_APP_ID == null) {
							            FaceBookUtil.showAlert(mContext, "Warning", getString(R.string.memo_share_facebook_login_error));
							        }
									
									// FacebookLogin 객체 생성 후 login() 호출
									MemoData memoShareData = null;
						        	
						        	if(mItems.get(mCurrentPosition).getMemoFlagEnable()) {
						        		memoShareData = Utils.getMemoData(mContext, mItems.get(mCurrentPosition).getId());
						        	}
									
									FaceBookLogin faceBookLogin = new FaceBookLogin(
											mContext, MainMenu.this, new MessageRequestListener(), mItems.get(mCurrentPosition), memoShareData, mLngIndex);
									faceBookLogin.login();
									
								}
								
								
								
								break;
								
							case Constants.SHARE_TWITTER:
								
								ShareTwitter shareTwitter = ShareTwitter.getInstance();
								
								// 저장된 로그인 정보가 없을 시,
								if(mConfigPref.getString(Constants.CONFIG_PREF_SHARE_TWITTER_ACCESS_TOKEN, "").equals("") ||
										mConfigPref.getString(Constants.CONFIG_PREF_SHARE_TWITTER_ACCESS_SECRET, "").equals("") ||
										mConfigPref.getString(Constants.CONFIG_PREF_SHARE_TWITTER_PIN_CODE, "").equals("")) {

									if(shareTwitter.login()) {	//	로그인 성공 시,
										
										Utils.logPrint(getClass(), "트위터 로그인 성공");
										
										Intent loginIntent = new Intent(MainMenu.this, ShareTwitterLogin.class);
										loginIntent.putExtra(Constants.PUT_SHARE_TWITTER_REQUEST_URL, shareTwitter.getRequestToken().getAuthorizationURL());
										startActivity(loginIntent);
										
										overridePendingTransition(0, 0);
										
									} else {					//	로그인 실패 시,
										
										Utils.logPrint(getClass(), "트위터 로그인 실패");
										
									}
									
								} else {		//	로그인 정보가 존재할 시
									//메모 다이얼로그 호출
									
									Utils.logPrint(getClass(), "트위터 로그인 정보가 이미 존재합니다. 메모 다이얼로그를 호출합니다.");
									
									MemoData memoData = null;
									if(mItems.get(mCurrentPosition).getMemoFlagEnable()) {
										memoData = Utils.getMemoData(mContext, mItems.get(mCurrentPosition).getId());
									}
									
									final MemoDialog postTwitterMemo = new MemoDialog(
											mContext, mItems.get(mCurrentPosition), memoData, MemoDialog.DIALOG_MODE_SHARE_TWITTER, mLngIndex);
									postTwitterMemo.show();
									
									postTwitterMemo.setOnDismissListener(new OnDismissListener() {
										public void onDismiss(DialogInterface dialog) {
											if(postTwitterMemo.getDialogState() == postTwitterMemo.DIALOG_OK) {
												mItems = mMessagePool.getDataArray();
												setSayingText(mItems, mCurrentPosition, mLngIndex);
											}
										}
									});
								}
									
								
								break;
								
							case Constants.SHARE_METOODAY:
								
								if(!Utils.isWifiAvailable(getApplicationContext()) && !Utils.is3GAvailable(getApplicationContext())) {
									Toast.makeText(mContext, getString(R.string.main_toast_caution_wifi_state), Toast.LENGTH_SHORT).show();
								} else {
									
									// 로그인 정보가 없다면, 미투데이 다이얼로그 출력
									String userId = mConfigPref.getString(Constants.PUT_SHARE_ME2DAY_ID, "");
							        String fullToken = mConfigPref.getString(Constants.PUT_SHARE_ME2DAY_TOKEN, "");
							        
							        if(userId.trim().toString().length() != 0 && fullToken.trim().toString().length() != 0) {
							        	
							        	MemoData memoShareData = null;
							        	
							        	if(mItems.get(mCurrentPosition).getMemoFlagEnable()) {
							        		memoShareData = Utils.getMemoData(mContext, mItems.get(mCurrentPosition).getId());
							        	}
							        	
							        	// 메모 공유하는 다이얼로그 호출
							        	final MemoDialog memoShareDialog = new MemoDialog(mContext, mItems.get(mCurrentPosition), 
							        			memoShareData, MemoDialog.DIALOG_MODE_SHARE_ME2DAY, mLngIndex, userId, fullToken);
							        	memoShareDialog.show();

							        	memoShareDialog.setOnDismissListener(new OnDismissListener() {
											public void onDismiss(DialogInterface dialog) {
												if(memoShareDialog.getDialogState() == memoShareDialog.DIALOG_OK) {
													mItems = mMessagePool.getDataArray();
													setSayingText(mItems, mCurrentPosition, mLngIndex);
												}
											}
										});
							        	
							        	
							        } else {
							        	GetAuthUrlWorker getAuthUrlWorker = GetAuthUrlWorker.getInstance();
							        	
								        boolean bAuthLoginStart = onClickLoginUsingOpenid();
								        if(bAuthLoginStart == false){
							        		Utils.logPrint(getClass(), "ERROR, fail to get login start url");
											return;
										} else {
											Utils.logPrint(getClass(), "SUCCESS to get login start url=" + getAuthUrlWorker.getReturnUrl());
										}
								        
								    	//Get Full-Auth Token
										boolean bFullAuthToken = openLoginWeb(getAuthUrlWorker);
										if(bFullAuthToken == false){
											Utils.logPrint(getClass(), "ERROR, fail to get full auth token");
											return;
										}
							        }
									
								}
								
								break;
							case Constants.SHARE_KAKAOTALK:
								
								try {
									KakaoLink kakaoLink = new KakaoLink(mContext, Constants.KAKAO_STR_URL, Constants.KAKAO_STR_APPID, 
											Constants.KAKAO_STR_APPVER, "checkAvailable", "UTF-8");
									
									if(kakaoLink.isAvailable()) {
										
										MemoData memoShareData = null;
							        	
							        	if(mItems.get(mCurrentPosition).getMemoFlagEnable()) {
							        		memoShareData = Utils.getMemoData(mContext, mItems.get(mCurrentPosition).getId());
							        	}
										
										final MemoDialog postKaKaoMemo = new MemoDialog(
												mContext, mItems.get(mCurrentPosition), memoShareData, MemoDialog.DIALOG_MODE_SHARE_KAKAO, mLngIndex);
										postKaKaoMemo.show();
										
										postKaKaoMemo.setOnDismissListener(new OnDismissListener() {
											public void onDismiss(DialogInterface dialog) {
												if(postKaKaoMemo.getDialogState() == postKaKaoMemo.DIALOG_OK) {
													mItems = mMessagePool.getDataArray();
													setSayingText(mItems, mCurrentPosition, mLngIndex);
												}
											}
										});
									} else {
										Toast.makeText(mContext, getString(R.string.memo_share_kakao_not_installed), Toast.LENGTH_SHORT).show();
									}
//									
								} catch (UnsupportedEncodingException e) {
									Toast.makeText(mContext, getString(R.string.memo_share_kakao_send_error), Toast.LENGTH_SHORT).show();
								}
								
								
								break;
						}
					}
				});
				
				builder.show();
				
				break;
		
		}
	}
	
	public void onPageSelected(int position) {
		
		Utils.logPrint(getClass(), "변경 전 : " + mCurrentPosition + ", 변경 후 : " + position);
		
		mCurrentPosition = position;
		setSayingText(mItems, position, mLngIndex);
		
	}
	
	public void onPageScrollStateChanged(int arg0) {}
	public void onPageScrolled(int arg0, float arg1, int arg2) {}
	
	/**
	 * 미투데이와 연결을 위한 HttpConnection 을 설정합니다.
	 * 
	 * @return
	 */
	private boolean onClickLoginUsingOpenid() {
		Utils.logPrint(getClass(), "onClickLoginUsingOpenId 호출");
    	
    	ProgressDialog prgDig = ProgressDialog.show(mContext, "", "로딩 중입니다.");
    	prgDig.show();

		GetAuthUrlWorker getAuthUrlWorker = GetAuthUrlWorker.getInstance();

		try {
			HttpParams params = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(params, Me2dayInfo.TIMEOUT);
			HttpConnectionParams.setSoTimeout(params, Me2dayInfo.TIMEOUT);

			DefaultHttpClient httpClient = new DefaultHttpClient(params);
			HttpRequestBase method = Utils.createHttpGetMehtod(GetAuthUrlWorker.get_auth_url());

			// configure http header
			settingHttpClient(method, httpClient);

			HttpResponse response = httpClient.execute(method);

			Utils.logPrint(getClass(), "prgDig : " + prgDig + ", " + prgDig.isShowing());
			
			if (prgDig != null) {
				if(prgDig.isShowing()) {
					prgDig.dismiss();
				}
			}

			InputStream in = response.getEntity().getContent();

			String xmlMessage = Utils.convertStreamToString(in);
			in = new StringBufferInputStream(xmlMessage);

			int responseCode = response.getStatusLine().getStatusCode();

			if (responseCode != HttpURLConnection.HTTP_OK) {
				getAuthUrlWorker.onError(response, in);
				return false;
			}

			Utils.logPrint(getClass(), xmlMessage);
			
			getAuthUrlWorker.onSuccess( response, in );
		}
		catch (SocketTimeoutException timeoutEx) {
			timeoutEx.printStackTrace();
			return false;
		}
		catch (SocketException e) {
			e.printStackTrace();
			return false;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 미투데이를 위해 웹에 로그인 합니다.
	 * 
	 * @param getAuthUrlWorker
	 * @return
	 */
    private boolean openLoginWeb(GetAuthUrlWorker getAuthUrlWorker) {
    	String url = getAuthUrlWorker.getReturnUrl();
    	
    	Utils.logPrint(getClass(), "url : " + url);
    	
		if (url != null) {
			Intent intent = new Intent(MainMenu.this, LoginWebActivity.class);
			intent.putExtra(LoginWebActivity.PARAM_URL, url);
			intent.putExtra(LoginWebActivity.PARAM_TOKEN, getAuthUrlWorker.getReturnToken());
			
			startActivity(intent);
			
			finish();
			overridePendingTransition(0, 0);
			
		} else {
			Utils.logPrint(getClass(), "로그인에 실패하였습니다.");
		}
		return true;
	}
    
    /**
     * 미투데이를 위한 HttpClient 설정
     * 
     * @param methodPost
     * @param httpClient
     */
	private void settingHttpClient(AbstractHttpMessage methodPost, DefaultHttpClient httpClient) {
		String aKey = Me2dayInfo.APP_KEY;
		methodPost.setHeader("Me2_application_key", aKey);
	}
	
	/**
	 * 페이스북 MessageRequestListener 클래스
	 * 
	 * @author Acsha
	 *
	 */
	public class MessageRequestListener extends BaseRequestListener {

		public void onComplete(String response, Object state) {
			runOnUiThread(new Runnable() {
                public void run() {
                	Toast.makeText(mContext, mContext.getString(R.string.memo_share_facebook_regist_post), Toast.LENGTH_SHORT).show();
                	
                	mItems = mMessagePool.getDataArray();
					setSayingText(mItems, mCurrentPosition, mLngIndex);
                }
            });
		}
	}
	
	
	
	/**
	 * ViewPager 를 설정하는 Adapter 클래스
	 * 
	 * @author Acsha
	 *
	 */
	public class AdapterSayingPager extends PagerAdapter implements TextLinkClickListener {
		
		@Override
		public Object instantiateItem(ViewGroup pager, int position) {
			
			// 패턴 설정
			// Pattern 설정을 위한 ArrayList 설정
			ArrayList<String> pattern = new ArrayList<String>();
			
			switch(mLngIndex) {
				case TAG_LANGUAGE_KOR:			//	한국어일 경우,
					
					// 1번 태그
					if(mItems.get(position).getKTag1().length() != 0) {
						pattern.add(mItems.get(position).getKTag1());
					}
					
					// 2번 태그
					if(mItems.get(position).getKTag2().length() != 0) {
						pattern.add(mItems.get(position).getKTag2());
					}
					
					// 3번 태그
					if(mItems.get(position).getKTag3().length() != 0) {
						pattern.add(mItems.get(position).getKTag3());
					}
					
					// 4번 태그
					if(mItems.get(position).getKTag4().length() != 0) {
						pattern.add(mItems.get(position).getKTag4());
					}
					
					// 5번 태그
					if(mItems.get(position).getKTag5().length() != 0) {
						pattern.add(mItems.get(position).getKTag5());
					}
					
					// 6번 태그
					if(mItems.get(position).getKTag6().length() != 0) {
						pattern.add(mItems.get(position).getKTag6());
					}
					
					
					break;
					
				case TAG_LANGUAGE_ENG:			//	영어일 경우,
					
					// 1번 태그
					if(mItems.get(position).getETag1().length() != 0) {
						pattern.add(mItems.get(position).getETag1());
					}
					
					// 2번 태그
					if(mItems.get(position).getETag2().length() != 0) {
						pattern.add(mItems.get(position).getETag2());
					}
					
					// 3번 태그
					if(mItems.get(position).getETag3().length() != 0) {
						pattern.add(mItems.get(position).getETag3());
					}
					
					// 4번 태그
					if(mItems.get(position).getETag4().length() != 0) {
						pattern.add(mItems.get(position).getETag4());
					}
					
					// 5번 태그
					if(mItems.get(position).getETag5().length() != 0) {
						pattern.add(mItems.get(position).getETag5());
					}
					
					// 6번 태그
					if(mItems.get(position).getETag6().length() != 0) {
						pattern.add(mItems.get(position).getETag6());
					}
					
					
					break;
			
			}

			// Pattern TextView 설정
			LinkEnabledTextView tvSaying = new LinkEnabledTextView(mContext, null);
			
			LinearLayout.LayoutParams tvSayingParams = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			tvSayingParams.weight = 1;
			
			tvSaying.setLayoutParams(tvSayingParams);

			switch(mConfigPref.getInt(Constants.CONFIG_PREF_TAG_CHANGE, Setting.PREF_TAG_CHANGE_ON)) {
				case Setting.PREF_TAG_CHANGE_ON:
					
					switch(mLngIndex) {
						case TAG_LANGUAGE_KOR:
							tvSaying.gatherLinksForText(mItems.get(position).getKText(), pattern);
							break;
						case TAG_LANGUAGE_ENG:
							tvSaying.gatherLinksForText(mItems.get(position).getEText(), pattern);
							break;
					}
					
					
					tvSaying.setLinkTextColor(Color.RED);
					tvSaying.setOnTextLinkClickListener(this);
					
					break;
				case Setting.PREF_TAG_CHANGE_OFF:
					
					switch(mLngIndex) {
						case TAG_LANGUAGE_KOR:
							tvSaying.setText(mItems.get(position).getKText());
							break;
						case TAG_LANGUAGE_ENG:
							tvSaying.setText(mItems.get(position).getEText());
							break;
					}
					
					break;
			}
			
			tvSaying.setTextColor(Color.BLACK);
			tvSaying.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
			tvSaying.setGravity(Gravity.CENTER);
			
			MovementMethod m = tvSaying.getMovementMethod();
	        if ((m == null) || !(m instanceof LinkMovementMethod)) {
	            if (tvSaying.getLinksClickable()) {
	            	tvSaying.setMovementMethod(LinkMovementMethod.getInstance());
	            }
	        }
	        
	        LinearLayout linearRoot = new LinearLayout(mContext);
	        linearRoot.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
	        linearRoot.setOrientation(LinearLayout.VERTICAL);
	        linearRoot.addView(tvSaying);

			
			// 패턴화된 TextView 를 LinearLayout에 추가
			((ViewPager)pager).addView(linearRoot, 0);
			
			return linearRoot;
		}
		
		@Override
		public int getCount() {
			return mItems.size();
		}
		
		@Override
		public void destroyItem(ViewGroup pager, int position, Object view) {
			((ViewPager)pager).removeView((View)view);
		}
		

		@Override
		public boolean isViewFromObject(View view, Object obj) {
			return view == obj;
		}

		public void onTextLinkClick(View textView, String clickedString) {

			Utils.logPrint(getClass(), "(Position : " + mCurrentPosition + "), 현재 명언 (id : " + mItems.get(mCurrentPosition).getId() + ") " + mItems.get(mCurrentPosition).getKText());
			
			ArrayList<FamousData> tempData = new ArrayList<FamousData>();
			
			for(int i=0; i < mItems.size(); i++) {
				if(mItems.get(i).getId() != mItems.get(mCurrentPosition-1).getId()) {
					
					switch(mLngIndex) {
					case TAG_LANGUAGE_KOR:
						
						if(mItems.get(i).getKTag1().equals(clickedString)) {
							tempData.add(mItems.get(i));
						} else if(mItems.get(i).getKTag2().equals(clickedString)) {
							tempData.add(mItems.get(i));
						} else if(mItems.get(i).getKTag3().equals(clickedString)) {
							tempData.add(mItems.get(i));
						} else if(mItems.get(i).getKTag4().equals(clickedString)) {
							tempData.add(mItems.get(i));
						} else if(mItems.get(i).getKTag5().equals(clickedString)) {
							tempData.add(mItems.get(i));
						} else if(mItems.get(i).getKTag6().equals(clickedString)) {
							tempData.add(mItems.get(i));
						}
						
						break;
					case TAG_LANGUAGE_ENG:
						
						if(mItems.get(i).getETag1().equals(clickedString)) {
							tempData.add(mItems.get(i));
						} else if(mItems.get(i).getETag2().equals(clickedString)) {
							tempData.add(mItems.get(i));
						} else if(mItems.get(i).getETag3().equals(clickedString)) {
							tempData.add(mItems.get(i));
						} else if(mItems.get(i).getETag4().equals(clickedString)) {
							tempData.add(mItems.get(i));
						} else if(mItems.get(i).getETag5().equals(clickedString)) {
							tempData.add(mItems.get(i));
						} else if(mItems.get(i).getETag6().equals(clickedString)) {
							tempData.add(mItems.get(i));
						}
						
						break;
					}
					
				}
			}
			
			if(tempData.size() > 0) {
				Random random = new Random(System.currentTimeMillis());
				int randIndex = random.nextInt(tempData.size());
				int selectedId = tempData.get(randIndex).getId();
				
				for(int i=0; i < mItems.size(); i++) {
					if(mItems.get(i).getId() == selectedId) {
						mCurrentPosition = i;
						setSayingText(mItems, mCurrentPosition, mLngIndex);
						mPager.setCurrentItem(mCurrentPosition, false);
						
						break;
					}
				}
			} else {
				Utils.logPrint(getClass(), clickedString + " 태그와 관련된 명언은 하나 혹은 그 미만입니다.");
			}
			
		}
		
	}

}


















