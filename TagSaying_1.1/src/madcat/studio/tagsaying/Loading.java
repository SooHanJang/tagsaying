package madcat.studio.tagsaying;

import madcat.studio.constants.Constants;
import madcat.studio.database.LoadDatabase;
import madcat.studio.service.DailyAlarmService;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.Toast;

public class Loading extends Activity {
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private boolean mFirstFlag;
	
	private MessagePool mMessagePool;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);
        this.mContext = this;
        this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
        this.mMessagePool = (MessagePool) this.getApplicationContext();

        // 앱 처음 실행 유무를 로드
        mFirstFlag = mConfigPref.getBoolean(Constants.CONFIG_PREF_APP_FIRST, true);
        
        SharedPreferences.Editor configEditor = mConfigPref.edit();
        
        // 앱을 처음 실행한다면, 단말기의 언어 설정 값을 가져와서 초기 언어 설정 값으로 지정한다.
        if(mFirstFlag) {
        	int lngIndex = (mContext.getResources().getConfiguration().locale.getLanguage().equals("ko") ? Setting.PREF_LNG_CHANGE_KOR : Setting.PREF_LNG_CHANGE_ENG);

        	Utils.logPrint(getClass(), "초기 언어 값 : " + lngIndex);
        	
        	configEditor.putInt(Constants.CONFIG_PREF_LNG_INDEX, lngIndex);
        }
        
        configEditor.commit();
        
        
        // 로딩 화면을 실행
        LoadingAsync loadingAsync = new LoadingAsync();
        loadingAsync.execute();
    }
    
	@Override
	protected void onDestroy() {
		
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
    
    class LoadingAsync extends AsyncTask<Void, Void, Void> {
    	
    	ProgressDialog progressDig;
    	boolean loadDbFlag;
    	
    	
    	@Override
    	protected void onPreExecute() {
    		if(mFirstFlag) {
    			progressDig = ProgressDialog.show(mContext, "", getString(R.string.loading_prg_txt));
    		}
    	}
    	
    	
    	@Override
    	protected Void doInBackground(Void... params) {
    		if(mFirstFlag) {
    			loadDbFlag = LoadDatabase.loadDataBase(mContext.getResources().getAssets(), mContext);
    		}

			mMessagePool.setDataArray(Utils.getFamouseData(mContext));		//	로딩 시간을 이용하여, 미리 명언 데이터를 불러온다.
			
			Utils.logPrint(getClass(), "명언 사이즈 : " + mMessagePool.getDataArray());
    		
    		
    		SystemClock.sleep(Constants.LOADING_DELAY);
    		return null;
    	}
    	
    	@Override
    	protected void onPostExecute(Void result) {
    		if(mFirstFlag) {
    			if(progressDig.isShowing()) {
    				progressDig.dismiss();
    			}
    			
    			// 데이터베이스 로드에 실패했을 경우
    			if(!loadDbFlag) {
    				Toast.makeText(mContext, getString(R.string.loading_prg_db_load_fai), Toast.LENGTH_SHORT).show();
    				finish();
    			}
    			
    			// 알람 매니져 등록
    			DailyAlarmService.registeUpdateService(mContext, Setting.PREF_WIDGET_UPDATE_DAILY);
    			
    		}
    		
    		Intent mainIntent = new Intent(Loading.this, MainMenu.class);
    		startActivity(mainIntent);
    		overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    		
    		finish(); 
    	}
    }
    
    @Override
    public void onBackPressed() {}
}













