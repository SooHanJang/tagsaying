package madcat.studio.constants;

public class Constants {
	
	public static final boolean DEVELOPE_MODE								=	false;
	
	public static final long LOADING_DELAY									=	1000;
	
	public static final String FACEBOOK_APP_ID								=	"339299809480165";
	
	public static final String TWITTER_API_KEY								=	"UhYsFh0nouzxOCQnQXF7w";
	public static final String TWITTER_CONSUMER_KEY							=	"UhYsFh0nouzxOCQnQXF7w";
	public static final String TWITTER_CONSUMER_SECRET						=	"EYzLmVdl8PCyF6CFWGwhd7SObnhfvRwbwnPIOghU5Q";
	
	public static final String CONFIG_PREF_NAME								=	"TAG_SAYING_CONFIG_PREF";
	
	public static final String CONFIG_PREF_APP_FIRST						=	"PREF_APP_FIRST";
	public static final String CONFIG_PREF_TAG_CHANGE						=	"PREF_TAG_CHANGE";
	public static final String CONFIG_PREF_LNG_INDEX						=	"PREF_LNG_INDEX";
	public static final String CONFIG_PREF_TODAY_RUN_CHECK					=	"PREF_TODAY_RUN_CHECK";
	public static final String CONFIG_PREF_TODAY_INDEX						=	"PREF_TODAY_INDEX";
	public static final String CONFIG_PREF_TODAY_DATE						=	"PREF_TODAY_DATE";
	public static final String CONFIG_PREF_DURATION_INDEX					=	"PREF_DURATION_INDEX";
	public static final String CONFIG_PREF_TODAY_K_SAYING					=	"PREF_TODAY_K_SAYING";
	public static final String CONFIG_PREF_TODAY_E_SAYING					=	"PREF_TODAY_E_SAYING";
	public static final String CONFIG_PREF_TODAY_K_AUTHOR					=	"PREF_TODAY_K_AUTHOR";
	public static final String CONFIG_PREF_TODAY_E_AUTHOR					=	"PREF_TODAY_E_AUTHOR";
	public static final String CONFIG_PREF_WIDGET_UPDATE_INDEX				=	"PREF_WIDGET_UPDATE_INDEX";
	public static final String CONFIG_PREF_WIDGET_DISPLAY_INDEX				=	"PREF_WIDGET_DISPLAY_INDEX";
	
	public static final String CONFIG_PREF_SHARE_ME2DAY_TOKEN				=	"PREF_SHARE_ME2DAY_TOKEN";
	
	public static final String CONFIG_PREF_SHARE_TWITTER_ACCESS_TOKEN		=	"CONFIG_PREF_SHARE_TWITTER_ACCESS_TOKEN";
	public static final String CONFIG_PREF_SHARE_TWITTER_ACCESS_SECRET		=	"CONFIG_PREF_SHARE_TWITTER_ACCESS_SECRET";
	public static final String CONFIG_PREF_SHARE_TWITTER_PIN_CODE			=	"CONFIG_PREF_SHARE_TWITTER_PIN_CODE";
	
	// 데이터 베이스
	public static final String DATABASE_NAME								=	"TagDb.db";
	public static final int DATABASE_VERSION								=	1;
	public static final int DATABASE_FILE_INDEX								=	3;
	public static final String DATABASE_ROOT_DIR							=	"/data/data/madcat.studio.tagsaying/databases/";
	
	// 데이터 베이스 테이블 및 컬럼
	// 명언 데이터 테이블 및 컬럼 변수
	public static final String TABLE_DATA									=	"DATA";
	public static final String DATA_ID										=	"_id";
	public static final String DATA_K_CATEGORY								=	"_k_category";
	public static final String DATA_E_CATEGORY								=	"_e_category";
	public static final String DATA_K_TEXT									=	"_k_text";
	public static final String DATA_K_AUTHOR								=	"_k_author";
	public static final String DATA_K_TAG_1									=	"_k_tag_1";
	public static final String DATA_K_TAG_2									=	"_k_tag_2";
	public static final String DATA_K_TAG_3									=	"_k_tag_3";
	public static final String DATA_K_TAG_4									=	"_k_tag_4";
	public static final String DATA_K_TAG_5									=	"_k_tag_5";
	public static final String DATA_K_TAG_6									=	"_k_tag_6";
	public static final String DATA_E_TEXT									=	"_e_text";
	public static final String DATA_E_AUTHOR								=	"_e_author";
	public static final String DATA_E_TAG_1									=	"_e_tag_1";
	public static final String DATA_E_TAG_2									=	"_e_tag_2";
	public static final String DATA_E_TAG_3									=	"_e_tag_3";
	public static final String DATA_E_TAG_4									=	"_e_tag_4";
	public static final String DATA_E_TAG_5									=	"_e_tag_5";
	public static final String DATA_E_TAG_6									=	"_e_tag_6";
	public static final String DATA_F_ENABLE								=	"_f_enable";
	public static final String DATA_M_ENABLE								=	"_m_enable";
	
	// 카테고리 테이블 및 컬럼 변수
	public static final String TABLE_CATEGORY								=	"CATEGORY";
	public static final String CATEGORY_K_NAME								=	"_k_name";
	public static final String CATEGORY_E_NAME								=	"_e_name";
	public static final String CATEGORY_SIZE								=	"_size";
	public static final String CATEGORY_ENABLE								=	"_enable";
	
	
	// 메모 테이블 및 컬럼 변수
	public static final String TABLE_MEMO									=	"MEMO";
	public static final String MEMO_DATE									=	"_date";
	public static final String MEMO_FAMOUS_ID								=	"_f_id";
	public static final String MEMO_MEMO_TEXT								=	"_m_txt";				//	메모 텍스트
	
	
	// 저자 정보 및 컬럼 변수
	public static final String TABLE_AUTHOR									=	"AUTHOR";
	public static final String AUTHOR_E_NAME								=	"_e_name";
	public static final String AUTHOR_K_JOB									=	"_k_job";
	public static final String AUTHOR_E_JOB									=	"_e_job";
	public static final String AUTHOR_K_INFO								=	"_k_info";
	public static final String AUTHOR_E_INFO								=	"_e_info";
		
	
	
	// 퀵 액션 메뉴 인덱스
	public static final int QUICK_ADD_MEMO									=	1;
	public static final int QUICK_MNG_MEMO									=	2;
	public static final int QUICK_SHARE_MEMO								=	3;
	
	
	// 인텐츠 첨부 변수
	public static final String PUT_FAMOUS_CATEGORY							=	"PUT_FAMOUS_CATEGORY";
	public static final String PUT_TODAY_RUN_CHECK							=	"PUT_TODAY_RUN_CHECK";
	public static final String PUT_SHARE_SNS_BODY							=	"PUT_SHARE_SNS_TEXT";
	public static final String PUT_SHARE_SNS_LOGIN_FLAG						=	"PUT_SHARE_SNS_LOGIN_FLAG";
	
	public static final String PUT_SHARE_ME2DAY_ID							=	"PUT_SHARE_ME2DAY_ID";
	public static final String PUT_SHARE_ME2DAY_TOKEN						=	"PUT_SHARE_ME2DAY_TOKEN";
	
	public static final String PUT_SHARE_TWITTER_REQUEST_URL				=	"PUT_SHARE_TWITTER_REQUEST_URL";
	public static final String PUT_SHARE_TWITTER_REQUEST_TOKEN				=	"PUT_SHARE_TWITTER_REQUEST_TOKEN";
	
	public static final String PUT_RELATE_AUTHOR_NAME						=	"PUT_RELATE_AUTHOR_NAME";
	public static final String PUT_RELATE_LNG_TYPE							=	"PUT_RELATE_LNG_TYPE";
	
	
	// 위젯 액션 변수
	public static final String ACTION_WIDGET_UPDATE_SAYING					=	"action.TagSaying.UPDATE_SAYING";
	
	
	// 공유 다이얼로그 변수
	public static final int SHARE_SMS										=	0;
	public static final int SHARE_FACEBOOK									=	1;
	public static final int SHARE_TWITTER									=	2;
	public static final int SHARE_METOODAY									=	3;
	public static final int SHARE_KAKAOTALK									=	4;
	
	// 미투데이(SNS) 관련 변수
	public static final String ME2DAY_URL									=	"http://me2day.net/api/plugins/mobile_post/new?";
	public static final String ME2DAY_BODY_PARAM							=	"new_post[body]=";
	public static final String ME2DAY_TAG_PARAM								=	"new_post[tags]=";
	public static final String ME2DAY_SIGN									=	"&";
	
	// 카카오톡(SNS) 관련 변수
	public static final String KAKAO_STR_MSG								=	"전송할 명언 메모"; 
	public static final String KAKAO_STR_URL								=	"https://market.android.com/details?id=madcat.studio.tagsaying";
	public static final String KAKAO_STR_INSTALL_URL						=	"https://market.android.com/details?id=madcat.studio.tagsaying";
	public static final String KAKAO_STR_APPID								=	"madcat.studio.tagsaying";
	public static final String KAKAO_STR_APPVER								=	"1.0";
	public static final String KAKAO_STR_APPNAME							=	"태그 명언 위젯";
	
	// 백업 & 복구 관련 변수
	public static final String BACKUP_PATH									=	"/MadCatStudio/QuotesWithTag/Backup";
	public static final String BACKUP_ZIP_PREFIX							=	"QuotesWithTag_";
	public static final String BACKUP_ZIP_POSTFIX							=	".zip";
		
	
	
	

}












