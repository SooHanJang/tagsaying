package madcat.studio.facebook;

import madcat.studio.constants.Constants;
import madcat.studio.data.FamousData;
import madcat.studio.data.MemoData;
import madcat.studio.dialog.MemoDialog;
import madcat.studio.facebook.Facebook.DialogListener;
import madcat.studio.facebook.SessionEvents.AuthListener;
import madcat.studio.facebook.SessionEvents.LogoutListener;
import madcat.studio.tagsaying.MainMenu.MessageRequestListener;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

public class FaceBookLogin implements DialogListener{

	private final String TAG										=	"FaceBookLogin";
	private boolean DEVELOPE_MODE									=	Constants.DEVELOPE_MODE;

	
	private static final String[] PERMISSIONS						=	new String[] {"publish_stream", "read_stream", "offline_access"};
	
	private Facebook mFacebook;
	private AsyncFacebookRunner mAsyncRunner;
	
	private Context mContext;
	private Activity mActivity;
	
	private FamousData mFamousData;
	private MemoData mMemoData;
	private int mLngType;
	
	private MessageRequestListener mMessageRequestListener;
	

	public FaceBookLogin(Context context, Activity activity) {
		this.mContext = context;
		this.mActivity = activity;
	}
	
	public FaceBookLogin(Context context, Activity activity, MessageRequestListener messageRequestListener, FamousData famousData, MemoData memoData, int lngType) {
		this.mContext = context;
		this.mActivity = activity;
		this.mMessageRequestListener = messageRequestListener;
		this.mFamousData = famousData;
		this.mMemoData = memoData;
		this.mLngType = lngType;
	}
	
	public void login() {
		mFacebook = new Facebook(Constants.FACEBOOK_APP_ID);		// FaceBook 에 등록된 앱의 아이디 설정
		
		mAsyncRunner = new AsyncFacebookRunner(mFacebook);	// 설정된 앱 아이디를 바탕으로 세션 초기화
		
		SessionStore.restore(mFacebook, mContext.getApplicationContext());
	    SessionEvents.addAuthListener(new FaceBookAuthListener());
	    SessionEvents.addLogoutListener(new FaceBookLogoutListener());
	    
		mFacebook.authorize(mActivity, PERMISSIONS, this);
	}
	
	public class FaceBookAuthListener implements AuthListener {
        public void onAuthSucceed() {}				//	로그인 성공
        public void onAuthFail(String error) {}		//	로그인 실패
    }

    public class FaceBookLogoutListener implements LogoutListener {
        public void onLogoutBegin() {}				//	로그아웃 시작
        public void onLogoutFinish() {}				//	로그아웃 완료
    }
    
    // 로그인이 성공하면, 메모를 공유하는 다이얼로그를 호출한다.
	public void onComplete(Bundle values) {
		MemoDialog memoDialog = new MemoDialog(mContext, mFacebook, mMessageRequestListener, mFamousData, mMemoData, MemoDialog.DIALOG_MODE_SHARE_FACE_BOOK, mLngType);
		memoDialog.show();
		
	}

	public void onFacebookError(FacebookError e) {
		final String errorMessage = e.getMessage();
		
		mActivity.runOnUiThread(new Runnable() {
            public void run() {
            	Toast.makeText(mContext, "FaceBook Error" + errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
		
	}

	public void onError(DialogError e) {
		final String errorMessage = e.getMessage();
		
		mActivity.runOnUiThread(new Runnable() {
            public void run() {
            	Toast.makeText(mContext, "FaceBook onError" + errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
		
	}

	public void onCancel() {}
}
