package madcat.studio.data;

import java.io.Serializable;

public class FamousData implements Serializable {
	
	private final long serialVerisionUID								=	1L;

	private String mKCategory, mKText, mKAuthor, mKTag1, mKTag2, mKTag3, mKTag4, mKTag5, mKTag6;
	private String mECategory, mEText, mEAuthor, mETag1, mETag2, mETag3, mETag4, mETag5, mETag6;
	private int mId, mFavoriteEnable, mMemoEnable;
	
	public int getId() {
		return mId;
	}

	public void setId(int id) {
		mId = id;
	}

	public int getFavoriteEnable() {
		return mFavoriteEnable;
	}
	
	public boolean getFavoriteFlagEnable() {
		return (mFavoriteEnable == 1 ? true : false);
	}
	
	public void setFavoriteEnable(boolean flag) {
		this.mFavoriteEnable = (flag == true ? 1 : 0);
	}
	
	public void setFavoriteEnable(int enable) {
		this.mFavoriteEnable = enable;
	}
	
	public int getMemoEnable() {
		return mMemoEnable;
	}
	
	public boolean getMemoFlagEnable() {
		return (mMemoEnable == 1 ? true : false);
	}
	
	public void setMemoEnable(boolean flag) {
		this.mMemoEnable = (flag == true ? 1 : 0);
	}
	
	public void setMemoEnable(int enable) {
		this.mMemoEnable = enable;
	}
	
	public String getKCategory() {
		return mKCategory;
	}
	public void setKCategory(String kCategory) {
		this.mKCategory = kCategory;
	}
	public String getECategory() {
		return mECategory;
	}
	public void setECategory(String eCategory) {
		this.mECategory = eCategory;
	}
	public String getKText() {
		return mKText;
	}
	public void setKText(String kText) {
		mKText = kText;
	}
	public String getKAuthor() {
		return mKAuthor;
	}
	public void setKAuthor(String kAuthor) {
		mKAuthor = kAuthor;
	}
	public String getKTag1() {
		return mKTag1;
	}
	public void setKTag1(String kTag1) {
		mKTag1 = kTag1;
	}
	public String getKTag2() {
		return mKTag2;
	}
	public void setKTag2(String kTag2) {
		mKTag2 = kTag2;
	}
	public String getKTag3() {
		return mKTag3;
	}
	public void setKTag3(String kTag3) {
		mKTag3 = kTag3;
	}
	public String getKTag4() {
		return mKTag4;
	}
	public void setKTag4(String kTag4) {
		mKTag4 = kTag4;
	}
	public String getKTag5() {
		return mKTag5;
	}
	public void setKTag5(String kTag5) {
		mKTag5 = kTag5;
	}
	public String getKTag6() {
		return mKTag6;
	}
	public void setKTag6(String kTag6) {
		mKTag6 = kTag6;
	}
	public String getEText() {
		return mEText;
	}
	public void setEText(String eText) {
		mEText = eText;
	}
	public String getEAuthor() {
		return mEAuthor;
	}
	public void setEAuthor(String eAuthor) {
		mEAuthor = eAuthor;
	}
	public String getETag1() {
		return mETag1;
	}
	public void setETag1(String eTag1) {
		mETag1 = eTag1;
	}
	public String getETag2() {
		return mETag2;
	}
	public void setETag2(String eTag2) {
		mETag2 = eTag2;
	}
	public String getETag3() {
		return mETag3;
	}
	public void setETag3(String eTag3) {
		mETag3 = eTag3;
	}
	public String getETag4() {
		return mETag4;
	}
	public void setETag4(String eTag4) {
		mETag4 = eTag4;
	}
	public String getETag5() {
		return mETag5;
	}
	public void setETag5(String eTag5) {
		mETag5 = eTag5;
	}
	public String getETag6() {
		return mETag6;
	}
	public void setETag6(String eTag6) {
		mETag6 = eTag6;
	}
	
}
