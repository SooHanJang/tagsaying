package madcat.studio.data;

public class AuthorData {
	
	private int mId;
	private String mKName, mEName;
	private String mDate;
	private String mKJob, mEJob;
	private String mKText, mEText;
	
	public int getId() {
		return mId;
	}
	
	public void setId(int id) {
		mId = id;
	}
	
	public String getKName() {
		return mKName;
	}
	
	public void setKName(String kName) {
		mKName = kName;
	}
	
	public String getEName() {
		return mEName;
	}
	public void setEName(String eName) {
		mEName = eName;
	}
	
	public String getDate() {
		return mDate;
	}
	
	public void setDate(String date) {
		mDate = date;
	}
	
	public String getKJob() {
		return mKJob;
	}
	public void setKJob(String kJob) {
		mKJob = kJob;
	}
	public String getEJob() {
		return mEJob;
	}
	public void setEJob(String eJob) {
		mEJob = eJob;
	}
	public String getKInfo() {
		return mKText;
	}
	public void setKText(String kText) {
		mKText = kText;
	}
	public String getEInfo() {
		return mEText;
	}
	public void setEText(String eText) {
		mEText = eText;
	}
	

}
