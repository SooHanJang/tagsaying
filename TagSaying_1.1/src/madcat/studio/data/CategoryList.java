package madcat.studio.data;

public class CategoryList {

	private String mKName, mEName;
	private int mId;
	private int mSize;
	private int mEnable;
	private boolean mEnableFlag;
	
	public CategoryList(int id, String kName, String eName, int number, int enable) {
		this.mId = id;
		this.mKName = kName;
		this.mEName = eName;
		this.mSize = number;
		this.mEnable = enable;
		this.mEnableFlag = (enable == 1 ? true : false);
	}
	
	public int getId() {	return mId;	}
	public void setId(int id) {	this.mId = id;	}
	
	public String getKName() {	return mKName;	}
	public void setName(String kName) {	this.mKName = kName;	}
	
	public String getEName() {	return mEName;	}
	public void setEName(String eName) {	this.mEName = eName;	}
	
	public int getSize() {	return mSize;	}
	public void setSize(int size) {	this.mSize = size;	}
	
	public int getEnable() {	return mEnable;	}
	public void setEnable(int enable) {	
		this.mEnable = enable;
		this.mEnableFlag = (enable == 1 ? true : false);
	}
	
	public void setEnableFlag(boolean flag) {
		this.mEnableFlag = flag;
		this.mEnable = (flag == true ? 1 : 0);
	}
	
	public boolean getEnableFlag() {	return mEnableFlag;	}
		

}
