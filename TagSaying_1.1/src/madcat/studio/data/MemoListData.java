package madcat.studio.data;

public class MemoListData {
	
	private MemoData mMemoData;
	private FamousData mFamousData;

	public MemoListData(MemoData memoData, FamousData famousData) {
		this.mMemoData = memoData;
		this.mFamousData = famousData;
	}
	
	public MemoData getMemoData() {
		return mMemoData;
	}

	public void setMemoData(MemoData memoData) {
		this.mMemoData = memoData;
	}

	public FamousData getFamousData() {
		return mFamousData;
	}

	public void setFamousData(FamousData famousData) {
		this.mFamousData = famousData;
	}


}
