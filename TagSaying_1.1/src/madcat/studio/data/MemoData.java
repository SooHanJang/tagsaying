package madcat.studio.data;

public class MemoData {
	
	private String mMemoDate, mMemoText;
	private int mFamouseId;
	
	public MemoData(String memoDate, int famouseId, String memoTxt) {
		this.mMemoDate = memoDate;
		this.mFamouseId = famouseId;
		this.mMemoText = memoTxt;
	}
	
	public int getFamouseId() {
		return mFamouseId;
	}

	public void setFamouseId(int famouseId) {
		mFamouseId = famouseId;
	}
	
	public String getMemoDate() {
		return mMemoDate;
	}

	public void setMemoDate(String memoDate) {
		mMemoDate = memoDate;
	}

	public String getMemoText() {
		return mMemoText;
	}

	public void setMemoText(String memoText) {
		mMemoText = memoText;
	}
	
}
