package madcat.studio.me2day;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import madcat.studio.utils.Utils;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.util.Log;

public class GetAuthUrlWorker {

	static final String TAG										=	"GetAuthUrlWorker";
	private String returnUrl = null;
	private String returnToken = null;

	private static GetAuthUrlWorker instance;

	public static GetAuthUrlWorker getInstance() {
		if (instance == null) {
			synchronized (GetAuthUrlWorker.class) {
				if (instance == null) {
					instance = new GetAuthUrlWorker();
				}
			}
		}
		return instance;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public String getReturnToken() {
		return returnToken;
	}

	public void onSuccess(HttpResponse response, InputStream in)
			throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(in);
		if (document != null) {
			NodeList items = document.getElementsByTagName("auth_token");
			if (items != null) {
				Element item = (Element) items.item(0);
				returnUrl = Utils.getElementValue(item.getElementsByTagName("url").item(0));
				returnToken = Utils.getElementValue(item.getElementsByTagName("token").item(0));
			}
		}

	}

	public void onError(HttpResponse response, InputStream in) throws Exception {
		int responseCode = response.getStatusLine().getStatusCode();
		returnUrl = null;
		returnToken = null;
	}

	static public String get_auth_url() {
		StringBuffer url = new StringBuffer();
		url.append(Me2dayInfo.host).append("/api/get_auth_url.xml?");
		Utils.appendSigUrl(url, false);
		String urlText = url.toString();
		return urlText;
	}
}
