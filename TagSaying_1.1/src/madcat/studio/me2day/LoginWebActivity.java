package madcat.studio.me2day;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import madcat.studio.constants.Constants;
import madcat.studio.tagsaying.MainMenu;
import madcat.studio.tagsaying.R;
import madcat.studio.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

public class LoginWebActivity extends Activity {
	
	private final String TAG										=	"LoginWebActivity";
	private boolean DEVELOPE_MODE									=	Constants.DEVELOPE_MODE;

	public static final int LOGIN_SUCCESS							=	1;
	public static final int LOGIN_FAIL								=	2;

	public static final String category								=	"LoginWebActivity";	//DANIEL
	public static final String PARAM_URL							=	"url";
	public static final String PARAM_TOKEN							=	"token";
	public static final String PARAM_ASIG							=	"asig";
	
	private Context mContext;
	private SharedPreferences mConfigPref;

	private WebView loginWebview;
	private LinearLayout completeView;
	ProgressDialog progressDialog;

	private boolean isLoginComplete;
	private String currentURL;
	private int oldOrientation = -1;

	private GetFullAuthTokenWorker workerGetFullAuthToken;
	private static int webViewFailCount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate 호출");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share_me2day_login);
		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		isLoginComplete = false;
		loginWebview = (WebView)this.findViewById(R.id.login_web);
		loginWebview.setVerticalScrollbarOverlay(true);
		completeView = (LinearLayout)this.findViewById(R.id.complete_view);
		completeView.setVisibility(View.GONE);
		loginWebview.getSettings().setJavaScriptEnabled(true);
		loginWebview.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageFinished(WebView view, String url) {
				Log.d(TAG, "Called onPageFinished(), url=" + url);
				onPageFinishLogin(url);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				Log.d(TAG, "Called shouldOverrideUrlLoading(), url=" + url);

				if (url.startsWith("http://me2day.net/api/auth_submit_popup") == true) {
					completeAuth(url);

				}	else {
					view.loadUrl(url);
				}
				return true;
			}

			//@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon)	{
				Log.d(TAG, "onPageStarted 호출, url=" + url);
				super.onPageStarted(view, url, favicon);
				currentURL = url;
				if(url.startsWith(Me2dayInfo.completeAuthUrl) == true)	{
					isLoginComplete = true;
					updateLoginUI();
					
				}
			}


			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				Log.d(TAG, "Called onReceivedError(), errorCode=" + errorCode);
				Log.d(TAG, "Called onReceivedError(), failingUrl=" + failingUrl);
				super.onReceivedError(view, errorCode, description, failingUrl);
				if (progressDialog != null) {
					progressDialog.setCancelable(true);
				} else {
					Log.d(TAG, "onReceivedError(), webViewFailCount=" + webViewFailCount);
					if (webViewFailCount >= 2 ){
						Log.d(TAG, "onReceivedError(), Fail to connect server");

						webViewFailCount = 0;
						view.goBack();
					}else{
						webViewFailCount++;
						Log.d(TAG, "onReceivedError(), Retry(" + webViewFailCount + ") to connect server" );
						loginWebview.loadUrl(failingUrl);
					}
				}
			}
		});

		Intent intent = this.getIntent();
		String url = intent.getStringExtra(LoginWebActivity.PARAM_URL);
		Log.d(TAG, String.format("loginUrl %s", url.toString()));
		currentURL = url.toString();
		loginWebview.loadUrl(currentURL);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		Log.d(TAG, "Called onConfigurationChanged()");
		if (oldOrientation == newConfig.orientation) {
			super.onConfigurationChanged(newConfig);
			return;
		}
		oldOrientation = newConfig.orientation;
    	super.onConfigurationChanged(newConfig);
    	if(isLoginComplete)
    		updateLoginUI();
    }

	private void updateLoginUI()	{
		Log.d(TAG, "updateLoginUI 호출");
		setContentView(R.layout.share_me2day_login);
		
		loginWebview = (WebView)this.findViewById(R.id.login_web);
		loginWebview.setVerticalScrollbarOverlay(true);
		completeView = (LinearLayout)this.findViewById(R.id.complete_view);

		if(isLoginComplete)	{
			loginWebview.setVisibility(View.GONE);
			completeView.setVisibility(View.VISIBLE);

		}	else	{
			loginWebview.setVisibility(View.VISIBLE);
			completeView.setVisibility(View.GONE);
		}

		if(currentURL != null && currentURL.length() > 0)	{
			loginWebview.loadUrl(currentURL);
		}

	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "Called onDestroy()");
		if (progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
		if(loginWebview != null)	{
			loginWebview.destroy();
			loginWebview = null;
		}
		if(completeView !=null)	{
			completeView.removeAllViews();
			completeView = null;
		}
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		startMainMenu();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d(TAG, "Called onKeyDown()");
		if(keyCode == KeyEvent.KEYCODE_SEARCH)
			return true;
		return super.onKeyDown(keyCode, event);
	}
	@Override
	public boolean onSearchRequested()	{
		return false;
	}


	private void onPageFinishLogin(String url) {
		Log.d(TAG, "Called onPageFinishLogin(), url=" + url);
		Log.d(TAG, String.format("onPageFinished url(%s)", url));
		//if (url.startsWith(Me2dayInfo.completeAuthUrl) == true) {
		
		
		if (url.startsWith("http://me2day.net/api/auth_submit_popup") == true) {
			completeAuth(url);
		}
	}

    public boolean checkNetworkAvailable(){
    	Log.d(TAG, "Called checkNetworkAvailable()");
    	ConnectivityManager conn_manager = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
    	boolean isNetworkAvailable = false;
        try{
            NetworkInfo network_info = conn_manager.getActiveNetworkInfo();
            if ( network_info != null && network_info.isConnected() ) {
                if (network_info.getType() == ConnectivityManager.TYPE_WIFI ){
                    // do some staff with WiFi connection
                	isNetworkAvailable = true;
                }	else if (network_info.getType() == ConnectivityManager.TYPE_MOBILE ){
                    // do something with Carrier connection
                	isNetworkAvailable = true;
                }	else if (network_info.getType() == ConnectivityManager.TYPE_WIMAX ){
                    // do something with Wibro/Wimax connection
                	isNetworkAvailable = true;
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        if(isNetworkAvailable == false)
        	showNetworkAlert();

        return isNetworkAvailable;
    }

    private void showNetworkAlert()	{
    	Log.d(TAG, "Called showNetworkAlert()");
    	AlertDialog.Builder adb = new AlertDialog.Builder(this);
		adb.setTitle("誘명닾�곗씠");
		adb.setIcon(android.R.drawable.ic_dialog_alert);
		adb.setMessage("�ㅽ듃�뚰겕 �곌껐���뺤씤��二쇱꽭��");
		adb.setPositiveButton("�뺤씤", new OnClickListener(){
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		adb.setOnKeyListener(new OnKeyListener(){
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if(keyCode == KeyEvent.KEYCODE_SEARCH)
					return true;
				return false;
			}
		});
		adb.show();
    }

	private void completeAuth(String url) {
		Log.d(TAG, "Called completeAuth(), url=" + url);

    	// Network Available check.
    	if(checkNetworkAvailable() == false){
			if (progressDialog != null) {
				progressDialog.hide();
			}
    		return;
    	}

	    getFullAuthToken();
	}

	private boolean getFullAuthToken() {
		Log.d(TAG, "Called getFullAuthToken()");

		boolean loginsuccess = false;
		Intent intent = this.getIntent();
		String token = intent.getStringExtra(LoginWebActivity.PARAM_TOKEN);
		workerGetFullAuthToken = GetFullAuthTokenWorker.getInstance();
		
		try {
			HttpParams params = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(params, Me2dayInfo.TIMEOUT);
			HttpConnectionParams.setSoTimeout(params, Me2dayInfo.TIMEOUT);

			DefaultHttpClient httpClient = new DefaultHttpClient(params);
			HttpRequestBase method = createHttpMehtod(GetFullAuthTokenWorker.get_full_auth_token(token));

			// configure http header
			settingHttpClient(method, httpClient);

			HttpResponse response = httpClient.execute(method);
			InputStream in = response.getEntity().getContent();

			String xmlMessage = Utils.convertStreamToString(in);
			in = new StringBufferInputStream(xmlMessage);

			int responseCode = response.getStatusLine().getStatusCode();
			Log.d(TAG, String.format("Response Code = %d", responseCode));

			if (responseCode != HttpURLConnection.HTTP_OK) {
				Log.d(TAG, String.format("onClickLoginUsingOpenid(), Error(%d, %s) message(%s)",
						responseCode, response.getStatusLine().getReasonPhrase(), xmlMessage));
				workerGetFullAuthToken.onError(response, in);
				return false;
			}

			Log.d(TAG, "xmlMessage : " + xmlMessage);
			workerGetFullAuthToken.onSuccess( response, in );

		    if ( workerGetFullAuthToken.ishttpok()==true ) {
		        String returnToken = workerGetFullAuthToken.getReturnToken();

				if (returnToken != null && returnToken.length()>0) {
					loginsuccess = true;
					loginSuccess(returnToken);
					
					
				} else {
					loginsuccess = false;
				}
			}

			if (loginsuccess == false) {
				Toast.makeText(LoginWebActivity.this, getString(R.string.memo_share_message_unknown_error), Toast.LENGTH_SHORT).show();
				startMainMenu();
			}
		}
		catch (SocketTimeoutException timeoutEx) {
			timeoutEx.printStackTrace();
			return false;
		}
		catch (SocketException e) {
			e.printStackTrace();
			return false;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private HttpRequestBase createHttpMehtod(String url) {
		Log.d(TAG, "createHttpMehtod() 호출, url=" + url);
		return new HttpPost(url);
	}

	private void settingHttpClient(AbstractHttpMessage methodPost, DefaultHttpClient httpClient) {
		String aKey = Me2dayInfo.APP_KEY;
		methodPost.setHeader("Me2_application_key", aKey);

	}

	private void loginSuccess(String fullAuthtoken) {
		Log.d(TAG, "loginSuccess 호출");
		this.setResult(LOGIN_SUCCESS);
		
		Log.d(TAG, "내가 받은 토큰 : " + fullAuthtoken);
		
		// 생성한 인증 토큰 저장하고
		SharedPreferences.Editor editor = mConfigPref.edit();
		editor.putString(Constants.PUT_SHARE_ME2DAY_ID, Me2dayInfo.getLoginId());
		editor.putString(Constants.PUT_SHARE_ME2DAY_TOKEN, fullAuthtoken);
		editor.commit();
		
//		// 글 쓰기 다이얼로그 생성
//		
//		try {
//			CreatePostPoster createPost = new CreatePostPoster();
//			createPost.setTag("abc");
//			createPost.setBody("def");
//
//			HttpParams params = new BasicHttpParams();
//			HttpConnectionParams.setConnectionTimeout(params, Me2dayInfo.TIMEOUT);
//			HttpConnectionParams.setSoTimeout(params, Me2dayInfo.TIMEOUT);
//
//			DefaultHttpClient httpClient = new DefaultHttpClient(params);
//			
//			HttpPost postMethod = (HttpPost)createHttpMehtod(createPost.create_post(Me2dayInfo.getLoginId()));
//			
//			createPost.settingHttpClient(postMethod, httpClient, Me2dayInfo.getLoginId(), fullAuthtoken);
//			HttpResponse response = httpClient.execute(postMethod);
//			
//			
//		} catch (ClientProtocolException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		

		startMainMenu();
		
	}
	
	private void startMainMenu() {
		Intent mainIntent = new Intent(LoginWebActivity.this, MainMenu.class);
		startActivity(mainIntent);
		
		finish();
	}
}
