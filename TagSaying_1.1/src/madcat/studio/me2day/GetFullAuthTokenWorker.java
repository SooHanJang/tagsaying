package madcat.studio.me2day;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import madcat.studio.utils.Utils;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.util.Log;

public class GetFullAuthTokenWorker {

	static final String TAG										=	"GetFullAuthTokenWorker";
	private static GetFullAuthTokenWorker instance;

	private String returnToken;
	private String userId;
	private boolean httpok = false;

	public boolean ishttpok() {
		return httpok;
	}

	public static GetFullAuthTokenWorker getInstance() {
		if (instance == null) {
			synchronized (GetFullAuthTokenWorker.class) {
				if (instance == null) {
					instance = new GetFullAuthTokenWorker();
				}
			}
		}
		return instance;
	}

	public String getReturnToken() {
		return returnToken;
	}

	public String getReturnUserId() {
		return userId;
	}
	
	public void onSuccess(HttpResponse response, InputStream in)
			throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(in);
		if (document != null) {
			NodeList items = document.getElementsByTagName("auth_token");
			if (items != null) {
				Element item = (Element) items.item(0);
				returnToken = Utils.getElementValue(item.getElementsByTagName("full_auth_token").item(0));
				userId = Utils.getElementValue(item.getElementsByTagName("user_id").item(0));
				Me2dayInfo.setLoginId(userId);
				httpok = true;
				
			}
		}
	}

	public void onError(HttpResponse response, InputStream in) throws Exception {
		int responseCode = response.getStatusLine().getStatusCode();
		Log.d(TAG, String.format("Response Code = %d", responseCode));
		returnToken = null;

	}
	
	static public String get_full_auth_token(String authToken) {
		StringBuffer url = new StringBuffer();
		url.append(Me2dayInfo.host).append("/api/get_full_auth_token.xml?");
		url.append("token=").append(authToken);
		Utils.appendSigUrl(url, true);
		
		String urlText = url.toString();
		return urlText;
	}
	
}
